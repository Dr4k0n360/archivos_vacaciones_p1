#ifndef BLOQUES_ARCHIVOS_VACAS_H_INCLUDED
#define BLOQUES_ARCHIVOS_VACAS_H_INCLUDED

#include "ejemplo.h"
#include "discos_particiones.h"
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <dirent.h>
#include <time.h>
#include "validacionescomandos.h"

//ESTRUCTURAS REFERENTES AL SISTEMA LWH
struct SUPER_BOOT{
    char sb_nombre_hd [25];
    int sb_arbol_virtual_count;
    int sb_detalle_directorio_count;
    int sb_inodos_count;
    int sb_bloques_count;
    int sb_arbol_virtual_free;
    int sb_detalle_directorio_free;
    int sb_inodos_free;
    int sb_bloques_free;
    time_t sb_date_creacion;
    time_t sb_date_ultimo_montaje;
    int sb_montajes_count;
    int sb_ap_bitmap_arbol_directorio;
    int sb_ap_arbol_directorio;
    int sb_ap_bitmap_detalle_directorio;
    int sb_ap_detalle_directorio;
    int sb_ap_bitmap_tabla_inodo;
    int sb_ap_tabla_inodo;
    int sb_ap_bitmap_bloques;
    int sb_ap_bloques;
    int sb_ap_log;
    int sb_size_struct_arbol_directorio;
    int sb_size_struct_detalle_directorio;
    int sb_size_struct_inodo;
    int sb_size_struct_bloque;
    int sb_first_free_bit_arbol_directorio;
    int sb_first_free_bit_detalle_directorio;
    int sb_first_free_bit_tabla_inodo;
    int sb_first_free_bit_bloques;
    int sb_magic_num;
};

struct Arbol_Directorio{
    time_t avd_fecha_creacion;
    char avd_nombre_directorio[12];
    int avd_ap_array_subdirectorios[6];
    int avd_ap_detalle_directorio;
    int avd_ap_arbol_virtual_directorio;
    int avd_proper;
    int avd_permisos;
};

//ESTE SOLO SE UTILIZA EN EL DETALLE_DIRECTORIO
struct Estructura_Directorio{
    char dd_file_nombre[12];
    int dd_file_ap_inodo;
    time_t dd_file_date_creacion;
    time_t dd_file_date_modificacion;
    int permisos;
};

struct Detalle_Directorio{
    Estructura_Directorio dd_array_files[5];
    int dd_ap_detalle_directorio;
};

struct Inodos{
    int i_count_inodo;
    int i_size_archivo;
    int i_count_bloques_asignados;
    int i_array_bloques[4];
    int i_ap_indirecto;
    int i_id_proper;
};

struct Bloque_Datos{
    char db_data[25];
};

struct Bitacora{//ESTO ESTA A CAMBIOS YA QUE PUEDE CAMBIAR
    char log_tipo_operacion[200];//ESTO TENDRA LA RUTA QUE FUE UTILIZADA PARA LA CREACION DE LA CARPETA/ARCHIVO
    int log_tipo;//ESTO ME INDICARA SI ES UN ARCHIVO O CARPETA
    char log_nombre[12];//ESTO CONTENDRA EL VALOR DE P, LO QUE ME INDICARA SI HAY QUE CREARLE LOS PADRES
    char log_contenido[200];//SI ES ARCHIVO ESTO CONTENDRA EL VALOR DE SIZE O CONT
    time_t log_fecha;//LA FECHA DEL ARCHIVO
};

int PV_OBTENER_CANTIDAD_ESTRUCTURAS(int tamanio_part);
void PV_FORMATEO_FAST(int inicio_particion,int valorN,string path);
void PV_FORMATEO_FULL(int inicio_particion, int valorN, string path);
void PV_GUARDAR_SUPERBOOT(struct SUPER_BOOT boot,int inicio_particion, int valorN, string path);
void PV_GUARDAR_SUPERBOOT_COPIA(struct SUPER_BOOT boot,int inicio_particion, int valorN, string path);
struct SUPER_BOOT PV_RECUPERAR_SUPERBOOT(int inicio_particion, int valor_N, string path);
struct SUPER_BOOT PV_RECUPERAR_SUPERBOOT_COPIA(int inicio_particion, int valor_N, string path);
void PV_GUARDAR_ARBOL_DIRECTORIO(int inicio_particion, int id_arbol, int valor_N, string path, struct Arbol_Directorio AD);
void PV_GUARDAR_DETALLE_DIRECTORIO(int inicio_particion, int id_dd, int valor_N, string path, struct Detalle_Directorio DD);
void PV_GUARDAR_INODOS(int inicio_particion,int id_inodo, int valor_N, string path, struct Inodos INODO);
void PV_GUARDAR_BLOQUE_DATOS(int inicio_particion, int id_bloque, int valor_N, string path, struct Bloque_Datos BD);
struct Arbol_Directorio PV_RECUPERAR_AD(int inicio_partion, int id_arbol, int valor_N, string path);
struct Detalle_Directorio PV_RECUPERAR_DD(int inicio_particion, int id_dd, int valor_N, string path);
struct Inodos PV_RECUPERAR_INODO(int inicio_particion, int id_inodo, int valor_N, string path);
struct Bloque_Datos PV_RECUPERAR_BLOQUES(int inicio_particion, int id_bloque,int valor_N, string path);
void PV_GUARDAR_BITACORA(struct Bitacora bit,int inicio_particion, int valor_N, string path);
vector<Bitacora> PV_RECUPERAR_BITACORA(int inicio_particion, int valor_N, string path);
bool PV_EXISTE_ARCHIVO_CARPETA(vector<string> BUSCAR_RUTA, int i_ARBOLDIRECTORIO_DETALLE_DIRECTORIO, int POSICION_BUSCAR_RUTA);
string PV_OBTENER_TEXTO_ARCHIVO(vector<string> BUSCAR_RUTA, int I_DETALLEARBOL, int POSICION_BUSCAR_RUTA);
string PV_INDIRECTO_INODO_OBTENER_TEXTO_ARCHIVO(vector<string> BUSCAR_RUTA,int I_INODO,int POSICION_BUSCAR_RUTA,int PERMISOS_DD);
bool PV_ESCRIBIR_TEXTO_ARCHIVO(vector<string> BUSCAR_RUTA,int I_ARBOLDETALLE, int POSICION_BUSCAR_RUTA);
int PV_INICIO_BITMAP_BLOQUE(int inicio_particion, int valor_N);
int PV_INICIO_BITMAP_INODOS(int inicio_particion,int valor_N);
bool PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(vector<string> BUSCAR_RUTA,int I_INODO,int POSICION_BUSCAR_RUTA,int PERMISOS_DD);
bool PV_CREAR_ARCHIVO_CARPETA(vector<string> BUSCAR_RUTA,int I_ARBOLDETALLE, int POSICION_BUSCAR_RUTA);
int PV_GID_USUARIO_PROPIETARIO(int u_id);
bool PV_DETALLE_DIRECTORIO_CREAR_ARCHIVO_CARPET(vector<string> BUSCAR_RUTA,int I_DETALLE_DIRECTORIO, int POSICION_RUTA);
int PV_CREAR_INODO_PARA_DETALLE_DIRECTORIO(int id_propietario);
bool PV_INDIRECTO_DD_EXISTE_ARCHIVO_CARPETA(vector<string> BUSCAR_RUTA,int I_DD, int POSICION_BUSCAR_RUTA);
int PV_INICIO_BITMAP_DD(int inicio_particion, int valor_N);
int PV_CREAR_DETALLE_DIRECTORIO();
int PV_INICIO_BITMAP_AD(int inicio_particion,int valor_N);
int PV_CREAR_ARBOL_DIRECTORIO(string Nombre_directorio, int propietario,int permisos);
string PV_INDIRECTO_DD_OBTENER_TEXTO_ARCHIVO(vector<string> BUSCAR_RUTA, int I_DD, int POSICION_BUSCAR_RUTA);
int PV_CREAR_BLOQUE_DATOS();
bool PV_INDIRECTO_DD_ESCRIBIR_TEXTO_ARCHIVO(vector<string> BUSCAR_RUTA,int I_DD, int POSICION_BUSCAR_RUTA);
void PV_CREAR_Y_ACTUALIZAR_BITACORA(int TIPO,string contenido_size, string padres,string ruta);
void PV_FORMATEO_LOST_PERDIDA(int inicio_particion,int valor_n, string path_disco);
bool PV_HUBO_FORMATEO_LOSS();
void GRAFICAR_REPORTAR_DIRECTORIOS(struct PARAMETRO_REP_TREE param, int I_AD,int apuntador,int I_ADPadre);
void REPORTAR_TREE_COMPLETO_AD(struct PARAMETRO_REP_TREE param,int I_AD,int apuntador,int I_ADPadre);
void REPORTAR_TREE_COMPLETO_DD(struct PARAMETRO_REP_TREE param,int I_DD,int apuntador,int I_AD_DDPadre,string etiquet_AD_DD);
void REPORTAR_TREE_COMPLETO_I(struct PARAMETRO_REP_TREE param,int I_I,int apuntador,int I_DD_IPadre,string etiqueta_DD_I);
void REPORTAR_TREE_COMPLETO_B(struct PARAMETRO_REP_TREE param,int I_B,int apuntador,int I_I_Padre, string etiqueta_I);
string PV_NGRUPO_PROPIETARIO(int u_id);
string PV_NUSUARIO_PROPIETARIO(int u_id);
bool REPORTAR_LS(vector<string> RUTA,int I_AD, int POSICION_RUTA,struct PARAMETRO_REP_TREE param);
bool REPORTAR_LS_DD(vector<string> RUTA,int I_DD, int POSICION_RUTA,struct PARAMETRO_REP_TREE param);
bool REPORTAR_LISTADO_TREE_DIRECTORIO(struct PARAMETRO_REP_TREE param,vector<string> RUTA,int I_AD, int POSICION_RUTA,bool PERMITIR_LISTADO,string concatenado);
void REPORTAR_TREE_DIRECTORIO_AD(vector<string>RUTA,int POSICION_RUTA,struct PARAMETRO_REP_TREE param,int I_AD,int apuntador,int I_ADPadre);
void REPORTAR_TREE_DIRECTORIO_DD(struct PARAMETRO_REP_TREE param,int I_DD,int apuntador,int I_AD_DDPadre,string etiquet_AD_DD);
bool REPORTAR_LISTADO_TREE_FILE(struct PARAMETRO_REP_TREE param,vector<string> RUTA,int I_AD,int POSICION_RUTA,bool PERMITIR_LISTADO,string concatenado);
void REPORTAR_LISTADO_TREE_FILE_DD(struct PARAMETRO_REP_TREE param,int I_DD,string concatenado);
void REPORTAR_TREE_FILE_AD(vector<string> RUTA, int POSICION_RUTA,int I_AD,struct PARAMETRO_REP_TREE param);
void REPORTAR_TREE_FILE_DD(struct PARAMETRO_REP_TREE param,int I_DD,string NOMBRE_ARCHIVO);
void REPORTAR_TREE_FILE_INODOS(struct PARAMETRO_REP_TREE param, int I_I,int apuntador,int I_DD_IPadre,string etiqueta_DD_I);
void REPORTAR_TREE_FILE_BLOQUES(struct PARAMETRO_REP_TREE param,int I_B,int apuntador,int I_I_Padre, string etiqueta_I);
void PV_CAMBIAR_NOMBRE_AC(vector<string> RUTA,int I_AD,int POSICION_RUTA,string NUEVO_NOMBRE);
void PV_CAMBIAR_NOMBRE_DD(vector<string> RUTA,int I_DD,int POSICION_RUTA,string NUEVO_NOMBRE);
bool PV_VALIDAR_UGO_CHMOD(string numero);
void PV_CAMBIAR_PERMISOS_AD(vector<string> RUTA, int I_AD, int POSICION_RUTA,int NUEVOS_PERMISOS, bool MODO_RECURSIVO);
void PV_CAMBIAR_PERMISOS_DD(vector<string> RUTA,int I_DD,int POSICION_RUTA,int NUEVOS_PERMISOS);
void PV_CAMBIAR_PERMISOS_RECURSIVO_AD(int I_AD,int NUEVOS_PERMISOS);
void PV_CAMBIAR_PERMISOS_RECURSIVO_DD(int I_DD,int NUEVOS_PERMISOS);
void PV_CAMBIAR_PROPIETARIO_AD(vector<string> RUTA,int I_AD,int POSICION_RUTA,int NUEVO_PROPIETARIO,bool MODO_RECURSIVO);
void PV_CAMBIAR_PROPIETARIO_DD(vector<string> RUTA,int I_DD,int POSICION_RUTA,int NUEVO_PROPIETARIO);
void PV_CAMBIAR_PROPIETARIO_RECURSIVO_AD(int I_AD,int NUEVO_PROPIETARIO);
void PV_CAMBIAR_PROPIETARIO_RECURSIVO_DD(int I_DD, int NUEVO_PROPIETARIO);
void PV_MOV_AD_M1(vector<string> RUTA_PATH,int I_AD,int POSICION_RUTA,vector<string> RUTA_DESTINY);
bool PV_MOV_AD_M2(vector<string> RUTA_DESTINY,int I_AD,int POSICION_RUTA,int I_AD_MOV);
bool PV_MOV_AD_M3(int I_AD,int I_AD_MOV);
void PV_MOV_DD_M1(vector<string> RUTA_PATH, int I_DD,int POSICION_RUTA, vector<string> RUTA_DESTINY);
bool PV_MOV_AD_M4(vector<string> RUTA_DESTINY,int I_AD,int POSICION_RUTA,struct Estructura_Directorio ED);
bool PV_MOV_DD_M2(int I_DD, struct Estructura_Directorio ED);
void PV_RM_AD(vector<string> RUTA,int I_AD,int POSICION_RUTA);
void PV_ELIMINAR_BIT_AD(int ID_AD,string path_disco,int inicio_particion,int valor_N);
void PV_ELIMINAR_BIT_DD(int I_DD,string path_disco,int inicio_particion,int valor_N);
void PV_ELIMINAR_BIT_I(int I_I,string path_disco,int inicio_particion,int valor_N);
void PV_ELIMINAR_BIT_B(int I_B,string path_disco,int inicio_particion,int valor_N);
void PV_RM_DD(vector<string> RUTA,int I_DD,int POSICION_RUTA);
void PV_RM_AD_M2(int I_AD);
void PV_RM_DD_M2(int I_DD);
void PV_RM_INODOS(int I_I);

//VARIABLE GLOBAL PARA LOS DIRECTORIOS
vector<string> DIRECTORIOS_TREE_REPORTES;



int PV_OBTENER_CANTIDAD_ESTRUCTURAS(int tamanio_part){
    int calculo=0;

    calculo=((tamanio_part-(2*sizeof(SUPER_BOOT)))/(27+sizeof(Arbol_Directorio)+sizeof(Detalle_Directorio)+(5*sizeof(Inodos)+(20*sizeof(Bloque_Datos)+sizeof(Bitacora)))));

    return calculo;
}

void PV_FORMATEO_FAST(int inicio_particion,int valorN,string path){
    int inicio_bitmap_ad=inicio_particion+sizeof(SUPER_BOOT);
    int inicio_bitmap_dd=inicio_particion+sizeof(SUPER_BOOT)+valorN+(valorN*sizeof(Arbol_Directorio));
    int inicio_bitmap_i=inicio_bitmap_dd+valorN+(valorN*sizeof(Detalle_Directorio));
    int inicio_bitmap_bd=inicio_bitmap_i+(5*valorN)+(5*valorN*sizeof(Inodos));
    char valor_Escribir=48;

    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        //PRIMERO LLENAMOS DE CEROS EL BITMAP DEL ARBOL DEL DIRECTORIO
        fseek(ARCHIVO_GLOBAL,inicio_bitmap_ad,SEEK_SET);
        for(int i=inicio_bitmap_ad; i<(inicio_bitmap_ad+valorN); i++){
            fwrite(&valor_Escribir,1,1,ARCHIVO_GLOBAL);
        }
        //LLENAMOS DE CEROS EL BITMAP DEL DETALLE DE DIRECTORIO
        fseek(ARCHIVO_GLOBAL,inicio_bitmap_dd,SEEK_SET);
        for(int i=inicio_bitmap_dd; i<(inicio_bitmap_dd+valorN); i++){
            fwrite(&valor_Escribir,1,1,ARCHIVO_GLOBAL);
        }
        //LLENAMOS DE CEROS EL BITMAP DE INODOS
        fseek(ARCHIVO_GLOBAL,inicio_bitmap_i,SEEK_SET);
        for(int i=inicio_bitmap_i; i<(inicio_bitmap_i+(5*valorN)); i++){
            fwrite(&valor_Escribir,1,1,ARCHIVO_GLOBAL);
        }
        //LLENAMOS DE CEROS EL BITMAP DE DATOS
        fseek(ARCHIVO_GLOBAL,inicio_bitmap_bd,SEEK_SET);
        for(int i=inicio_bitmap_bd; i<(inicio_bitmap_bd+(20*valorN)); i++){
            fwrite(&valor_Escribir,1,1,ARCHIVO_GLOBAL);
        }

        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln("....ARCHIVO GLOBAL ABIERTO 1....");
    }

}


void PV_FORMATEO_FULL(int inicio_particion, int valorN, string path){
    int fin_formateo=inicio_particion+sizeof(SUPER_BOOT)+(27*valorN)+(valorN*sizeof(Arbol_Directorio))+(valorN*sizeof(Detalle_Directorio))+(5*valorN*sizeof(Inodos))+(20*valorN*sizeof(Bloque_Datos))+(valorN*sizeof(Bitacora))+sizeof(SUPER_BOOT);
    char valor_Escribir=48;

    if(ARCHIVO_GLOBAL==NULL){

        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,inicio_particion,SEEK_SET);
        for(int i=inicio_particion; i<fin_formateo; i++){
            fwrite(&valor_Escribir,1,1,ARCHIVO_GLOBAL);
        }

        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 2....");
    }
}

//--------------------------------------------------METODOS PARA GUARDAR Y RECUPERAR ESTRUCTURAS DEL ARCHIVO ----------------------------
void PV_GUARDAR_SUPERBOOT(struct SUPER_BOOT boot,int inicio_particion, int valorN, string path){

    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,inicio_particion,SEEK_SET);
        fwrite(&boot,sizeof(SUPER_BOOT),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 3....");
    }
}

void PV_GUARDAR_SUPERBOOT_COPIA(struct SUPER_BOOT boot,int inicio_particion, int valorN, string path){

    int posicion_guardar_copia=inicio_particion+sizeof(SUPER_BOOT)+(27*valorN)+(valorN*sizeof(Arbol_Directorio))+(valorN*sizeof(Detalle_Directorio))+(5*valorN*sizeof(Inodos))+(20*valorN*sizeof(Bloque_Datos))+(valorN*sizeof(Bitacora));

    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion_guardar_copia,SEEK_SET);
        fwrite(&boot,sizeof(SUPER_BOOT),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 4....");
    }
}

struct SUPER_BOOT PV_RECUPERAR_SUPERBOOT(int inicio_particion, int valor_N, string path){
    struct SUPER_BOOT temporal;

    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,inicio_particion,SEEK_SET);
        fread(&temporal,sizeof(SUPER_BOOT),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 5....");
    }

    return temporal;
}

struct SUPER_BOOT PV_RECUPERAR_SUPERBOOT_COPIA(int inicio_particion, int valorN, string path){
    struct SUPER_BOOT temporal;
    int posicion_recuperar_copia=inicio_particion+sizeof(SUPER_BOOT)+(27*valorN)+(valorN*sizeof(Arbol_Directorio))+(valorN*sizeof(Detalle_Directorio))+(5*valorN*sizeof(Inodos))+(20*valorN*sizeof(Bloque_Datos))+(valorN*sizeof(Bitacora));

    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion_recuperar_copia,SEEK_SET);
        fread(&temporal,sizeof(SUPER_BOOT),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 6....");
    }
    return temporal;
}


void PV_GUARDAR_ARBOL_DIRECTORIO(int inicio_particion, int id_arbol, int valor_N, string path, struct Arbol_Directorio AD){
    int posicion_guardar=inicio_particion+sizeof(SUPER_BOOT)+valor_N+(id_arbol*sizeof(Arbol_Directorio));

    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion_guardar,SEEK_SET);
        fwrite(&AD,sizeof(Arbol_Directorio),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 7....");
    }

}

void PV_GUARDAR_DETALLE_DIRECTORIO(int inicio_particion, int id_dd, int valor_N, string path, struct Detalle_Directorio DD){
    int posicion_guardar=inicio_particion+sizeof(SUPER_BOOT)+valor_N+(valor_N*sizeof(Arbol_Directorio))+valor_N+(id_dd*sizeof(Detalle_Directorio));

    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion_guardar,SEEK_SET);
        fwrite(&DD,sizeof(Detalle_Directorio),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 8....");
    }
}

void PV_GUARDAR_INODOS(int inicio_particion,int id_inodo, int valor_N, string path, struct Inodos INODO){
    int posicion_guardar=inicio_particion+sizeof(SUPER_BOOT)+valor_N+(valor_N*sizeof(Arbol_Directorio))+valor_N+(valor_N*sizeof(Detalle_Directorio))+(5*valor_N)+(id_inodo*sizeof(Inodos));

    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion_guardar,SEEK_SET);
        fwrite(&INODO,sizeof(Inodos),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 9....");
    }
}

void PV_GUARDAR_BLOQUE_DATOS(int inicio_particion, int id_bloque, int valor_N, string path, struct Bloque_Datos BD){
    int posicion_guardar=inicio_particion+sizeof(SUPER_BOOT)+valor_N+(valor_N*sizeof(Arbol_Directorio))+valor_N+(valor_N*sizeof(Detalle_Directorio))+(5*valor_N)+(5*valor_N*sizeof(Inodos))+(20*valor_N)+(id_bloque*sizeof(Bloque_Datos));

    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion_guardar,SEEK_SET);
        fwrite(&BD,sizeof(Bloque_Datos),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 10....");
    }

}

struct Arbol_Directorio PV_RECUPERAR_AD(int inicio_partion, int id_arbol, int valor_N, string path){
    struct Arbol_Directorio temporal;
    int posicion_recuperar=inicio_partion+sizeof(SUPER_BOOT)+valor_N+(id_arbol*sizeof(Arbol_Directorio));
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion_recuperar,SEEK_SET);
        fread(&temporal,sizeof(Arbol_Directorio),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 11....");
    }
    return temporal;
}

struct Detalle_Directorio PV_RECUPERAR_DD(int inicio_particion, int id_dd, int valor_N, string path){
    struct Detalle_Directorio temporal;
    int posicion_recuperar=inicio_particion+sizeof(SUPER_BOOT)+valor_N+(valor_N*sizeof(Arbol_Directorio))+valor_N+(id_dd*sizeof(Detalle_Directorio));
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion_recuperar,SEEK_SET);
        fread(&temporal,sizeof(Detalle_Directorio),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 12....");
    }
    return temporal;
}

struct Inodos PV_RECUPERAR_INODO(int inicio_particion, int id_inodo, int valor_N, string path){
    struct Inodos temporal;
    int posicion_recuperar=inicio_particion+sizeof(SUPER_BOOT)+valor_N+(valor_N*sizeof(Arbol_Directorio))+valor_N+(valor_N*sizeof(Detalle_Directorio))+(5*valor_N)+(id_inodo*sizeof(Inodos));
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion_recuperar,SEEK_SET);
        fread(&temporal,sizeof(Inodos),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 13....");
    }
    return temporal;
}

struct Bloque_Datos PV_RECUPERAR_BLOQUES(int inicio_particion, int id_bloque,int valor_N, string path){
    struct Bloque_Datos temporal;
    int posicion_recuperar=inicio_particion+sizeof(SUPER_BOOT)+valor_N+(valor_N*sizeof(Arbol_Directorio))+valor_N+(valor_N*sizeof(Detalle_Directorio))+(5*valor_N)+(5*valor_N*sizeof(Inodos))+(20*valor_N)+(id_bloque*sizeof(Bloque_Datos));
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion_recuperar,SEEK_SET);
        fread(&temporal,sizeof(Bloque_Datos),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 14....");
    }
    return temporal;
}

void PV_GUARDAR_BITACORA(struct Bitacora bit,int inicio_particion, int valor_N, string path){
    int inicio_bitacora=inicio_particion+sizeof(SUPER_BOOT)+valor_N+(valor_N*sizeof(Arbol_Directorio))+valor_N+(valor_N*sizeof(Detalle_Directorio))+(5*valor_N)+(5*valor_N*sizeof(Inodos))+(20*valor_N)+(20*valor_N*sizeof(Bloque_Datos));
    int id_bitacora=0;

    FILE* ARCHIVO_TEMPORAL;
    ARCHIVO_TEMPORAL=fopen(path.c_str(),"rb+");
    struct Bitacora BIT_TEMPORAL;
    bool posicion_encontrada=false;
    for(int i=inicio_bitacora; (i<(inicio_bitacora+(valor_N*sizeof(Bitacora)))) && (posicion_encontrada==false); i=i+sizeof(Bitacora)){
        fseek(ARCHIVO_TEMPORAL,i,SEEK_SET);
        fread(&BIT_TEMPORAL,sizeof(Bitacora),1,ARCHIVO_TEMPORAL);
        if((BIT_TEMPORAL.log_tipo!=0) && (BIT_TEMPORAL.log_tipo!=1)){
            posicion_encontrada=true;
        }else{
            id_bitacora++;
        }
    }
    fflush(ARCHIVO_TEMPORAL);
    fclose(ARCHIVO_TEMPORAL);
    ARCHIVO_TEMPORAL=NULL;

    int posicion_guardar=inicio_bitacora+(id_bitacora*sizeof(Bitacora));

    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,posicion_guardar,SEEK_SET);
        fwrite(&bit,sizeof(Bitacora),1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 15....");
    }
}


vector<Bitacora> PV_RECUPERAR_BITACORA(int inicio_particion, int valor_N, string path){
    vector<Bitacora> temporal;
    bool final_bitacora=false;
    int posicion_recuperar=inicio_particion+sizeof(SUPER_BOOT)+valor_N+(valor_N*sizeof(Arbol_Directorio))+valor_N+(valor_N*sizeof(Detalle_Directorio))+(5*valor_N)+(5*valor_N*sizeof(Inodos))+(20*valor_N)+(20*valor_N*sizeof(Bloque_Datos));

    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path.c_str(),"rb+");
        struct Bitacora BIT;
        for(int i=posicion_recuperar;(i<(posicion_recuperar+(valor_N*sizeof(Bitacora)))) && (final_bitacora==false); i=i+sizeof(Bitacora)){
            fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
            fread(&BIT,sizeof(Bitacora),1,ARCHIVO_GLOBAL);
            if((BIT.log_tipo==0) || (BIT.log_tipo==1)){
                temporal.push_back(BIT);
            }else{
                final_bitacora=true;
            }
        }
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 16....");
    }

    return temporal;
}
//----------------------------------METODOS QUE VALIDAN LA EXISTENCIA DE UNA CARPETA/ARCHIVO ---------------------------------------------
bool PV_EXISTE_ARCHIVO_CARPETA(vector<string> BUSCAR_RUTA, int i_ARBOLDIRECTORIO_DETALLE_DIRECTORIO, int POSICION_BUSCAR_RUTA){
    bool existe=false;
    //PRIMERO VALIDAMOS SI LO QUE VAMOS A BUSCAR ES UN ARCHIVO O CARPETA
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,i_ARBOLDIRECTORIO_DETALLE_DIRECTORIO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    //imprimirln("BANDERA1");
    if(VALIDAR_EXTENSION_SINMENSAJE(BUSCAR_RUTA[POSICION_BUSCAR_RUTA])){//SI ES TRUE SIGNIFICA QUE ES UN ARCHIVO
      //  imprimirln("ENTRA ARCHIVO");
        if(AD.avd_ap_detalle_directorio!=-1){//SI ESTO ES CIERTO SIGNIFICA QUE LA CARPETA TIENE ARCHIVOS EN EL DETALLE DE DIRECTORIO
            struct Detalle_Directorio DD;
            DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_detalle_directorio,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
            //imprimirln("BANDERA2");
            for(int i=0; (i<6) && (existe==false); i++){//RECORREMOS LOS APUNTADORES DEL DETALLE DE DIRECTORIO
                if(i==5){
                    //imprimirln("BANDERA3");
                    if((DD.dd_ap_detalle_directorio!=-1) && (strcmp(BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str(),"users.txt")!=0)){//SI ESTO ES TRUE SIGNIFICA QUE HAY APUNTADOR INDIRECTO
                        existe=PV_INDIRECTO_DD_EXISTE_ARCHIVO_CARPETA(BUSCAR_RUTA,DD.dd_ap_detalle_directorio,POSICION_BUSCAR_RUTA);
                    }
                }else{
                    if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){//SI ESTO ES TRUE SIGNICA QUE EXISTE EL NOMBRE DE UN ARCHIVO QUE EXISTE
                    //   imprimirNumln(DD.dd_array_files[i].dd_file_ap_inodo);
                     //  cout<<DD.dd_array_files[i].dd_file_nombre<<endl;
                       if(strcmp(DD.dd_array_files[i].dd_file_nombre,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){//VALIDAMOS SI ES EL ARCHIVO BUSCADO
                         //  imprimirln("ENCONTRO ARCHIVO 1");
                         //   cout<<DD.dd_array_files[i].dd_file_nombre<<endl;
                            existe=true;
                       }
                    }
                }
            }
        }
    }else{//SI ES FALSE ES UNA CARPETA
       // imprimirln("ENTRA CARPETA");
        for(int i=0; (i<7) && (existe==false); i++){
            if(i==6){
                if(AD.avd_ap_arbol_virtual_directorio!=-1){
                    existe=PV_EXISTE_ARCHIVO_CARPETA(BUSCAR_RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_BUSCAR_RUTA);
                }
            }else{
                if(AD.avd_ap_array_subdirectorios[i]!=-1){
                    struct Arbol_Directorio SUBDIR;
                    SUBDIR=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                    if(strcmp(SUBDIR.avd_nombre_directorio,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
                    //        imprimirln("ENCONTRO CARPETA 2");
                    //        cout<<SUBDIR.avd_nombre_directorio<<endl;
                        existe=true;
                    }
                }
            }
        }
    }

    return existe;
}

bool PV_INDIRECTO_DD_EXISTE_ARCHIVO_CARPETA(vector<string> BUSCAR_RUTA,int I_DD, int POSICION_BUSCAR_RUTA){
    bool existe=false;
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N, DATOS_PIVOTE[0].path_disco);

    for(int i=0; (i<6) && (existe==false); i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                existe=PV_INDIRECTO_DD_EXISTE_ARCHIVO_CARPETA(BUSCAR_RUTA,DD.dd_ap_detalle_directorio,POSICION_BUSCAR_RUTA);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                if(strcmp(DD.dd_array_files[i].dd_file_nombre,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
           //         imprimirln("ENCONTRO ARCHIVO 3");
           //         cout<<DD.dd_array_files[i].dd_file_nombre<<endl;
                    existe=true;
                }
            }
        }
    }

    return existe;
}
//----------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------METODOS PARA RECUPERAR DATOS DE UN ARCHIVO --------------------------------------------------------

string PV_OBTENER_TEXTO_ARCHIVO(vector<string> BUSCAR_RUTA, int I_DETALLEARBOL, int POSICION_BUSCAR_RUTA){
    string respuesta="";
    bool ARCHIVO_ENCONTRADO=false;
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_DETALLEARBOL,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);


    if(PV_EXISTE_ARCHIVO_CARPETA(BUSCAR_RUTA,I_DETALLEARBOL,POSICION_BUSCAR_RUTA)){
        //*****
        if(VALIDAR_EXTENSION_SINMENSAJE(BUSCAR_RUTA[POSICION_BUSCAR_RUTA])){//SI ENTRA AQUI ES UN ARCHIVO
            if(AD.avd_ap_detalle_directorio!=-1){//LA CARPETA TIENE ARCHIVOS
                struct Detalle_Directorio DD;
                DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_detalle_directorio,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                for(int i=0; (i<6) && (ARCHIVO_ENCONTRADO==false); i++){
                    if(i==5){
                        if(DD.dd_ap_detalle_directorio!=-1){//TIENE APUNTADOR INDIRECTO DE ARCHIVOS
                            respuesta+=PV_INDIRECTO_DD_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,DD.dd_ap_detalle_directorio,POSICION_BUSCAR_RUTA);
                        }
                    }else{
                        if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                            if((strcmp(DD.dd_array_files[i].dd_file_nombre,"users.txt")==0) && (strcmp(BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str(),"users.txt")==0)){//SI ES EL ARCHIVO USERSR NO VALIDAMOS PERMISOS
                                //RECUPERAMOS LA ESTRUCTURA INODO DEL ARCHIVO USERS
                                struct Inodos INODO;
                                ARCHIVO_ENCONTRADO=true;
                                INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

                                for(int j=0; j<5; j++){
                                    if(j==4){//APUNTADOR INDIRECTO DE INODOS
                                        if(INODO.i_ap_indirecto!=-1){
                                            respuesta+=PV_INDIRECTO_INODO_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                        }
                                    }else{
                                        if(INODO.i_array_bloques[j]!=-1){//SI ES DIFERENTES -1 RECUPERAMOS LAS ESTRUCTURAS BLOQUES A LAS QUE APUNTA EL INODO
                                            struct Bloque_Datos BLOQUE;
                                            BLOQUE=PV_RECUPERAR_BLOQUES(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[j],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                            respuesta+=BLOQUE.db_data;
                                        }
                                    }
                                }

                            }else{//SI NO ES EL ARCHIVO USERS SI VALIDAMOS PERMISOS
                                if(strcmp(DD.dd_array_files[i].dd_file_nombre,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
                                    ARCHIVO_ENCONTRADO=true;
                                    /*imprimirln("INTENTA OBTENER TEXTO NO DEBERIA PASAR A MENOS DE SER EL ARCHIVO ...");
                                    imprimirNumln(i);
                                    cout<<DD.dd_array_files[i].dd_file_nombre<<endl;*/
                                    struct Inodos INODO;
                                    INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                    //VALIDANDO LOS PERMISOS
                                    if((PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==4) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==5) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==6) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==7)){
                                        for(int j=0; j<5; j++){
                                            if(j==4){//APUNTADOR INDIRECTO DE INODOS
                                                if(INODO.i_ap_indirecto!=-1){
                                                    respuesta+=PV_INDIRECTO_INODO_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                                }
                                            }else{
                                                if(INODO.i_array_bloques[j]!=-1){
                                                    struct Bloque_Datos BLOQUE;
                                                    BLOQUE=PV_RECUPERAR_BLOQUES(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[j],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                                    respuesta+=BLOQUE.db_data;
                                                }
                                            }
                                        }
                                    }else{
                                        imprimirln("=== ERROR: NO TIENE PERMISOS DE LECTURA ARCHIVO ===");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }else{//EN CASO CONTRARIO CARPETA
            for(int i=0; (i<7) && (ARCHIVO_ENCONTRADO==false); i++){
                if(i==6){
                    if(AD.avd_ap_arbol_virtual_directorio!=-1){
                        respuesta+=PV_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_BUSCAR_RUTA);
                    }
                }else{
                    if(AD.avd_ap_array_subdirectorios[i]!=-1){
                        struct Arbol_Directorio AD2;
                        AD2=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

                        if(strcmp(AD2.avd_nombre_directorio,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
                            ARCHIVO_ENCONTRADO=true;
                            //VALIDANDO PERMISOS DE LECTURA
                            if((PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==4) || (PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==5) || (PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==6) || (PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==7)){
                                if(POSICION_BUSCAR_RUTA!=(BUSCAR_RUTA.size()-1)){
                                    respuesta+=PV_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_BUSCAR_RUTA+1));
                                }
                            }else{
                                imprimirln("=== ERROR: NO TIENE PERMISOS DE LECTURA CARPETA ===");
                            }
                        }
                    }
                }
            }
        }
        //*****
    }else{
        imprimirln(" === ERROR: EL ARCHIVO/CARPETA PADRE NO EXISTE ====");
    }

 /*   if(VALIDAR_EXTENSION_SINMENSAJE(BUSCAR_RUTA[POSICION_BUSCAR_RUTA])){//SI ENTRA AQUI ES UN ARCHIVO
        if(AD.avd_ap_detalle_directorio!=-1){//LA CARPETA TIENE ARCHIVOS
            struct Detalle_Directorio DD;
            DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_detalle_directorio,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
            for(int i=0; (i<6) && (ARCHIVO_ENCONTRADO==false); i++){
                if(i==5){
                    if(DD.dd_ap_detalle_directorio!=-1){//TIENE APUNTADOR INDIRECTO DE ARCHIVOS
                        respuesta+=PV_INDIRECTO_DD_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,DD.dd_ap_detalle_directorio,POSICION_BUSCAR_RUTA);
                    }
                }else{
                    if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                        if((strcmp(DD.dd_array_files[i].dd_file_nombre,"users.txt")==0) && (strcmp(BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str(),"users.txt")==0)){//SI ES EL ARCHIVO USERSR NO VALIDAMOS PERMISOS
                            //RECUPERAMOS LA ESTRUCTURA INODO DEL ARCHIVO USERS
                            struct Inodos INODO;
                            ARCHIVO_ENCONTRADO=true;
                            INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

                            for(int j=0; j<5; j++){
                                if(j==4){//APUNTADOR INDIRECTO DE INODOS
                                    if(INODO.i_ap_indirecto!=-1){
                                        respuesta+=PV_INDIRECTO_INODO_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                    }
                                }else{
                                    if(INODO.i_array_bloques[j]!=-1){//SI ES DIFERENTES -1 RECUPERAMOS LAS ESTRUCTURAS BLOQUES A LAS QUE APUNTA EL INODO
                                        struct Bloque_Datos BLOQUE;
                                        BLOQUE=PV_RECUPERAR_BLOQUES(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[j],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                        respuesta+=BLOQUE.db_data;
                                    }
                                }
                            }

                        }else{//SI NO ES EL ARCHIVO USERS SI VALIDAMOS PERMISOS
                            if(strcmp(DD.dd_array_files[i].dd_file_nombre,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
                                ARCHIVO_ENCONTRADO=true;
                           //     imprimirln("INTENTA OBTENER TEXTO NO DEBERIA PASAR A MENOS DE SER EL ARCHIVO ...");
                           //     imprimirNumln(i);
                           //     cout<<DD.dd_array_files[i].dd_file_nombre<<endl;
                                struct Inodos INODO;
                                INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                //VALIDANDO LOS PERMISOS
                                if((PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==4) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==5) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==6) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==7)){
                                    for(int j=0; j<5; j++){
                                        if(j==4){//APUNTADOR INDIRECTO DE INODOS
                                            if(INODO.i_ap_indirecto!=-1){
                                                respuesta+=PV_INDIRECTO_INODO_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                            }
                                        }else{
                                            if(INODO.i_array_bloques[j]!=-1){
                                                struct Bloque_Datos BLOQUE;
                                                BLOQUE=PV_RECUPERAR_BLOQUES(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[j],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                                respuesta+=BLOQUE.db_data;
                                            }
                                        }
                                    }
                                }else{
                                    imprimirln("=== ERROR: NO TIENE PERMISOS DE LECTURA ARCHIVO ===");
                                }
                            }
                        }
                    }
                }
            }
        }
    }else{//EN CASO CONTRARIO CARPETA
        for(int i=0; (i<7) && (ARCHIVO_ENCONTRADO==false); i++){
            if(i==6){
                if(AD.avd_ap_arbol_virtual_directorio!=-1){
                    respuesta+=PV_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_BUSCAR_RUTA);
                }
            }else{
                if(AD.avd_ap_array_subdirectorios[i]!=-1){
                    struct Arbol_Directorio AD2;
                    AD2=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

                    if(strcmp(AD2.avd_nombre_directorio,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
                        ARCHIVO_ENCONTRADO=true;
                        //VALIDANDO PERMISOS DE LECTURA
                        if((PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==4) || (PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==5) || (PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==6) || (PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==7)){
                            if(POSICION_BUSCAR_RUTA!=(BUSCAR_RUTA.size()-1)){
                                respuesta+=PV_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_BUSCAR_RUTA+1));
                            }
                        }else{
                            imprimirln("=== ERROR: NO TIENE PERMISOS DE LECTURA CARPETA ===");
                        }
                    }
                }
            }
        }
    }*/
    return respuesta;
}

string PV_INDIRECTO_INODO_OBTENER_TEXTO_ARCHIVO(vector<string> BUSCAR_RUTA,int I_INODO,int POSICION_BUSCAR_RUTA,int PERMISOS_DD){
    string respuesta="";
    struct Inodos INODO;
    INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,I_INODO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    for(int i=0; i<5; i++){
        if(i==4){
            if(INODO.i_ap_indirecto!=-1){
            //    if(strcmp(BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str(),"users.txt")==0){
                    respuesta+=PV_INDIRECTO_INODO_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,PERMISOS_DD);
            //    }else{

            //    }
            }
        }else{
            if(INODO.i_array_bloques[i]!=-1){
            //    if(strcmp(BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str(),"users.txt")==0){
                    struct Bloque_Datos BLOQUE;
                    BLOQUE=PV_RECUPERAR_BLOQUES(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                    respuesta+=BLOQUE.db_data;
            //    }else{

            //    }
            }
        }
    }

    return respuesta;
}

string PV_INDIRECTO_DD_OBTENER_TEXTO_ARCHIVO(vector<string> BUSCAR_RUTA, int I_DD, int POSICION_BUSCAR_RUTA){
    string respuesta="";
    bool ARCHIVO_ENCONTRADO=false;
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    for(int i=0; (i<6) && (ARCHIVO_ENCONTRADO==false); i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                respuesta+=PV_INDIRECTO_DD_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,DD.dd_ap_detalle_directorio,POSICION_BUSCAR_RUTA);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                if(strcmp(DD.dd_array_files[i].dd_file_nombre,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
                    ARCHIVO_ENCONTRADO=true;
                    struct Inodos INODO;
                    INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                    if((PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==4) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==5) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==6) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==7)){
                        for(int j=0; j<5; j++){
                            if(j==4){
                                    if(INODO.i_ap_indirecto!=-1){
                                        respuesta+=PV_INDIRECTO_INODO_OBTENER_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                    }
                            }else{
                                if(INODO.i_array_bloques[j]!=-1){
                                    struct Bloque_Datos BLOQUE;
                                    BLOQUE=PV_RECUPERAR_BLOQUES(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[j],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                    respuesta+=BLOQUE.db_data;
                                }
                            }
                        }
                    }else{
                        imprimirln("=== ERROR: NO TIENE PERMISOS DE LECTURA ARCHIVO ===");
                    }
                }
            }
        }
    }

    return respuesta;
}
//--------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------METODOS PARA ESCRIBIR DATOS EN UN ARCHIVO ----------------------------------------------------------
//ESTOS METODOS BUSCAN EL ARCHIVO Y RELLENAN SU TEXTO, YA DEBE EXISTIR O HABERSE CREADO EL ARCHIVO
bool PV_ESCRIBIR_TEXTO_ARCHIVO(vector<string> BUSCAR_RUTA,int I_ARBOLDETALLE, int POSICION_BUSCAR_RUTA){
    bool encontrado=false;

    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_ARBOLDETALLE,DATOS_PIVOTE[0].valor_N, DATOS_PIVOTE[0].path_disco);

    if(PV_EXISTE_ARCHIVO_CARPETA(BUSCAR_RUTA,I_ARBOLDETALLE,POSICION_BUSCAR_RUTA)){
     //*********************
        if(VALIDAR_EXTENSION_SINMENSAJE(BUSCAR_RUTA[POSICION_BUSCAR_RUTA])){//SI ENTRA AQUI ES UN ARCHIVO
            if(AD.avd_ap_detalle_directorio!=-1){//VERIFICAMOS SI TIENE ARCHIVOS
                //RECUPERAMOS EL DETALLE DIRECTORIO Y RECORREMOS SUS 6 APUNTADORES
                struct Detalle_Directorio DD;
                DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_detalle_directorio,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                for(int i=0; (i<6) && (encontrado==false); i++){
                    if(i==5){//PARA VALIDAR EL APUNTADOR DE DETALLE INDIRECTO
                        if(DD.dd_ap_detalle_directorio!=-1){//SI EL APUNTADOR INDIRECTO TIENE ARCHIVOS ENTRAMOS A SEGUIR BUSCANDO
                            PV_INDIRECTO_DD_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,DD.dd_ap_detalle_directorio,POSICION_BUSCAR_RUTA);
                        }
                    }else{
                        if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){//SI ES DIFERENTE A -1 SIGNIFICA QUE EXISTE UN ARCHIVO AL CUAL APUNTE
                           // imprimirNumln(i);
                            if((strcmp(DD.dd_array_files[i].dd_file_nombre,"users.txt")==0) && (strcmp(BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str(),"users.txt")==0)){ //SI ES EL ARCHIVO USERS NO VALIDAMOS PERMISOS
                                encontrado=true;
                                //RECUPERAMOS LA ESTRUCTUR INODO DEL ARCHIVO USERS
                                struct Inodos INODO;
                                INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                for(int j=0; (j<5) && (DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()!=0); j++){
                                    if(j==4){
                                        if(INODO.i_ap_indirecto!=-1){//SI ES DIFERENTE A -1 NOS VAMOS A INSERTARLE NUEVOS DATOS
                                            PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                        }else{//EN CASO CONTRARIO SE CREAR Y GUARDA UNA ESTRUCTURA DE INDIRECTOS
                                            int INICIO_BM_INODOS=PV_INICIO_BITMAP_INODOS(DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                                            bool BITMAP_ACTUALIZADO=false;
                                            int IDENTIFICADOR_NINODO=0;
                                            char VERIFICAR_VALOR;
                                            if(ARCHIVO_GLOBAL==NULL){
                                                ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_disco.c_str(),"rb+");
                                                for(int k=INICIO_BM_INODOS; (k<(INICIO_BM_INODOS+(5*DATOS_PIVOTE[0].valor_N))) && (BITMAP_ACTUALIZADO==false); k++){
                                                    VERIFICAR_VALOR=00;
                                                    fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                                    fread(&VERIFICAR_VALOR,1,1,ARCHIVO_GLOBAL);
                                                    if(VERIFICAR_VALOR==48){
                                                        char ACTUALIZAR=49;
                                                        BITMAP_ACTUALIZADO=true;
                                                        fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                                        fwrite(&ACTUALIZAR,1,1,ARCHIVO_GLOBAL);
                                                        fflush(ARCHIVO_GLOBAL);
                                                        fclose(ARCHIVO_GLOBAL);
                                                        ARCHIVO_GLOBAL=NULL;
                                                        //ACTUALIZANDO EL INODO
                                                        INODO.i_ap_indirecto=IDENTIFICADOR_NINODO;
                                                        PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                                                        //CREANDO LA NUEVA ESTRUCTURA INODO PARA EL INDIRECTO
                                                        struct Inodos NINODO;
                                                        NINODO.i_count_inodo=IDENTIFICADOR_NINODO;
                                                        NINODO.i_ap_indirecto=-1;
                                                        //NINODO.i_id_proper=atoi((to_string(DATOS_PIVOTE[0].u_id)+to_string(DATOS_PIVOTE[0].g_id)).c_str());
                                                        NINODO.i_id_proper=DATOS_PIVOTE[0].u_id;
                                                        for(int z=0; z<4; z++){
                                                            NINODO.i_array_bloques[z]=-1;
                                                        }
                                                        PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,INODO.i_ap_indirecto,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,NINODO);
                                                        PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                                    }
                                                    IDENTIFICADOR_NINODO++;
                                                }
                                                if(ARCHIVO_GLOBAL!=NULL){
                                                    fflush(ARCHIVO_GLOBAL);
                                                    fclose(ARCHIVO_GLOBAL);
                                                    ARCHIVO_GLOBAL=NULL;
                                                }
                                            }else{
                                                imprimirln(".... ARCHIVO GLOBAL ABIERTO 18....");
                                            }
                                        }
                                    }else{
                                        if(INODO.i_array_bloques[j]!=-1){//SI ES DIFERENTE A -1 SE ACTUALIZA Y GUARDA EL BLOQUE DATOS
                                            struct Bloque_Datos BD;
                                            strcpy(BD.db_data,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());
                                            PV_GUARDAR_BLOQUE_DATOS(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[j],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,BD);
                                            DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                        }else{//EN CASO CONTRARIO SE CREA Y GUARDA UN NUEVO BLOQUE DE DATOS ACTUALIZADO
                                            int inicio_bmp_bloques=PV_INICIO_BITMAP_BLOQUE(DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                                            bool bitmap_actualizado=false;
                                            int IDENTIFICADOR_NBLOQUE=0;
                                            char VERIFICAR_VALOR;
                                            if(ARCHIVO_GLOBAL==NULL){
                                                ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_disco.c_str(),"rb+");
                                                for(int k=inicio_bmp_bloques;(k<(inicio_bmp_bloques+(20*DATOS_PIVOTE[0].valor_N))) && (bitmap_actualizado==false); k++){
                                                    VERIFICAR_VALOR=00;
                                                    fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                                    fread(&VERIFICAR_VALOR,1,1,ARCHIVO_GLOBAL);
                                                    if(VERIFICAR_VALOR==48){
                                                        //ACTUALIZO EL BITMAP DE BLOQUE DATOS
                                                        char ACTUALIZACION=49;
                                                        bitmap_actualizado=true;
                                                        fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                                        fwrite(&ACTUALIZACION,1,1,ARCHIVO_GLOBAL);
                                                        fflush(ARCHIVO_GLOBAL);
                                                        fclose(ARCHIVO_GLOBAL);
                                                        ARCHIVO_GLOBAL=NULL;
                                                        //ACTUALIZO INODO
                                                        INODO.i_array_bloques[j]=IDENTIFICADOR_NBLOQUE;
                                                        PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                                                        //CREAO EL BLOQUE DATO Y LO GUARDO
                                                        struct Bloque_Datos BD2;
                                                        strcpy(BD2.db_data,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());
                                                        PV_GUARDAR_BLOQUE_DATOS(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[j],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,BD2);
                                                        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                                    }
                                                    IDENTIFICADOR_NBLOQUE++;
                                                }
                                                if(ARCHIVO_GLOBAL!=NULL){
                                                    fflush(ARCHIVO_GLOBAL);
                                                    fclose(ARCHIVO_GLOBAL);
                                                    ARCHIVO_GLOBAL=NULL;
                                                }
                                            }else{
                                                imprimirln(".... ARCHIVO GLOBAL ABIERTO 17....");
                                            }
                                        }
                                    }
                                }
                            }else{//SI NO LO ES, HAY QUE VALIDAR LOS PERMISOS Y VERIFICAR SI ES EL ARCHIVO A EDITAR
                                if(strcmp(DD.dd_array_files[i].dd_file_nombre,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
                                    encontrado=true;
                                   // imprimirln("ESTO NO DEBERIA PASAR POR AHORA UNICAMENTE SI ES EL ARCHIVO...");
                                    struct Inodos INODO;
                                    INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                    //VALIDANDO PERMISOS PARA LA CREACION DE BLOQUES E INODOS
                                    if((PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==2) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==3) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==6) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==7)){
                                        for(int k=0; (k<5) && (DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()!=0); k++){
                                            if(k==4){
                                                if(INODO.i_ap_indirecto!=-1){
                                                    PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                                }else{
                                                    INODO.i_ap_indirecto=PV_CREAR_INODO_PARA_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].u_id);
                                                    PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                                                    PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                                }
                                            }else{
                                                if(INODO.i_array_bloques[k]!=-1){
                                                    struct Bloque_Datos BD;
                                                    strcpy(BD.db_data,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());
                                                    PV_GUARDAR_BLOQUE_DATOS(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[k],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,BD);
                                                    DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                                }else{
                                                    INODO.i_array_bloques[k]=PV_CREAR_BLOQUE_DATOS();
                                                    PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                                                }
                                            }
                                        }
                                    }else{
                                        imprimirln("=== ERROR: NO TIENE PERMISOS DE ESCRITURA ARCHIVO ===");
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }else{//EN CASO CONTRARIO ES UNA CARPETA
            for(int i=0; (i<7) && (encontrado==false); i++){
                if(i==6){
                    if(AD.avd_ap_arbol_virtual_directorio!=-1){
                        PV_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_BUSCAR_RUTA);
                    }
                }else{
                    if(AD.avd_ap_array_subdirectorios[i]!=-1){
                        struct Arbol_Directorio AD2;
                        AD2=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

                        if(strcmp(AD2.avd_nombre_directorio,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
                            encontrado=true;
                            if((PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==2) || (PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==3) || (PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==6) || (PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==7)){
                                if(POSICION_BUSCAR_RUTA!=(BUSCAR_RUTA.size()-1)){
                                    PV_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_BUSCAR_RUTA+1));
                                }
                            }else{
                                imprimirln("=== ERROR: NO TIEME PERMISOS DE ESCRITURA CARPETAS ===");
                            }
                        }
                    }
                }
            }
        }
     //*********************
    }else{
        imprimirln("==== ERROR: ARCHIVO/CARPETA PADRE INEXISTENTE PARA PODER EDITAR EL ARCHIVO ====");
        encontrado=false;
    }

 /*   if(VALIDAR_EXTENSION_SINMENSAJE(BUSCAR_RUTA[POSICION_BUSCAR_RUTA])){//SI ENTRA AQUI ES UN ARCHIVO
        if(AD.avd_ap_detalle_directorio!=-1){//VERIFICAMOS SI TIENE ARCHIVOS
            //RECUPERAMOS EL DETALLE DIRECTORIO Y RECORREMOS SUS 6 APUNTADORES
            struct Detalle_Directorio DD;
            DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_detalle_directorio,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
            for(int i=0; (i<6) && (encontrado==false); i++){
                if(i==5){//PARA VALIDAR EL APUNTADOR DE DETALLE INDIRECTO
                    if(DD.dd_ap_detalle_directorio!=-1){//SI EL APUNTADOR INDIRECTO TIENE ARCHIVOS ENTRAMOS A SEGUIR BUSCANDO
                        PV_INDIRECTO_DD_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,DD.dd_ap_detalle_directorio,POSICION_BUSCAR_RUTA);
                    }
                }else{
                    if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){//SI ES DIFERENTE A -1 SIGNIFICA QUE EXISTE UN ARCHIVO AL CUAL APUNTE
                        imprimirNumln(i);
                        if((strcmp(DD.dd_array_files[i].dd_file_nombre,"users.txt")==0) && (strcmp(BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str(),"users.txt")==0)){ //SI ES EL ARCHIVO USERS NO VALIDAMOS PERMISOS
                            encontrado=true;
                            //RECUPERAMOS LA ESTRUCTUR INODO DEL ARCHIVO USERS
                            struct Inodos INODO;
                            INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                            for(int j=0; (j<5) && (DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()!=0); j++){
                                if(j==4){
                                    if(INODO.i_ap_indirecto!=-1){//SI ES DIFERENTE A -1 NOS VAMOS A INSERTARLE NUEVOS DATOS
                                        PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                    }else{//EN CASO CONTRARIO SE CREAR Y GUARDA UNA ESTRUCTURA DE INDIRECTOS
                                        int INICIO_BM_INODOS=PV_INICIO_BITMAP_INODOS(DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                                        bool BITMAP_ACTUALIZADO=false;
                                        int IDENTIFICADOR_NINODO=0;
                                        char VERIFICAR_VALOR;
                                        if(ARCHIVO_GLOBAL==NULL){
                                            ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_disco.c_str(),"rb+");
                                            for(int k=INICIO_BM_INODOS; (k<(INICIO_BM_INODOS+(5*DATOS_PIVOTE[0].valor_N))) && (BITMAP_ACTUALIZADO==false); k++){
                                                VERIFICAR_VALOR=00;
                                                fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                                fread(&VERIFICAR_VALOR,1,1,ARCHIVO_GLOBAL);
                                                if(VERIFICAR_VALOR==48){
                                                    char ACTUALIZAR=49;
                                                    BITMAP_ACTUALIZADO=true;
                                                    fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                                    fwrite(&ACTUALIZAR,1,1,ARCHIVO_GLOBAL);
                                                    fflush(ARCHIVO_GLOBAL);
                                                    fclose(ARCHIVO_GLOBAL);
                                                    ARCHIVO_GLOBAL=NULL;
                                                    //ACTUALIZANDO EL INODO
                                                    INODO.i_ap_indirecto=IDENTIFICADOR_NINODO;
                                                    PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                                                    //CREANDO LA NUEVA ESTRUCTURA INODO PARA EL INDIRECTO
                                                    struct Inodos NINODO;
                                                    NINODO.i_count_inodo=IDENTIFICADOR_NINODO;
                                                    NINODO.i_ap_indirecto=-1;
                                                    //NINODO.i_id_proper=atoi((to_string(DATOS_PIVOTE[0].u_id)+to_string(DATOS_PIVOTE[0].g_id)).c_str());
                                                    NINODO.i_id_proper=DATOS_PIVOTE[0].u_id;
                                                    for(int z=0; z<4; z++){
                                                        NINODO.i_array_bloques[z]=-1;
                                                    }
                                                    PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,INODO.i_ap_indirecto,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,NINODO);
                                                    PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                                }
                                                IDENTIFICADOR_NINODO++;
                                            }
                                            if(ARCHIVO_GLOBAL!=NULL){
                                                fflush(ARCHIVO_GLOBAL);
                                                fclose(ARCHIVO_GLOBAL);
                                                ARCHIVO_GLOBAL=NULL;
                                            }
                                        }else{
                                            imprimirln(".... ARCHIVO GLOBAL ABIERTO 18....");
                                        }
                                    }
                                }else{
                                    if(INODO.i_array_bloques[j]!=-1){//SI ES DIFERENTE A -1 SE ACTUALIZA Y GUARDA EL BLOQUE DATOS
                                        struct Bloque_Datos BD;
                                        strcpy(BD.db_data,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());
                                        PV_GUARDAR_BLOQUE_DATOS(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[j],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,BD);
                                        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                    }else{//EN CASO CONTRARIO SE CREA Y GUARDA UN NUEVO BLOQUE DE DATOS ACTUALIZADO
                                        int inicio_bmp_bloques=PV_INICIO_BITMAP_BLOQUE(DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                                        bool bitmap_actualizado=false;
                                        int IDENTIFICADOR_NBLOQUE=0;
                                        char VERIFICAR_VALOR;
                                        if(ARCHIVO_GLOBAL==NULL){
                                            ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_disco.c_str(),"rb+");
                                            for(int k=inicio_bmp_bloques;(k<(inicio_bmp_bloques+(20*DATOS_PIVOTE[0].valor_N))) && (bitmap_actualizado==false); k++){
                                                VERIFICAR_VALOR=00;
                                                fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                                fread(&VERIFICAR_VALOR,1,1,ARCHIVO_GLOBAL);
                                                if(VERIFICAR_VALOR==48){
                                                    //ACTUALIZO EL BITMAP DE BLOQUE DATOS
                                                    char ACTUALIZACION=49;
                                                    bitmap_actualizado=true;
                                                    fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                                    fwrite(&ACTUALIZACION,1,1,ARCHIVO_GLOBAL);
                                                    fflush(ARCHIVO_GLOBAL);
                                                    fclose(ARCHIVO_GLOBAL);
                                                    ARCHIVO_GLOBAL=NULL;
                                                    //ACTUALIZO INODO
                                                    INODO.i_array_bloques[j]=IDENTIFICADOR_NBLOQUE;
                                                    PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                                                    //CREAO EL BLOQUE DATO Y LO GUARDO
                                                    struct Bloque_Datos BD2;
                                                    strcpy(BD2.db_data,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());
                                                    PV_GUARDAR_BLOQUE_DATOS(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[j],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,BD2);
                                                    DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                                }
                                                IDENTIFICADOR_NBLOQUE++;
                                            }
                                            if(ARCHIVO_GLOBAL!=NULL){
                                                fflush(ARCHIVO_GLOBAL);
                                                fclose(ARCHIVO_GLOBAL);
                                                ARCHIVO_GLOBAL=NULL;
                                            }
                                        }else{
                                            imprimirln(".... ARCHIVO GLOBAL ABIERTO 17....");
                                        }
                                    }
                                }
                            }
                        }else{//SI NO LO ES, HAY QUE VALIDAR LOS PERMISOS Y VERIFICAR SI ES EL ARCHIVO A EDITAR
                            if(strcmp(DD.dd_array_files[i].dd_file_nombre,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
                                encontrado=true;
                               // imprimirln("ESTO NO DEBERIA PASAR POR AHORA UNICAMENTE SI ES EL ARCHIVO...");
                                struct Inodos INODO;
                                INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                //VALIDANDO PERMISOS PARA LA CREACION DE BLOQUES E INODOS
                                if((PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==2) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==3) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==6) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==7)){
                                    for(int k=0; (k<5) && (DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()!=0); k++){
                                        if(k==4){
                                            if(INODO.i_ap_indirecto!=-1){
                                                PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                            }else{
                                                INODO.i_ap_indirecto=PV_CREAR_INODO_PARA_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].u_id);
                                                PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                                                PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                            }
                                        }else{
                                            if(INODO.i_array_bloques[k]!=-1){
                                                struct Bloque_Datos BD;
                                                strcpy(BD.db_data,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());
                                                PV_GUARDAR_BLOQUE_DATOS(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[k],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,BD);
                                                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                            }else{
                                                INODO.i_array_bloques[k]=PV_CREAR_BLOQUE_DATOS();
                                                PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                                            }
                                        }
                                    }
                                }else{
                                    imprimirln("=== ERROR: NO TIENE PERMISOS DE ESCRITURA ARCHIVO ===");
                                }
                            }
                        }
                    }
                }
            }

        }
    }else{//EN CASO CONTRARIO ES UNA CARPETA
        for(int i=0; (i<7) && (encontrado==false); i++){
            if(i==6){
                if(AD.avd_ap_arbol_virtual_directorio!=-1){
                    PV_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_BUSCAR_RUTA);
                }
            }else{
                if(AD.avd_ap_array_subdirectorios[i]!=-1){
                    struct Arbol_Directorio AD2;
                    AD2=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

                    if(strcmp(AD2.avd_nombre_directorio,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
                        encontrado=true;
                        if((PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==2) || (PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==3) || (PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==6) || (PERMISO_ESCRITURA_LECTURA(AD2.avd_permisos,AD2.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD2.avd_proper))==7)){
                            if(POSICION_BUSCAR_RUTA!=(BUSCAR_RUTA.size()-1)){
                                PV_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_BUSCAR_RUTA+1));
                            }
                        }else{
                            imprimirln("=== ERROR: NO TIEME PERMISOS DE ESCRITURA CARPETAS ===");
                        }
                    }
                }
            }
        }
    }*/

    return encontrado;
}

bool PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(vector<string> BUSCAR_RUTA,int I_INODO,int POSICION_BUSCAR_RUTA,int PERMISOS_DD){
    bool respuesta=false;
    struct Inodos INODO;
    INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,I_INODO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    for(int j=0; (j<5) && (DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()!=0); j++){
//-------------codigo copiado
                                if(j==4){
                                    if(INODO.i_ap_indirecto!=-1){//SI ES DIFERENTE A -1 NOS VAMOS A INSERTARLE NUEVOS DATOS
                                        PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,PERMISOS_DD);
                                    }else{//EN CASO CONTRARIO SE CREAR Y GUARDA UNA ESTRUCTURA DE INDIRECTOS
                                        int INICIO_BM_INODOS=PV_INICIO_BITMAP_INODOS(DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                                        bool BITMAP_ACTUALIZADO=false;
                                        int IDENTIFICADOR_NINODO=0;
                                        char VERIFICAR_VALOR;
                                        if(ARCHIVO_GLOBAL==NULL){
                                            ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_disco.c_str(),"rb+");
                                            for(int k=INICIO_BM_INODOS; (k<(INICIO_BM_INODOS+(5*DATOS_PIVOTE[0].valor_N))) && (BITMAP_ACTUALIZADO==false); k++){
                                                VERIFICAR_VALOR=00;
                                                fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                                fread(&VERIFICAR_VALOR,1,1,ARCHIVO_GLOBAL);
                                                if(VERIFICAR_VALOR==48){
                                                    char ACTUALIZAR=49;
                                                    BITMAP_ACTUALIZADO=true;
                                                    fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                                    fwrite(&ACTUALIZAR,1,1,ARCHIVO_GLOBAL);
                                                    fflush(ARCHIVO_GLOBAL);
                                                    fclose(ARCHIVO_GLOBAL);
                                                    ARCHIVO_GLOBAL=NULL;
                                                    //ACTUALIZANDO EL INODO
                                                    INODO.i_ap_indirecto=IDENTIFICADOR_NINODO;
                                                    PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,I_INODO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                                                    //CREANDO LA NUEVA ESTRUCTURA INODO PARA EL INDIRECTO
                                                    struct Inodos NINODO;
                                                    NINODO.i_count_inodo=IDENTIFICADOR_NINODO;
                                                    NINODO.i_ap_indirecto=-1;
                                                    //NINODO.i_id_proper=atoi((to_string(DATOS_PIVOTE[0].u_id)+to_string(DATOS_PIVOTE[0].g_id)).c_str());
                                                    NINODO.i_id_proper=DATOS_PIVOTE[0].g_id;
                                                    for(int z=0; z<4; z++){
                                                        NINODO.i_array_bloques[z]=-1;
                                                    }
                                                    PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,INODO.i_ap_indirecto,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,NINODO);
                                                    PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,PERMISOS_DD);
                                                }
                                                IDENTIFICADOR_NINODO++;
                                            }
                                            if(ARCHIVO_GLOBAL!=NULL){
                                                fflush(ARCHIVO_GLOBAL);
                                                fclose(ARCHIVO_GLOBAL);
                                                ARCHIVO_GLOBAL=NULL;
                                            }
                                        }else{
                                            imprimirln(".... ARCHIVO GLOBAL ABIERTO 18....");
                                        }
                                    }
                                }else{
                                    if(INODO.i_array_bloques[j]!=-1){//SI ES DIFERENTE A -1 SE ACTUALIZA Y GUARDA EL BLOQUE DATOS
                                        struct Bloque_Datos BD;
                                        strcpy(BD.db_data,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());
                                        PV_GUARDAR_BLOQUE_DATOS(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[j],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,BD);
                                        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                    }else{//EN CASO CONTRARIO SE CREA Y GUARDA UN NUEVO BLOQUE DE DATOS ACTUALIZADO
                                        int inicio_bmp_bloques=PV_INICIO_BITMAP_BLOQUE(DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                                        bool bitmap_actualizado=false;
                                        int IDENTIFICADOR_NBLOQUE=0;
                                        char VERIFICAR_VALOR;
                                        if(ARCHIVO_GLOBAL==NULL){
                                            ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_disco.c_str(),"rb+");
                                            for(int k=inicio_bmp_bloques;(k<(inicio_bmp_bloques+(20*DATOS_PIVOTE[0].valor_N))) && (bitmap_actualizado==false); k++){
                                                VERIFICAR_VALOR=00;
                                                fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                                fread(&VERIFICAR_VALOR,1,1,ARCHIVO_GLOBAL);
                                                if(VERIFICAR_VALOR==48){
                                                    //ACTUALIZO EL BITMAP DE BLOQUE DATOS
                                                    char ACTUALIZACION=49;
                                                    bitmap_actualizado=true;
                                                    fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                                    fwrite(&ACTUALIZACION,1,1,ARCHIVO_GLOBAL);
                                                    fflush(ARCHIVO_GLOBAL);
                                                    fclose(ARCHIVO_GLOBAL);
                                                    ARCHIVO_GLOBAL=NULL;
                                                    //ACTUALIZO INODO
                                                    INODO.i_array_bloques[j]=IDENTIFICADOR_NBLOQUE;
                                                    PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,I_INODO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                                                    //CREAO EL BLOQUE DATO Y LO GUARDO
                                                    struct Bloque_Datos BD2;
                                                    strcpy(BD2.db_data,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());
                                                    PV_GUARDAR_BLOQUE_DATOS(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[j],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,BD2);
                                                    DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                                }
                                                IDENTIFICADOR_NBLOQUE++;
                                            }
                                            if(ARCHIVO_GLOBAL!=NULL){
                                                fflush(ARCHIVO_GLOBAL);
                                                fclose(ARCHIVO_GLOBAL);
                                                ARCHIVO_GLOBAL=NULL;
                                            }
                                        }else{
                                            imprimirln(".... ARCHIVO GLOBAL ABIERTO 17....");
                                        }
                                    }
                                }
//------------codigo copiado
    }

    return respuesta;
}

bool PV_INDIRECTO_DD_ESCRIBIR_TEXTO_ARCHIVO(vector<string> BUSCAR_RUTA,int I_DD, int POSICION_BUSCAR_RUTA){
    bool encontrado=false;
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    for(int i=0; (i<6) && (encontrado==false); i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                PV_INDIRECTO_DD_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,DD.dd_ap_detalle_directorio,POSICION_BUSCAR_RUTA);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                if(strcmp(DD.dd_array_files[i].dd_file_nombre,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
                    encontrado=true;
                    struct Inodos INODO;
                    INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                    if((PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==2) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==3) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==6) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==7)){
//---
                                    for(int k=0; (k<5) && (DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()!=0); k++){
                                        if(k==4){
                                            if(INODO.i_ap_indirecto!=-1){
                                                PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                            }else{
                                                INODO.i_ap_indirecto=PV_CREAR_INODO_PARA_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].u_id);
                                                PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                                                PV_INDIRECTO_INODO_ESCRIBIR_TEXTO_ARCHIVO(BUSCAR_RUTA,INODO.i_ap_indirecto,POSICION_BUSCAR_RUTA,DD.dd_array_files[i].permisos);
                                            }
                                        }else{
                                            if(INODO.i_array_bloques[k]!=-1){
                                                struct Bloque_Datos BD;
                                                strcpy(BD.db_data,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());
                                                PV_GUARDAR_BLOQUE_DATOS(DATOS_PIVOTE[0].inicio_particion,INODO.i_array_bloques[k],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,BD);
                                                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                            }else{
                                                INODO.i_array_bloques[k]=PV_CREAR_BLOQUE_DATOS();
                                                PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                                            }
                                        }
                                    }
//--
                    }else{
                        imprimirln("=== ERROR: NO TIENE PERMISOS DE ESCRITURA ARCHIVOS ===");
                    }
                }
            }
        }
    }

    return encontrado;
}

//--------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------METODO PARA CREAR ARCHIVO/CARPETA ---------------------------------------------------------------------
bool PV_CREAR_ARCHIVO_CARPETA(vector<string> BUSCAR_RUTA,int I_ARBOLDETALLE, int POSICION_BUSCAR_RUTA){
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_ARBOLDETALLE,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    bool AC_CREADO=false;
  /*  imprimirln("DATOS CON LOS QUE SE CUENTAN PARA LOS PERMISOS");
    imprimirNumln(AD.avd_permisos);
    imprimirNumln(AD.avd_proper);
    imprimirNumln(PV_GID_USUARIO_PROPIETARIO(AD.avd_proper));
    imprimirNumln(DATOS_PIVOTE[0].u_id);
    imprimirNumln(DATOS_PIVOTE[0].g_id);*/

    if(POSICION_BUSCAR_RUTA<BUSCAR_RUTA.size()){//SI ENTRA AQUI SIGNIFICA QUE NO A EXCEDIDO DE LAS RUTAS

        if((PERMISO_ESCRITURA_LECTURA(AD.avd_permisos,AD.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD.avd_proper))==2) || (PERMISO_ESCRITURA_LECTURA(AD.avd_permisos,AD.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD.avd_proper))==3) || (PERMISO_ESCRITURA_LECTURA(AD.avd_permisos,AD.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD.avd_proper))==6) || (PERMISO_ESCRITURA_LECTURA(AD.avd_permisos,AD.avd_proper,PV_GID_USUARIO_PROPIETARIO(AD.avd_proper))==7)){
        //---------
            if(PV_EXISTE_ARCHIVO_CARPETA(BUSCAR_RUTA,I_ARBOLDETALLE,POSICION_BUSCAR_RUTA)){//SI EXISTE EL ARCHIO O CARPETA ENTRA AQUI
           //     imprimirln("SI EXISTE EL ARCHIVO/CARPETA");
                if(VALIDAR_EXTENSION_SINMENSAJE(BUSCAR_RUTA[POSICION_BUSCAR_RUTA])){//SI ENTRA AQUI ES UN ARCHIVO
           //         imprimirln("VALIDADO ARHIIVO EXISTE");
                }else{//EN CASO CONTRARIO ES UNA CARPETA
               //     imprimirln("VALIDADO EXISTE CARPETA");
                    //TENEMOS QUE RECUPERAR EL ARBOL_DIRECTORIO DE ESTA CARPETA PARA QUE RECORRA LA RAMA DE ESE ARBOL
                    if(POSICION_BUSCAR_RUTA!=(BUSCAR_RUTA.size()-1)){
                    //--
                        for(int i=0; (i<7) && (AC_CREADO==false); i++){
                            if(i==6){
                                if(AD.avd_ap_arbol_virtual_directorio!=-1){
                                    AC_CREADO=PV_CREAR_ARCHIVO_CARPETA(BUSCAR_RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_BUSCAR_RUTA);
                                }
                            }else{
                                if(AD.avd_ap_array_subdirectorios[i]!=-1){
                                    struct Arbol_Directorio BUSCAR;
                                    BUSCAR=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                    if(strcmp(BUSCAR.avd_nombre_directorio,BUSCAR_RUTA[POSICION_BUSCAR_RUTA].c_str())==0){
                                     //   imprimirln("CARPETA PADRE ENCONTRADA...");
                                     //   cout<<BUSCAR.avd_nombre_directorio<<endl;
                                        AC_CREADO=PV_CREAR_ARCHIVO_CARPETA(BUSCAR_RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_BUSCAR_RUTA+1));
                                    }
                                }
                            }
                        }
                    //--
                    }else{
                      //  imprimirln("FIN DEL VECTOR DIRECCION");
                    }
                }
            }else if((DATOS_PIVOTE[0]._p_archivo!="") || (POSICION_BUSCAR_RUTA==(BUSCAR_RUTA.size()-1))){//NO EXISTE EL ARCHIVO O CARPETA
        //        imprimirln("PERMISOS CONCEDIDOS PARA CREAR CARPETA/ARCHIVO");
                if(VALIDAR_EXTENSION_SINMENSAJE(BUSCAR_RUTA[POSICION_BUSCAR_RUTA])){//SI ENTRA ES ARCHIVO
                    if(AD.avd_ap_detalle_directorio!=-1){//TIENE ARCHIVOS
                        AC_CREADO=PV_DETALLE_DIRECTORIO_CREAR_ARCHIVO_CARPET(BUSCAR_RUTA,AD.avd_ap_detalle_directorio,POSICION_BUSCAR_RUTA);
                    }else{//NO TIENE ARCHIVOS
                        AD.avd_ap_detalle_directorio=PV_CREAR_DETALLE_DIRECTORIO();
                        PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_ARBOLDETALLE,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,AD);
                        AC_CREADO=PV_CREAR_ARCHIVO_CARPETA(BUSCAR_RUTA,I_ARBOLDETALLE,POSICION_BUSCAR_RUTA);
                    }
                }else{//EN CASO CONTRARIO ES CARPETA
                    for(int i=0; (i<7) && (AC_CREADO==false); i++){
                        if(i==6){
                            if(AD.avd_ap_arbol_virtual_directorio!=-1){
                                //SI ENTRA AQUI SIGNIFICA QUE NO ENCONTRO UN ESPACIO LIBRE, PERO TIENE MAS APUNTADORES PARA SER USADOS
                                AC_CREADO=PV_CREAR_ARCHIVO_CARPETA(BUSCAR_RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_BUSCAR_RUTA);
                            }else{
                                //SI NO SE TIENE QUE CREARR UN NUEVO ARBOL_DIRECTORIO PARA MAS APUNTADORES
                                AD.avd_ap_arbol_virtual_directorio=PV_CREAR_ARBOL_DIRECTORIO(AD.avd_nombre_directorio,AD.avd_proper,AD.avd_permisos);
                                PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_ARBOLDETALLE,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,AD);
                                AC_CREADO=PV_CREAR_ARCHIVO_CARPETA(BUSCAR_RUTA,I_ARBOLDETALLE,POSICION_BUSCAR_RUTA);
                            }
                        }else{
                            //SE BUSCA SI TIENE UN APUNTADOR DE DIRECTORIO LIBRE PARA CREAR LA CARPETA
                            if(AD.avd_ap_array_subdirectorios[i]==-1){
                                AD.avd_ap_array_subdirectorios[i]=PV_CREAR_ARBOL_DIRECTORIO(BUSCAR_RUTA[POSICION_BUSCAR_RUTA],DATOS_PIVOTE[0].u_id,664);
                                PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_ARBOLDETALLE,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,AD);

                                if(POSICION_BUSCAR_RUTA!=(BUSCAR_RUTA.size()-1)){
                                    AC_CREADO=PV_CREAR_ARCHIVO_CARPETA(BUSCAR_RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_BUSCAR_RUTA+1));
                                }else{
                                    AC_CREADO=true;
                                    //SI ENTRA AQUI SIGNIFICA QUE CREO TODAS LAS CARPETAS PADRE Y LA CARPETA ESPECIFICA DE MKDIR
                                    imprimirln("++++ CARPETA CREADA EXITOSAMENTE ++++");
                                   // PV_CREAR_Y_ACTUALIZAR_BITACORA(1,DATOS_PIVOTE[0]._p_carpeta,DATOS_PIVOTE[0]._p_archivo,OBTENER_RUTA_COMPLETA_JOURNALING(BUSCAR_RUTA));
                                    PV_CREAR_Y_ACTUALIZAR_BITACORA(1,DATOS_PIVOTE[0]._p_carpeta,"p",OBTENER_RUTA_COMPLETA_JOURNALING(BUSCAR_RUTA));
                                }
                            }
                        }
                    }
                }

            }else{//EN CASO DE QUE NO EXISTA
                imprimirln("=== ERROR: CARPETA PADRE NO EXISTENTE PARA CREAR EL ARCHIVO ===");
            }
        //---------
        }else{
            imprimirln("=== ERROR: NO CUENTA CON PERMISOS DE ESCRITURA");
        }
    }else{
        imprimirln("=== ERROR: EXCEDIO LIMITES DE DIRECCION/RUTA ===");
    }

    return AC_CREADO;
}

bool PV_DETALLE_DIRECTORIO_CREAR_ARCHIVO_CARPET(vector<string> BUSCAR_RUTA,int I_DETALLE_DIRECTORIO, int POSICION_RUTA){
  //  imprimirln("ENTRA A CCA DD");
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DETALLE_DIRECTORIO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    bool DD_E_INODO_ACTUALIZADO=false;
    for(int i=0; (i<6) && (DD_E_INODO_ACTUALIZADO==false); i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){//SE LLAMA A SI MISMO RECURSIVAMENTE
                DD_E_INODO_ACTUALIZADO=PV_DETALLE_DIRECTORIO_CREAR_ARCHIVO_CARPET(BUSCAR_RUTA,DD.dd_ap_detalle_directorio,POSICION_RUTA);
            }else{//SE LE CREA UNO, SE ACTUALIZA Y GUARDA Y LUEGO SE LLAMA RECURSIVAMENTE
                DD.dd_ap_detalle_directorio=PV_CREAR_DETALLE_DIRECTORIO();
                PV_GUARDAR_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_DETALLE_DIRECTORIO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,DD);
                DD_E_INODO_ACTUALIZADO=PV_DETALLE_DIRECTORIO_CREAR_ARCHIVO_CARPET(BUSCAR_RUTA,I_DETALLE_DIRECTORIO,POSICION_RUTA);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo==-1){//BUSCAMOS UN ESPACIO LIBRE PARA CREAR EL NUEVO INODO DE ARCHIVOS Y  ACTUALIZAR EL DETALLE DE DIRECTORIO ACTUAL
                srand(time(NULL));
                strcpy(DD.dd_array_files[i].dd_file_nombre,BUSCAR_RUTA[POSICION_RUTA].c_str());
                DD.dd_array_files[i].dd_file_date_creacion=time(NULL);
                DD.dd_array_files[i].dd_file_date_modificacion=time(NULL);
                DD.dd_array_files[i].permisos=664;
                DD.dd_array_files[i].dd_file_ap_inodo=PV_CREAR_INODO_PARA_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].u_id);
                PV_GUARDAR_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_DETALLE_DIRECTORIO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,DD);
                DD_E_INODO_ACTUALIZADO=true;
                //AQUI TAMBIEN SE COLOCA PARA INDICAR QUE SE ACTUALIZA EL JOURNAL CON EL MKDIR
                //PV_CREAR_Y_ACTUALIZAR_BITACORA(0,DATOS_PIVOTE[0]._p_carpeta,DATOS_PIVOTE[0]._p_archivo,OBTENER_RUTA_COMPLETA_JOURNALING(BUSCAR_RUTA));
                PV_CREAR_Y_ACTUALIZAR_BITACORA(0,DATOS_PIVOTE[0]._p_carpeta,"p",OBTENER_RUTA_COMPLETA_JOURNALING(BUSCAR_RUTA));
            }else{
              //  imprimirln("IMPRIMIENDO EJEMPLO ARCHIVOS EXISTENTES");
              //  cout<<DD.dd_array_files[i].dd_file_nombre<<endl;
            }
        }
    }
    return DD_E_INODO_ACTUALIZADO;
}
//--------------------------------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------METODOS DE CREACION DE ESTRUCTURAS ----------------------------------------------------------
int PV_CREAR_INODO_PARA_DETALLE_DIRECTORIO(int id_propietario){
    int INICIO_BM_INODOS=PV_INICIO_BITMAP_INODOS(DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
    bool INODO_CREADO=false;
    int I_INODO=0;
    char VERIFICAR_VALOR;
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_disco.c_str(),"rb+");

        for(int i=INICIO_BM_INODOS; (i<(INICIO_BM_INODOS+(5*DATOS_PIVOTE[0].valor_N))) && (INODO_CREADO==false); i++){
            VERIFICAR_VALOR=00;
            fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
            fread(&VERIFICAR_VALOR,1,1,ARCHIVO_GLOBAL);
            if(VERIFICAR_VALOR==48){
                char ACTUALIZAR_BITMAP=49;
                fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
                fwrite(&ACTUALIZAR_BITMAP,1,1,ARCHIVO_GLOBAL);
                INODO_CREADO=true;
                fflush(ARCHIVO_GLOBAL);
                fclose(ARCHIVO_GLOBAL);
                ARCHIVO_GLOBAL=NULL;
                //CREANMOS EL INODO QUE USARA
                struct Inodos INODO;
                INODO.i_count_inodo=I_INODO;
                INODO.i_count_bloques_asignados=4;
                INODO.i_ap_indirecto=-1;
                INODO.i_id_proper=id_propietario;
                for(int j=0; j<4; j++){
                    INODO.i_array_bloques[j]=-1;
                }
                PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,I_INODO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
            }else{
                I_INODO++;
            }
        }

        if(ARCHIVO_GLOBAL!=NULL){
            fflush(ARCHIVO_GLOBAL);
            fclose(ARCHIVO_GLOBAL);
            ARCHIVO_GLOBAL=NULL;
        }

    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 19....");
    }

    return I_INODO;
}

int PV_CREAR_DETALLE_DIRECTORIO(){
    int INICIO_BM_DD=PV_INICIO_BITMAP_DD(DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
    bool DD_CREADO=false;
    int I_DD=0;
    char VERIFICAR_VALOR;
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_disco.c_str(),"rb+");

        for(int i=INICIO_BM_DD; (i<(INICIO_BM_DD+DATOS_PIVOTE[0].valor_N)) && (DD_CREADO==false); i++){
            VERIFICAR_VALOR=00;
            fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
            fread(&VERIFICAR_VALOR,1,1,ARCHIVO_GLOBAL);
            if(VERIFICAR_VALOR==48){
                char ACTUALIZAR_BM=49;
                fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
                fwrite(&ACTUALIZAR_BM,1,1,ARCHIVO_GLOBAL);
                DD_CREADO=true;
                fflush(ARCHIVO_GLOBAL);
                fclose(ARCHIVO_GLOBAL);
                ARCHIVO_GLOBAL=NULL;
                //CREANDO EL DETALLE DE DIRECTORIO Y GUARDANDOLO
                struct Detalle_Directorio DD;

                DD.dd_ap_detalle_directorio=-1;
                for(int j=0; j<5; j++){
                    DD.dd_array_files[j].dd_file_ap_inodo=-1;
                }
                PV_GUARDAR_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,DD);
            }else{
                I_DD++;
            }
        }

        if(ARCHIVO_GLOBAL!=NULL){
            fflush(ARCHIVO_GLOBAL);
            fclose(ARCHIVO_GLOBAL);
            ARCHIVO_GLOBAL=NULL;
        }

    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 20....");
    }

    return I_DD;
}

int PV_CREAR_ARBOL_DIRECTORIO(string Nombre_directorio, int propietario,int permisos){
    int INICIO_BM_AD=PV_INICIO_BITMAP_AD(DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
    bool AD_CREADO=false;
    srand(time(NULL));
    int I_AD=0;
    char VERIFICAR_VALOR;
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_disco.c_str(),"rb+");

        for(int i=INICIO_BM_AD; (i<(INICIO_BM_AD+DATOS_PIVOTE[0].valor_N)) && (AD_CREADO==false); i++){
            VERIFICAR_VALOR=00;
            fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
            fread(&VERIFICAR_VALOR,1,1,ARCHIVO_GLOBAL);
            if(VERIFICAR_VALOR==48){
                char ACTUALIZAR_BM=49;
                fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
                fwrite(&ACTUALIZAR_BM,1,1,ARCHIVO_GLOBAL);
                AD_CREADO=true;
                fflush(ARCHIVO_GLOBAL);
                fclose(ARCHIVO_GLOBAL);
                ARCHIVO_GLOBAL=NULL;
                //CREANDO EL ARBOL DE DIRECTORIO Y GUARDANDOLO
                struct Arbol_Directorio AD2;
                AD2.avd_proper=propietario;
                AD2.avd_permisos=permisos;
                strcpy(AD2.avd_nombre_directorio,Nombre_directorio.c_str());
                AD2.avd_fecha_creacion=time(NULL);
               // AD2.avd_ap_detalle_directorio=-1;
                AD2.avd_ap_detalle_directorio=PV_CREAR_DETALLE_DIRECTORIO();
                AD2.avd_ap_arbol_virtual_directorio=-1;
                for(int j=0; j<6; j++){
                    AD2.avd_ap_array_subdirectorios[j]=-1;
                }
                PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,AD2);
            }else{
                I_AD++;
            }
        }

        if(ARCHIVO_GLOBAL!=NULL){
            fflush(ARCHIVO_GLOBAL);
            fclose(ARCHIVO_GLOBAL);
            ARCHIVO_GLOBAL=NULL;
        }
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 21....");
    }
    return I_AD;
}

int PV_CREAR_BLOQUE_DATOS(){
    int INICIO_BM_BD=PV_INICIO_BITMAP_BLOQUE(DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
    bool B_CREADO=false;
    int I_BLOQUE=0;
    char VERIFICAR_VALOR;

    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_disco.c_str(),"rb+");

        for(int i=INICIO_BM_BD; (i<(INICIO_BM_BD+(20*DATOS_PIVOTE[0].valor_N))) && (B_CREADO==false); i++){
            VERIFICAR_VALOR=00;
            fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
            fread(&VERIFICAR_VALOR,1,1,ARCHIVO_GLOBAL);

            if(VERIFICAR_VALOR==48){
                char ACTUALIZAR_BITMAP=49;
                fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
                fwrite(&ACTUALIZAR_BITMAP,1,1,ARCHIVO_GLOBAL);
                B_CREADO=true;
                fflush(ARCHIVO_GLOBAL);
                fclose(ARCHIVO_GLOBAL);
                ARCHIVO_GLOBAL=NULL;
                //CREAR BLOQUE Y ALMACENARLO
                struct Bloque_Datos BDATOS;
                strcpy(BDATOS.db_data,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());
                PV_GUARDAR_BLOQUE_DATOS(DATOS_PIVOTE[0].inicio_particion,I_BLOQUE,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,BDATOS);
                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
            }else{
                I_BLOQUE++;
            }
        }

        if(ARCHIVO_GLOBAL!=NULL){
            fflush(ARCHIVO_GLOBAL);
            fclose(ARCHIVO_GLOBAL);
            ARCHIVO_GLOBAL=NULL;
        }

    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 22....");
    }

    return I_BLOQUE;
}


void PV_CREAR_Y_ACTUALIZAR_BITACORA(int TIPO,string contenido_size, string padres,string ruta){
    srand(time(NULL));
    struct Bitacora BIT_ARCHIVO;
    if(TIPO==0){//ARCHIVO
        BIT_ARCHIVO.log_tipo=0;
    }else{//CARPETA
        BIT_ARCHIVO.log_tipo=1;
    }
    strcpy(BIT_ARCHIVO.log_tipo_operacion,ruta.c_str());
    strcpy(BIT_ARCHIVO.log_nombre,padres.c_str());
    strcpy(BIT_ARCHIVO.log_contenido,contenido_size.c_str());
    BIT_ARCHIVO.log_fecha=time(NULL);

    PV_GUARDAR_BITACORA(BIT_ARCHIVO,DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
}

//--------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------METODOS PARA EL LOSS Y RECOVERY -----------------------------------------------------------------
void PV_FORMATEO_LOST_PERDIDA(int inicio_particion,int valor_n, string path_disco){
    int FINAL_FORMATEO_PERDIDA=inicio_particion+sizeof(SUPER_BOOT)+valor_n+(valor_n*sizeof(Arbol_Directorio))+valor_n+(valor_n*sizeof(Detalle_Directorio))+(5*valor_n)+(5*valor_n*sizeof(Inodos))+(20*valor_n)+(20*valor_n*sizeof(Bloque_Datos));
    char VALOR_ESCRIBIR=48;
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
        fseek(ARCHIVO_GLOBAL,inicio_particion,SEEK_SET);
        for(int i=inicio_particion;i<FINAL_FORMATEO_PERDIDA; i++){
            fwrite(&VALOR_ESCRIBIR,1,1,ARCHIVO_GLOBAL);
        }
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;

    }else{
        imprimirln("....ARCHIVO ABIERTO 23....");
    }
}


bool PV_HUBO_FORMATEO_LOSS(){
    string RUTA="/users.txt";
    vector<string> DIRECCION=OBTENER_DIRECCION(RUTA,"/");
    return PV_EXISTE_ARCHIVO_CARPETA(DIRECCION,0,0);
}

//--------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------ METODOS PARA CAMBIAR NOMBRE A UN ARCHIVO/CARPETA ------------------------------------------------
void PV_CAMBIAR_NOMBRE_AC(vector<string> RUTA,int I_AD,int POSICION_RUTA,string NUEVO_NOMBRE){
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    if(POSICION_RUTA<RUTA.size()){
        if(PV_EXISTE_ARCHIVO_CARPETA(RUTA,I_AD,POSICION_RUTA)){
            if(VALIDAR_EXTENSION_SINMENSAJE(RUTA[POSICION_RUTA])){//ARCHIVO
                if(AD.avd_ap_detalle_directorio!=-1){
                    string DIRECCION="/"+NUEVO_NOMBRE;
                    vector<string> TMP_DIR=OBTENER_DIRECCION(DIRECCION,"/");
                    if(!PV_EXISTE_ARCHIVO_CARPETA(TMP_DIR,I_AD,0)){
                        if(VALIDAR_EXTENSION_SINMENSAJE(NUEVO_NOMBRE)){
                            PV_CAMBIAR_NOMBRE_DD(RUTA,AD.avd_ap_detalle_directorio,POSICION_RUTA,NUEVO_NOMBRE);
                        }else{
                            imprimirln("=== ERROR: EL NUEVO NOMBRE PARA EL ARCHIVO NO CUENTA CON UNA EXTENSION ===");
                        }
                    }else{
                        imprimirln("=== ERROR: YA EXISTE UN ARCHIVO CON EL NAME ESPECIFICADO ===");
                    }
                }
            }else{//CARPETA
                bool ENCONTRADO=false;
                for(int i=0; (i<7) && (ENCONTRADO==false); i++){
                    if(i==6){
                        if(AD.avd_ap_arbol_virtual_directorio!=-1){
                            PV_CAMBIAR_NOMBRE_AC(RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_RUTA,NUEVO_NOMBRE);
                        }
                    }else{
                        if(AD.avd_ap_array_subdirectorios[i]!=-1){
                            struct Arbol_Directorio TEMPORAL;
                            TEMPORAL=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                            if(strcmp(TEMPORAL.avd_nombre_directorio,RUTA[POSICION_RUTA].c_str())==0){
                                ENCONTRADO=true;
                                if(POSICION_RUTA==(RUTA.size()-1)){
                                    string DIRECCION="/"+NUEVO_NOMBRE;
                                    vector<string> TMP_DIR=OBTENER_DIRECCION(DIRECCION,"/");
                                    if(!PV_EXISTE_ARCHIVO_CARPETA(TMP_DIR,I_AD,0)){
                                        if((PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==2) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==3) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==6) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==7)){
                                            if(!VALIDAR_EXTENSION_SINMENSAJE(NUEVO_NOMBRE)){
                                                strcpy(TEMPORAL.avd_nombre_directorio,NUEVO_NOMBRE.c_str());
                                                PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,TEMPORAL);
                                                imprimirln("++++ NOMBRE DE CARPETA CAMBIADO EXITOSAMENTE ++++");
                                            }else{
                                                imprimirln("=== ERROR: NO SE PERMITE NOMBRES CON EXTENSION PARA LOS NOMBRES DE CARPETA ====");
                                            }
                                        }else{
                                            imprimirln("=== ERROR: NO CUENTA CON PERMISOS DE ESCRITURA EN LA CARPETA ===");
                                        }
                                    }else{
                                        imprimirln("=== ERROR: YA EXISTE UNA CARPETA CON EL NAME ESPECIFICADO ====");
                                    }
                                }else{
                                    PV_CAMBIAR_NOMBRE_AC(RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_RUTA+1),NUEVO_NOMBRE);
                                }
                            }
                        }
                    }
                }
            }
        }else{
            imprimirln("=== ERROR: NO SE ENCONTRO CARPETA/ARCHIVO PARA PODER CAMBIAR NOMBRE ===");
        }
    }else{
        imprimirln("=== ERROR: LIMITE EXCEDIDO DE BUSQUEDA ===");
    }
}

void PV_CAMBIAR_NOMBRE_DD(vector<string> RUTA,int I_DD,int POSICION_RUTA,string NUEVO_NOMBRE){
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    bool ENCONTRADO=false;
    for(int i=0; (i<6) && (ENCONTRADO==false); i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                PV_CAMBIAR_NOMBRE_DD(RUTA,DD.dd_ap_detalle_directorio,POSICION_RUTA,NUEVO_NOMBRE);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                if(strcmp(DD.dd_array_files[i].dd_file_nombre,RUTA[POSICION_RUTA].c_str())==0){
                    ENCONTRADO=true;
                    //VALIDAR PERMISOS
                    struct Inodos INODO;
                    INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                    if((PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==2) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==3) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==6) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==7)){
                        strcpy(DD.dd_array_files[i].dd_file_nombre,NUEVO_NOMBRE.c_str());
                        PV_GUARDAR_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,DD);
                        imprimirln("++++ NOMBRE ARCHIVO CAMBIADO EXITOSAMENTE ++++");
                    }else{
                        imprimirln("=== ERROR: NO CUENTA CON PERMISOS DE ESCRITURA EN ARCHIVO ===");
                    }
                }
            }
        }
    }

}
//--------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------ METODOS PARA CAMBIAR PERMISOS A UN ARCHIVO/CARPETA ----------------------------------------------
void PV_CAMBIAR_PERMISOS_AD(vector<string> RUTA, int I_AD, int POSICION_RUTA,int NUEVOS_PERMISOS, bool MODO_RECURSIVO){
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    if(POSICION_RUTA<RUTA.size()){
        if(PV_EXISTE_ARCHIVO_CARPETA(RUTA,I_AD,POSICION_RUTA)){
            if(VALIDAR_EXTENSION_SINMENSAJE(RUTA[POSICION_RUTA])){//ARCHIVO
                if(AD.avd_ap_detalle_directorio!=-1){
                    PV_CAMBIAR_PERMISOS_DD(RUTA,AD.avd_ap_detalle_directorio,POSICION_RUTA,NUEVOS_PERMISOS);
                }
            }else{
                bool ENCONTRADO=false;
                for(int i=0; (i<7) && (ENCONTRADO==false); i++){
                    if(i==6){
                        if(AD.avd_ap_arbol_virtual_directorio!=-1){
                            PV_CAMBIAR_PERMISOS_AD(RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_RUTA,NUEVOS_PERMISOS,MODO_RECURSIVO);
                        }
                    }else{
                        if(AD.avd_ap_array_subdirectorios[i]!=-1){
                            struct Arbol_Directorio TEMPORAL;
                            TEMPORAL=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                            if(strcmp(TEMPORAL.avd_nombre_directorio,RUTA[POSICION_RUTA].c_str())==0){
                                ENCONTRADO=true;
                                if(POSICION_RUTA==(RUTA.size()-1)){
                                    //VALIDANDO PERMISOS DE ESCRITURA
                                    if((PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==2) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==3) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==6) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==7)){
                                        TEMPORAL.avd_permisos=NUEVOS_PERMISOS;
                                        PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,TEMPORAL);
                                        //VALIDANDO SI HAY QUE HACER TODO DE FORMA RECURSIVA
                                        if(MODO_RECURSIVO){
                                            PV_CAMBIAR_PERMISOS_RECURSIVO_AD(AD.avd_ap_array_subdirectorios[i],NUEVOS_PERMISOS);
                                        }
                                        imprimirln("++++ PERMISOS MODIFICADOS EXITOSAMENTE ++++");
                                    }else{
                                        imprimirln("=== ERROR: NO TIENE PERMISOS DE ESCRITURA EN LA CARPETA ===");
                                    }
                                }else{
                                    PV_CAMBIAR_PERMISOS_AD(RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_RUTA+1),NUEVOS_PERMISOS,MODO_RECURSIVO);
                                }
                            }
                        }
                    }
                }
            }
        }else{
            imprimirln("=== ERROR: ARCHIVO/CARPETA INEXISTENTES ===");
        }
    }else{
        imprimirln("=== ERROR: SE EXCEDIO EN RUTA BUSQUEDA ===");
    }
}

void PV_CAMBIAR_PERMISOS_DD(vector<string> RUTA,int I_DD,int POSICION_RUTA,int NUEVOS_PERMISOS){
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    bool ENCONTRADO=false;

    for(int i=0; (i<6) && (ENCONTRADO==false); i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                PV_CAMBIAR_PERMISOS_DD(RUTA,DD.dd_ap_detalle_directorio,POSICION_RUTA,NUEVOS_PERMISOS);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                if(strcmp(DD.dd_array_files[i].dd_file_nombre,RUTA[POSICION_RUTA].c_str())==0){
                    ENCONTRADO=true;
                    struct Inodos INODO;
                    INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                    if((PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==2) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==3) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==6) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==7)){
                        DD.dd_array_files[i].permisos=NUEVOS_PERMISOS;
                        PV_GUARDAR_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,DD);
                        imprimirln("++++ PERMISOS ARCHIVO MODIFICADO EXITOSAMENTE ++++");
                    }else{
                        imprimirln("=== ERROR: NO TIENE PERMISOS DE ESCRITURA SOBRE EL ARCHIVO ===");
                    }
                }
            }
        }
    }

}

void PV_CAMBIAR_PERMISOS_RECURSIVO_AD(int I_AD,int NUEVOS_PERMISOS){
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);


    for(int i=0; i<8; i++){
        if(i==6){
            if(AD.avd_ap_detalle_directorio!=-1){
                PV_CAMBIAR_PERMISOS_RECURSIVO_DD(AD.avd_ap_detalle_directorio,NUEVOS_PERMISOS);
            }
        }else if(i==7){
            if(AD.avd_ap_arbol_virtual_directorio!=-1){
                PV_CAMBIAR_PERMISOS_RECURSIVO_AD(AD.avd_ap_arbol_virtual_directorio,NUEVOS_PERMISOS);
            }
        }else{
            if(AD.avd_ap_array_subdirectorios[i]!=-1){
                struct Arbol_Directorio TEMPORAL;
                TEMPORAL=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                if((PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==2) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==3) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==6) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==7)){
                    TEMPORAL.avd_permisos=NUEVOS_PERMISOS;
                    PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,TEMPORAL);
                    PV_CAMBIAR_PERMISOS_RECURSIVO_AD(AD.avd_ap_array_subdirectorios[i],NUEVOS_PERMISOS);
                }else{
                    imprimirln("=== ERROR: CARPETA SIN PERMISOS DE ESCRITURA ===");
                }
            }
        }
    }

}

void PV_CAMBIAR_PERMISOS_RECURSIVO_DD(int I_DD,int NUEVOS_PERMISOS){
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    for(int i=0; i<6; i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                PV_CAMBIAR_PERMISOS_RECURSIVO_DD(DD.dd_ap_detalle_directorio,NUEVOS_PERMISOS);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                struct Inodos INODO;
                INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                if((PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==2) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==3) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==6) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==7)){
                    DD.dd_array_files[i].permisos=NUEVOS_PERMISOS;
                    PV_GUARDAR_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,DD);
                }else{
                    imprimirln("=== ERROR: ARCHIVO SIN PERMISOS DE ESCRITURA ===");
                }
            }
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------- METODOS PARA CAMBIAR PROPIETARIO ARCHIVO/CARPETA -----------------------------------------------------------------
void PV_CAMBIAR_PROPIETARIO_AD(vector<string> RUTA,int I_AD,int POSICION_RUTA,int NUEVO_PROPIETARIO,bool MODO_RECURSIVO){
 //--
     struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    if(POSICION_RUTA<RUTA.size()){
        if(PV_EXISTE_ARCHIVO_CARPETA(RUTA,I_AD,POSICION_RUTA)){
            if(VALIDAR_EXTENSION_SINMENSAJE(RUTA[POSICION_RUTA])){//ARCHIVO
                if(AD.avd_ap_detalle_directorio!=-1){
                   // PV_CAMBIAR_PERMISOS_DD(RUTA,AD.avd_ap_detalle_directorio,POSICION_RUTA,NUEVO_PROPIETARIO);
                   PV_CAMBIAR_PROPIETARIO_DD(RUTA,AD.avd_ap_detalle_directorio,POSICION_RUTA,NUEVO_PROPIETARIO);
                }
            }else{
                bool ENCONTRADO=false;
                for(int i=0; (i<7) && (ENCONTRADO==false); i++){
                    if(i==6){
                        if(AD.avd_ap_arbol_virtual_directorio!=-1){
                            //PV_CAMBIAR_PROPIETARIO_AD(RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_RUTA,NUEVO_PROPIETARIO,MODO_RECURSIVO);
                            PV_CAMBIAR_PROPIETARIO_AD(RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_RUTA,NUEVO_PROPIETARIO,MODO_RECURSIVO);
                        }
                    }else{
                        if(AD.avd_ap_array_subdirectorios[i]!=-1){
                            struct Arbol_Directorio TEMPORAL;
                            TEMPORAL=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                            if(strcmp(TEMPORAL.avd_nombre_directorio,RUTA[POSICION_RUTA].c_str())==0){
                                ENCONTRADO=true;
                                if(POSICION_RUTA==(RUTA.size()-1)){
                                    //VALIDANDO PERMISOS DE ESCRITURA
                                    if((PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==2) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==3) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==6) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==7)){
                                        TEMPORAL.avd_proper=NUEVO_PROPIETARIO;
                                        PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,TEMPORAL);
                                        //VALIDANDO SI HAY QUE HACER TODO DE FORMA RECURSIVA
                                        if(MODO_RECURSIVO){
                                            //PV_CAMBIAR_PERMISOS_RECURSIVO_AD(AD.avd_ap_array_subdirectorios[i],NUEVO_PROPIETARIO);
                                            PV_CAMBIAR_PROPIETARIO_RECURSIVO_AD(AD.avd_ap_array_subdirectorios[i],NUEVO_PROPIETARIO);
                                        }
                                        imprimirln("++++ PROPIETARIO MODIFICADO EXITOSAMENTE ++++");
                                    }else{
                                        imprimirln("=== ERROR: NO TIENE PERMISOS DE ESCRITURA EN LA CARPETA ===");
                                    }
                                }else{
                                   // PV_CAMBIAR_PERMISOS_AD(RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_RUTA+1),NUEVO_PROPIETARIO,MODO_RECURSIVO);
                                   PV_CAMBIAR_PROPIETARIO_AD(RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_RUTA+1),NUEVO_PROPIETARIO,MODO_RECURSIVO);
                                }
                            }
                        }
                    }
                }
            }
        }else{
            imprimirln("=== ERROR: ARCHIVO/CARPETA INEXISTENTES ===");
        }
    }else{
        imprimirln("=== ERROR: SE EXCEDIO EN RUTA BUSQUEDA ===");
    }
 //--
}


void PV_CAMBIAR_PROPIETARIO_DD(vector<string> RUTA,int I_DD,int POSICION_RUTA,int NUEVO_PROPIETARIO){
//--
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    bool ENCONTRADO=false;

    for(int i=0; (i<6) && (ENCONTRADO==false); i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
               // PV_CAMBIAR_PERMISOS_DD(RUTA,DD.dd_ap_detalle_directorio,POSICION_RUTA,NUEVOS_PERMISOS);
               PV_CAMBIAR_PROPIETARIO_DD(RUTA,DD.dd_ap_detalle_directorio,POSICION_RUTA,NUEVO_PROPIETARIO);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                if(strcmp(DD.dd_array_files[i].dd_file_nombre,RUTA[POSICION_RUTA].c_str())==0){
                    ENCONTRADO=true;
                    struct Inodos INODO;
                    INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                    if((PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==2) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==3) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==6) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==7)){
                       /* DD.dd_array_files[i].permisos=NUEVOS_PERMISOS;
                        PV_GUARDAR_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,DD);*/
                        INODO.i_id_proper=NUEVO_PROPIETARIO;
                        PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                        imprimirln("++++ PROPIETARIO ARCHIVO MODIFICADO EXITOSAMENTE ++++");
                    }else{
                        imprimirln("=== ERROR: NO TIENE PERMISOS DE ESCRITURA SOBRE EL ARCHIVO ===");
                    }
                }
            }
        }
    }
//--
}

void PV_CAMBIAR_PROPIETARIO_RECURSIVO_AD(int I_AD,int NUEVO_PROPIETARIO){
//--
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);


    for(int i=0; i<8; i++){
        if(i==6){
            if(AD.avd_ap_detalle_directorio!=-1){
               // PV_CAMBIAR_PERMISOS_RECURSIVO_DD(AD.avd_ap_detalle_directorio,NUEVOS_PERMISOS);
               PV_CAMBIAR_PROPIETARIO_RECURSIVO_DD(AD.avd_ap_detalle_directorio,NUEVO_PROPIETARIO);
            }
        }else if(i==7){
            if(AD.avd_ap_arbol_virtual_directorio!=-1){
               // PV_CAMBIAR_PERMISOS_RECURSIVO_AD(AD.avd_ap_arbol_virtual_directorio,NUEVOS_PERMISOS);
               PV_CAMBIAR_PROPIETARIO_RECURSIVO_AD(AD.avd_ap_arbol_virtual_directorio,NUEVO_PROPIETARIO);
            }
        }else{
            if(AD.avd_ap_array_subdirectorios[i]!=-1){
                struct Arbol_Directorio TEMPORAL;
                TEMPORAL=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                if((PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==2) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==3) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==6) || (PERMISO_ESCRITURA_LECTURA(TEMPORAL.avd_permisos,TEMPORAL.avd_proper,PV_GID_USUARIO_PROPIETARIO(TEMPORAL.avd_proper))==7)){
                    TEMPORAL.avd_proper=NUEVO_PROPIETARIO;
                    PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,TEMPORAL);
                    //PV_CAMBIAR_PERMISOS_RECURSIVO_AD(AD.avd_ap_array_subdirectorios[i],NUEVOS_PERMISOS);
                    PV_CAMBIAR_PROPIETARIO_RECURSIVO_AD(AD.avd_ap_array_subdirectorios[i],NUEVO_PROPIETARIO);
                }else{
                    imprimirln("=== ERROR: CARPETA SIN PERMISOS DE ESCRITURA ===");
                }
            }
        }
    }
//--
}

void PV_CAMBIAR_PROPIETARIO_RECURSIVO_DD(int I_DD, int NUEVO_PROPIETARIO){
//--
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    for(int i=0; i<6; i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
               // PV_CAMBIAR_PERMISOS_RECURSIVO_DD(DD.dd_ap_detalle_directorio,NUEVOS_PERMISOS);
               PV_CAMBIAR_PROPIETARIO_RECURSIVO_DD(DD.dd_ap_detalle_directorio,NUEVO_PROPIETARIO);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                struct Inodos INODO;
                INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                if((PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==2) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==3) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==6) || (PERMISO_ESCRITURA_LECTURA(DD.dd_array_files[i].permisos,INODO.i_id_proper,PV_GID_USUARIO_PROPIETARIO(INODO.i_id_proper))==7)){
                   // DD.dd_array_files[i].permisos=NUEVOS_PERMISOS;
                    //PV_GUARDAR_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,DD);
                    INODO.i_id_proper=NUEVO_PROPIETARIO;
                    PV_GUARDAR_INODOS(DATOS_PIVOTE[0].inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,INODO);
                }else{
                    imprimirln("=== ERROR: ARCHIVO SIN PERMISOS DE ESCRITURA ===");
                }
            }
        }
    }
//--
}
//--------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------ METJODOS PARA MOVER ARCHIVO/CARPETA -------------------------------------------------------------
void PV_MOV_AD_M1(vector<string> RUTA_PATH,int I_AD,int POSICION_RUTA,vector<string> RUTA_DESTINY){

    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    if(POSICION_RUTA<RUTA_PATH.size()){
        if(PV_EXISTE_ARCHIVO_CARPETA(RUTA_PATH,I_AD,POSICION_RUTA)){
            if(VALIDAR_EXTENSION_SINMENSAJE(RUTA_PATH[POSICION_RUTA])){//ARCHIVO
                if(AD.avd_ap_detalle_directorio!=-1){
                //    imprimirln(" VA A ENTRAR A BUSCAR EL ARCHIVO A MOVER ");
                    PV_MOV_DD_M1(RUTA_PATH,AD.avd_ap_detalle_directorio,POSICION_RUTA,RUTA_DESTINY);
                }
            }else{//CARPETA
                bool ENCONTRADO=false;
                for(int i=0; (i<7) && (ENCONTRADO==false); i++){
                    if(i==6){
                        if(AD.avd_ap_arbol_virtual_directorio!=-1){
                            PV_MOV_AD_M1(RUTA_PATH,AD.avd_ap_arbol_virtual_directorio,POSICION_RUTA,RUTA_DESTINY);
                        }
                    }else{
                        if(AD.avd_ap_array_subdirectorios[i]!=-1){
                            struct Arbol_Directorio TEMPORAL;
                            TEMPORAL=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                            if(strcmp(TEMPORAL.avd_nombre_directorio,RUTA_PATH[POSICION_RUTA].c_str())==0){
                                ENCONTRADO=true;
                                if(POSICION_RUTA==(RUTA_PATH.size()-1)){
                              //      imprimirln(" ENCONTRO LA CARPETA QUE SE VA A MOVER ");
                                    bool ACTUALIZACION_PERMITIDA=PV_MOV_AD_M2(RUTA_DESTINY,0,0,AD.avd_ap_array_subdirectorios[i]);
                                    if(ACTUALIZACION_PERMITIDA){
                                        AD.avd_ap_array_subdirectorios[i]=-1;
                                        PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,AD);
                                        imprimirln("+++ CARPETA MOVIDA EXITOSAMENTE +++");
                                    }else{
                                        imprimirln("=== ERROR: NO SE PUDO MOVER LA CARPETA ===");
                                    }
                                }else{
                                    PV_MOV_AD_M1(RUTA_PATH,AD.avd_ap_array_subdirectorios[i],(POSICION_RUTA+1),RUTA_DESTINY);
                                }
                            }
                        }
                    }
                }
            }
        }else{
            imprimirln(" === ERROR: ARCHIVO/CARPETA INEXISTENTE PARA MOVER ===");
        }
    }

}

bool PV_MOV_AD_M2(vector<string> RUTA_DESTINY,int I_AD,int POSICION_RUTA,int I_AD_MOV){
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    bool ENCONTRADO=false;

    if(POSICION_RUTA<RUTA_DESTINY.size()){
        if(PV_EXISTE_ARCHIVO_CARPETA(RUTA_DESTINY,I_AD,POSICION_RUTA)){
            if(VALIDAR_EXTENSION_SINMENSAJE(RUTA_DESTINY[POSICION_RUTA])){
                imprimirln("=== ERROR: NO SE PUEDE MOVER UNA CARPETA A UN ARCHIVO DESTINO ===");
            }else{
                for(int i=0; (i<7) && (ENCONTRADO==false); i++){
                    if(i==6){
                        if(AD.avd_ap_arbol_virtual_directorio!=-1){
                            ENCONTRADO=PV_MOV_AD_M2(RUTA_DESTINY,AD.avd_ap_arbol_virtual_directorio,POSICION_RUTA,I_AD_MOV);
                        }
                    }else{
                        if(AD.avd_ap_array_subdirectorios[i]!=-1){
                            struct Arbol_Directorio TEMPORAL;
                            TEMPORAL=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                            if(strcmp(TEMPORAL.avd_nombre_directorio,RUTA_DESTINY[POSICION_RUTA].c_str())==0){
                                if(POSICION_RUTA==(RUTA_DESTINY.size()-1)){
                               //     imprimirln("ENCONTRO LA CARPETA DESTINO");
                                    ENCONTRADO=PV_MOV_AD_M3(AD.avd_ap_array_subdirectorios[i],I_AD_MOV);
                                }else{
                                    ENCONTRADO=PV_MOV_AD_M2(RUTA_DESTINY,AD.avd_ap_array_subdirectorios[i],(POSICION_RUTA+1),I_AD_MOV);
                                }
                            }
                        }
                    }
                }
            }
        }else{
            imprimirln("=== ERROR: ARCHIVO/CARPETA DESINO NO EXISTE PARA MOVER ===");
        }
    }

    return ENCONTRADO;
}

bool PV_MOV_AD_M3(int I_AD,int I_AD_MOV){
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    bool COLOCADO=false;

    for(int i=0; (i<7) && (COLOCADO==false); i++){
        if(i==6){
            if(AD.avd_ap_arbol_virtual_directorio==-1){
                AD.avd_ap_arbol_virtual_directorio=PV_CREAR_ARBOL_DIRECTORIO(AD.avd_nombre_directorio,AD.avd_proper,AD.avd_permisos);
                PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,AD);
                COLOCADO=PV_MOV_AD_M3(I_AD,I_AD_MOV);
            }else{
                COLOCADO=PV_MOV_AD_M3(AD.avd_ap_arbol_virtual_directorio,I_AD_MOV);
            }
        }else{
            if(AD.avd_ap_array_subdirectorios[i]==-1){
                COLOCADO=true;
                AD.avd_ap_array_subdirectorios[i]=I_AD_MOV;
                PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,AD);
            }
        }
    }

    return COLOCADO;
}

void PV_MOV_DD_M1(vector<string> RUTA_PATH, int I_DD,int POSICION_RUTA, vector<string> RUTA_DESTINY){
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    bool ENCONTRADO=false;
    for(int i=0; (i<6) && (ENCONTRADO==false); i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                PV_MOV_DD_M1(RUTA_PATH,DD.dd_ap_detalle_directorio,POSICION_RUTA,RUTA_DESTINY);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                if(strcmp(DD.dd_array_files[i].dd_file_nombre,RUTA_PATH[POSICION_RUTA].c_str())==0){
                    ENCONTRADO=true;
                  //  imprimirln(" ENCONTRO EL ARCHIVO A MOVER ");
                    struct Estructura_Directorio ED;
                    ED.dd_file_ap_inodo=DD.dd_array_files[i].dd_file_ap_inodo;
                    ED.permisos=DD.dd_array_files[i].permisos;
                    ED.dd_file_date_creacion=DD.dd_array_files[i].dd_file_date_creacion;
                    ED.dd_file_date_modificacion=DD.dd_array_files[i].dd_file_date_modificacion;
                    strcpy(ED.dd_file_nombre,DD.dd_array_files[i].dd_file_nombre);
                    bool ACTUALIZACION_PERMITIDA=PV_MOV_AD_M4(RUTA_DESTINY,0,0,ED);
                    if(ACTUALIZACION_PERMITIDA){
                        DD.dd_array_files[i].dd_file_ap_inodo=-1;
                        PV_GUARDAR_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,DD);
                        imprimirln("++++ ARCHIVO MOVIDO EXITOSAMENTE ++++");
                    }else{
                        imprimirln("=== ERROR: NO SE PUDO MOVER EL ARCHIVO ===");
                    }
                }
            }
        }
    }

}

bool PV_MOV_AD_M4(vector<string> RUTA_DESTINY,int I_AD,int POSICION_RUTA,struct Estructura_Directorio ED){
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    bool ENCONTRADO=false;

    if(POSICION_RUTA<RUTA_DESTINY.size()){
        if(PV_EXISTE_ARCHIVO_CARPETA(RUTA_DESTINY,I_AD,POSICION_RUTA)){
            if(VALIDAR_EXTENSION_SINMENSAJE(RUTA_DESTINY[POSICION_RUTA])){//ARCHIVO
                    imprimirln("=== ERROR: SE DEBE MOVER EL ARCHIVO HACIA UN DIRECTORIO, NO HACIA OTRO ARCHIVO ===");
            }else{//CARPETA
                for(int i=0; (i<7) && (ENCONTRADO==false); i++){
                    if(i==6){
                        if(AD.avd_ap_arbol_virtual_directorio!=-1){
                            ENCONTRADO=PV_MOV_AD_M4(RUTA_DESTINY,AD.avd_ap_arbol_virtual_directorio,POSICION_RUTA,ED);
                        }
                    }else{
                        if(AD.avd_ap_array_subdirectorios[i]!=-1){
                            struct Arbol_Directorio TEMPORAL;
                            TEMPORAL=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                            if(strcmp(TEMPORAL.avd_nombre_directorio,RUTA_DESTINY[POSICION_RUTA].c_str())==0){
                                if(POSICION_RUTA==(RUTA_DESTINY.size()-1)){
                               //     imprimirln("ENCONTRO LA CARPETA A DONDE SE MOVERA EL ARCHIVO");
                                    if(TEMPORAL.avd_ap_detalle_directorio!=-1){
                                        ENCONTRADO=PV_MOV_DD_M2(TEMPORAL.avd_ap_detalle_directorio,ED);
                                    }else{
                                        TEMPORAL.avd_ap_detalle_directorio=PV_CREAR_DETALLE_DIRECTORIO();
                                        PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,TEMPORAL);
                                        ENCONTRADO=PV_MOV_AD_M4(RUTA_DESTINY,I_AD,POSICION_RUTA,ED);
                                    }
                                }else{
                                    ENCONTRADO=PV_MOV_AD_M4(RUTA_DESTINY,AD.avd_ap_array_subdirectorios[i],(POSICION_RUTA+1),ED);
                                }
                            }
                        }
                    }
                }
            }
        }else{
            imprimirln("=== ERROR: ARCHIVO/CARPETA INEXISTENTE PARA MOVER ARCHIVO ===");
        }
    }

    return ENCONTRADO;
}

bool PV_MOV_DD_M2(int I_DD, struct Estructura_Directorio ED){
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    bool COLOCADO=false;

    for(int i=0; (i<6) && (COLOCADO==false); i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                COLOCADO=PV_MOV_DD_M2(DD.dd_ap_detalle_directorio,ED);
            }else{
                DD.dd_ap_detalle_directorio=PV_CREAR_DETALLE_DIRECTORIO();
                PV_GUARDAR_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,DD);
                COLOCADO=PV_MOV_DD_M2(I_DD,ED);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo==-1){
                COLOCADO=true;
                DD.dd_array_files[i].dd_file_ap_inodo=ED.dd_file_ap_inodo;
                DD.dd_array_files[i].dd_file_date_creacion=ED.dd_file_date_creacion;
                DD.dd_array_files[i].dd_file_date_modificacion=ED.dd_file_date_modificacion;
                strcpy(DD.dd_array_files[i].dd_file_nombre,ED.dd_file_nombre);
                DD.dd_array_files[i].permisos=ED.permisos;
                PV_GUARDAR_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,DD);
            }
        }
    }

    return COLOCADO;
}
//--------------------------------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------METODOS PARA ELIMINAR UN ARCHIVO/CARPETA ---------------------------------------------
void PV_RM_AD(vector<string> RUTA,int I_AD,int POSICION_RUTA){
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    if(POSICION_RUTA<RUTA.size()){
        if(PV_EXISTE_ARCHIVO_CARPETA(RUTA,I_AD,POSICION_RUTA)){
            if(VALIDAR_EXTENSION_SINMENSAJE(RUTA[POSICION_RUTA])){//ARCHIVO
                if(AD.avd_ap_detalle_directorio!=-1){
                    PV_RM_DD(RUTA,AD.avd_ap_detalle_directorio,POSICION_RUTA);
                }
            }else{//CARPEETA
                bool ENCONTRADO=false;
                for(int i=0; (i<7) && (ENCONTRADO==false); i++){
                    if(i==6){
                        if(AD.avd_ap_arbol_virtual_directorio!=-1){
                            PV_RM_AD(RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_RUTA);
                        }
                    }else{
                        if(AD.avd_ap_array_subdirectorios[i]!=-1){
                            struct Arbol_Directorio TEMPORAL;
                            TEMPORAL=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                            if(strcmp(TEMPORAL.avd_nombre_directorio,RUTA[POSICION_RUTA].c_str())==0){
                                ENCONTRADO=true;
                                if(POSICION_RUTA==(RUTA.size()-1)){
                                    PV_ELIMINAR_BIT_AD(AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].path_disco,DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                                    PV_RM_AD_M2(AD.avd_ap_array_subdirectorios[i]);
                                    AD.avd_ap_array_subdirectorios[i]=-1;
                                    PV_GUARDAR_ARBOL_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,AD);
                                    imprimirln("++++ CARPETA ELIMINADA EXITOSAMENTE ++++");
                                }else{
                                    PV_RM_AD(RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_RUTA+1));
                                }
                            }
                        }
                    }
                }
            }
        }else{
            imprimirln("=== ERROR: NO EXISTE EL ARCHIVO/CARPETA A ELIMINAR ===");
        }
    }

}

void PV_RM_AD_M2(int I_AD){
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(DATOS_PIVOTE[0].inicio_particion,I_AD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    for(int i=0; i<8; i++){
        if(i==7){
            if(AD.avd_ap_arbol_virtual_directorio!=-1){
                PV_ELIMINAR_BIT_AD(AD.avd_ap_arbol_virtual_directorio,DATOS_PIVOTE[0].path_disco,DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                PV_RM_AD_M2(AD.avd_ap_arbol_virtual_directorio);
            }
        }else if(i==6){
            if(AD.avd_ap_detalle_directorio!=-1){
                PV_ELIMINAR_BIT_DD(AD.avd_ap_detalle_directorio,DATOS_PIVOTE[0].path_disco,DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                PV_RM_DD_M2(AD.avd_ap_detalle_directorio);
            }
        }else{
            if(AD.avd_ap_array_subdirectorios[i]!=-1){
                PV_ELIMINAR_BIT_AD(AD.avd_ap_array_subdirectorios[i],DATOS_PIVOTE[0].path_disco,DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                PV_RM_AD_M2(AD.avd_ap_array_subdirectorios[i]);
            }
        }
    }
}

void PV_RM_DD_M2(int I_DD){
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    for(int i=0; i<6; i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                PV_ELIMINAR_BIT_DD(DD.dd_ap_detalle_directorio,DATOS_PIVOTE[0].path_disco,DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                PV_RM_DD_M2(DD.dd_ap_detalle_directorio);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                PV_ELIMINAR_BIT_I(DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].path_disco,DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                PV_RM_INODOS(DD.dd_array_files[i].dd_file_ap_inodo);
            }
        }
    }

}

void PV_RM_INODOS(int I_I){
    struct Inodos INODO;
    INODO=PV_RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,I_I,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    for(int i=0; i<5; i++){
        if(i==4){
            if(INODO.i_ap_indirecto!=-1){
                PV_ELIMINAR_BIT_I(I_I,DATOS_PIVOTE[0].path_disco,DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                PV_RM_INODOS(INODO.i_ap_indirecto);
            }
        }else{
            if(INODO.i_array_bloques[i]!=-1){
                PV_ELIMINAR_BIT_B(INODO.i_array_bloques[i],DATOS_PIVOTE[0].path_disco,DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
            }
        }
    }
}

void PV_RM_DD(vector<string> RUTA,int I_DD,int POSICION_RUTA){
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    bool ENCONTRADO=false;
    for(int i=0;(i<6) && (ENCONTRADO==false); i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                PV_RM_DD(RUTA,DD.dd_ap_detalle_directorio,POSICION_RUTA);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                if(strcmp(DD.dd_array_files[i].dd_file_nombre,RUTA[POSICION_RUTA].c_str())==0){
                    ENCONTRADO=true;
                    PV_RM_INODOS(DD.dd_array_files[i].dd_file_ap_inodo);
                    PV_ELIMINAR_BIT_I(DD.dd_array_files[i].dd_file_ap_inodo,DATOS_PIVOTE[0].path_disco,DATOS_PIVOTE[0].inicio_particion,DATOS_PIVOTE[0].valor_N);
                    DD.dd_array_files[i].dd_file_ap_inodo=-1;
                    PV_GUARDAR_DETALLE_DIRECTORIO(DATOS_PIVOTE[0].inicio_particion,I_DD,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco,DD);
                    imprimirln("++++ ARCHIVO ELIMINADO EXITOSAMENTE ++++");
                }
            }
        }
    }

}


void PV_ELIMINAR_BIT_AD(int ID_AD,string path_disco,int inicio_particion,int valor_N){
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
        int POSICION_MODIFICAR_BITMAP=PV_INICIO_BITMAP_AD(inicio_particion,valor_N)+ID_AD;
        char NUEVO_VALOR=48;
        fseek(ARCHIVO_GLOBAL,POSICION_MODIFICAR_BITMAP,SEEK_SET);
        fwrite(&NUEVO_VALOR,1,1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 50....");
    }
}

void PV_ELIMINAR_BIT_DD(int I_DD,string path_disco,int inicio_particion,int valor_N){
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
        int POSICION_MODIFICAR_BITMAP=PV_INICIO_BITMAP_DD(inicio_particion,valor_N)+I_DD;
        char NUEVO_VALOR=48;
        fseek(ARCHIVO_GLOBAL,POSICION_MODIFICAR_BITMAP,SEEK_SET);
        fwrite(&NUEVO_VALOR,1,1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 51....");
    }
}

void PV_ELIMINAR_BIT_I(int I_I,string path_disco,int inicio_particion,int valor_N){
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
        int POSICION_MODIFICAR_BITMAP=PV_INICIO_BITMAP_INODOS(inicio_particion,valor_N)+I_I;
        char NUEVO_VALOR=48;
        fseek(ARCHIVO_GLOBAL,POSICION_MODIFICAR_BITMAP,SEEK_SET);
        fwrite(&NUEVO_VALOR,1,1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 52....");
    }
}

void PV_ELIMINAR_BIT_B(int I_B,string path_disco,int inicio_particion,int valor_N){
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(path_disco.c_str(),"rb+");
        int POSICION_MODIFICAR_BITMAP=PV_INICIO_BITMAP_BLOQUE(inicio_particion,valor_N)+I_B;
        char NUEVO_VALOR=48;
        fseek(ARCHIVO_GLOBAL,POSICION_MODIFICAR_BITMAP,SEEK_SET);
        fwrite(&NUEVO_VALOR,1,1,ARCHIVO_GLOBAL);
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }else{
        imprimirln(".... ARCHIVO GLOBAL ABIERTO 52....");
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------------------


int PV_GID_USUARIO_PROPIETARIO(int u_id){
    int ID_GRUPO=0;
    vector<string> DIRECCION=OBTENER_DIRECCION("/users.txt","/");
    string TEXTO_USERS=PV_OBTENER_TEXTO_ARCHIVO(DIRECCION,0,0);
    string GRUPO_USUARIO="";
    vector<string> SEPARADOR_UNO=OBTENER_DIRECCION(TEXTO_USERS,"\n");
    bool GRUPO_ENCONTRADO=false;
    for(int i=0; (i<SEPARADOR_UNO.size()) && (GRUPO_ENCONTRADO==false); i++){
        vector<string> SEPARADOR_DOS=OBTENER_DIRECCION(SEPARADOR_UNO[i],",");
        if((SEPARADOR_DOS.size()==5) && (strcmp(SEPARADOR_DOS[0].c_str(),"0")!=0)){//SI ENTRA AQUI ENCONTRO UN USUARIO
            if(strcmp(SEPARADOR_DOS[0].c_str(),to_string(u_id).c_str())==0){
                GRUPO_USUARIO=SEPARADOR_DOS[2];
                GRUPO_ENCONTRADO=true;
            }
        }
    }
    bool ID_GRUPO_ENCONTRADO=false;
    for(int i=0; (i<SEPARADOR_UNO.size()) && (ID_GRUPO_ENCONTRADO==false); i++){
        vector<string> SEPARADOR_DOS=OBTENER_DIRECCION(SEPARADOR_UNO[i],",");
        if((SEPARADOR_DOS.size()==3) && (strcmp(SEPARADOR_DOS[0].c_str(),"0")!=0)){
            if(strcmp(GRUPO_USUARIO.c_str(),SEPARADOR_DOS[2].c_str())==0){
                ID_GRUPO=atoi(SEPARADOR_DOS[0].c_str());
                ID_GRUPO_ENCONTRADO=true;
            }
        }
    }
    return ID_GRUPO;
}

string PV_NGRUPO_PROPIETARIO(int u_id){
    string NOMBRE_GRUPO="";
    vector<string> DIRECCION=OBTENER_DIRECCION("/users.txt","/");
    string TEXTO_USUARIOS=PV_OBTENER_TEXTO_ARCHIVO(DIRECCION,0,0);
    vector<string> SEPARADOR_UNO=OBTENER_DIRECCION(TEXTO_USUARIOS,"\n");
    string ID_US=to_string(u_id);

    bool ENCONTRADO=false;
    for(int i=0; (i<SEPARADOR_UNO.size()) && (ENCONTRADO==false); i++){
        vector<string> SEPARADOR_DOS=OBTENER_DIRECCION(SEPARADOR_UNO[i],",");
        if((SEPARADOR_DOS.size()==5) && (strcmp(SEPARADOR_DOS[0].c_str(),"0")!=0)){
            if(strcmp(ID_US.c_str(),SEPARADOR_DOS[0].c_str())==0){
                ENCONTRADO=true;
                NOMBRE_GRUPO=SEPARADOR_DOS[2];
            }
        }
    }

    return NOMBRE_GRUPO;
}

string PV_NUSUARIO_PROPIETARIO(int u_id){
    string NOMBRE_GRUPO="";
    vector<string> DIRECCION=OBTENER_DIRECCION("/users.txt","/");
    string TEXTO_USUARIOS=PV_OBTENER_TEXTO_ARCHIVO(DIRECCION,0,0);
    vector<string> SEPARADOR_UNO=OBTENER_DIRECCION(TEXTO_USUARIOS,"\n");
    string ID_US=to_string(u_id);

    bool ENCONTRADO=false;
    for(int i=0; (i<SEPARADOR_UNO.size()) && (ENCONTRADO==false); i++){
        vector<string> SEPARADOR_DOS=OBTENER_DIRECCION(SEPARADOR_UNO[i],",");
        if((SEPARADOR_DOS.size()==5) && (strcmp(SEPARADOR_DOS[0].c_str(),"0")!=0)){
            if(strcmp(ID_US.c_str(),SEPARADOR_DOS[0].c_str())==0){
                ENCONTRADO=true;
                NOMBRE_GRUPO=SEPARADOR_DOS[3];
            }
        }
    }

    return NOMBRE_GRUPO;
}


bool PV_VALIDAR_UGO_CHMOD(string numero){
    bool NUMERO_PERMITIDO=true;
    imprimirNumln(numero.size());
    if(numero.size()!=3){
        NUMERO_PERMITIDO=false;
    }

    for(int i=0; (i<numero.size()) && (NUMERO_PERMITIDO==true); i++){
        if((numero[i]!=48) && (numero[i]!=49) && (numero[i]!=50) && (numero[i]!=51) && (numero[i]!=52) && (numero[i]!=53) && (numero[i]!=54) && (numero[i]!=55)){
            NUMERO_PERMITIDO=false;
        }
    }

    return NUMERO_PERMITIDO;
}

int PV_INICIO_BITMAP_BLOQUE(int inicio_particion, int valor_N){
    int inicio_bmp_bloque=inicio_particion+sizeof(SUPER_BOOT)+valor_N+(valor_N*sizeof(Arbol_Directorio))+valor_N+(valor_N*sizeof(Detalle_Directorio))+(5*valor_N)+(5*valor_N*sizeof(Inodos));
    return inicio_bmp_bloque;
}

int PV_INICIO_BITMAP_INODOS(int inicio_particion,int valor_N){
    int inicio_bm_inodos=inicio_particion+sizeof(SUPER_BOOT)+valor_N+(valor_N*sizeof(Arbol_Directorio))+valor_N+(valor_N*sizeof(Detalle_Directorio));
    return inicio_bm_inodos;
}

int PV_INICIO_BITMAP_DD(int inicio_particion, int valor_N){
    int inicio_bitmap_dd=inicio_particion+sizeof(SUPER_BOOT)+valor_N+(valor_N*sizeof(Arbol_Directorio));
    return inicio_bitmap_dd;
}

int PV_INICIO_BITMAP_AD(int inicio_particion,int valor_N){
    int inicio_bitmap_ad=inicio_particion+sizeof(SUPER_BOOT);
    return inicio_bitmap_ad;
}

//----------------------------------------------------------------------- GRAFICAR Y REPORTAR --------------------------------------------------
void GRAFICAR_REPORTAR_DIRECTORIOS(struct PARAMETRO_REP_TREE param, int I_AD,int apuntador,int I_ADPadre){
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(param.inicio_particion,I_AD,param.valor_n,param.path_disco);

    ARCHIVO_REPORTE<<"DA"<<I_AD<<"[label=\"{<t0>"<<AD.avd_nombre_directorio<<"|{";
    for(int i=0; i<8; i++){
        if(i==6){
            ARCHIVO_REPORTE<<"<p"<<i<<">"<<AD.avd_ap_detalle_directorio<<"|";
        }else if(i==7){
            ARCHIVO_REPORTE<<"<p"<<i<<">"<<AD.avd_ap_arbol_virtual_directorio;
        }else{
            ARCHIVO_REPORTE<<"<p"<<i<<">"<<AD.avd_ap_array_subdirectorios[i]<<"|";
        }
    }
    ARCHIVO_REPORTE<<"}}\"];"<<endl;

    if((I_ADPadre!=-1) && (apuntador!=-1)){
        ARCHIVO_REPORTE<<"DA"<<I_ADPadre<<":p"<<apuntador<<"->DA"<<I_AD<<":t0"<<endl;
    }

    for(int i=0; i<7; i++){
        if(i==6){
            if(AD.avd_ap_arbol_virtual_directorio!=-1){
                GRAFICAR_REPORTAR_DIRECTORIOS(param,AD.avd_ap_arbol_virtual_directorio,(i+1),I_AD);
            }
        }else{
            if(AD.avd_ap_array_subdirectorios[i]!=-1){
                GRAFICAR_REPORTAR_DIRECTORIOS(param,AD.avd_ap_array_subdirectorios[i],i,I_AD);
            }
        }
    }

}

void REPORTAR_TREE_COMPLETO_AD(struct PARAMETRO_REP_TREE param,int I_AD,int apuntador,int I_ADPadre){
    //---
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(param.inicio_particion,I_AD,param.valor_n,param.path_disco);

    ARCHIVO_REPORTE<<"DA"<<I_AD<<"[label=\"{<t0>"<<AD.avd_nombre_directorio<<"|{";
    for(int i=0; i<8; i++){
        if(i==6){
            ARCHIVO_REPORTE<<"<p"<<i<<">"<<AD.avd_ap_detalle_directorio<<"|";
        }else if(i==7){
            ARCHIVO_REPORTE<<"<p"<<i<<">"<<AD.avd_ap_arbol_virtual_directorio;
        }else{
            ARCHIVO_REPORTE<<"<p"<<i<<">"<<AD.avd_ap_array_subdirectorios[i]<<"|";
        }
    }
    ARCHIVO_REPORTE<<"}}\"];"<<endl;

    if((I_ADPadre!=-1) && (apuntador!=-1)){
        ARCHIVO_REPORTE<<"DA"<<I_ADPadre<<":p"<<apuntador<<"->DA"<<I_AD<<":t0"<<endl;
    }

    for(int i=0; i<8; i++){
        if(i==7){
            if(AD.avd_ap_arbol_virtual_directorio!=-1){
                REPORTAR_TREE_COMPLETO_AD(param,AD.avd_ap_arbol_virtual_directorio,i,I_AD);
            }
        }else if(i==6){
            if(AD.avd_ap_detalle_directorio!=-1){//EL DETALLE DIRECTORIO LO APUNTA O UN ARBOL_DIRECTORIO O UN MISMO DETALLE_DIRECTORIO
                REPORTAR_TREE_COMPLETO_DD(param,AD.avd_ap_detalle_directorio,i,I_AD,"DA");
            }
        }else{
            if(AD.avd_ap_array_subdirectorios[i]!=-1){
                REPORTAR_TREE_COMPLETO_AD(param,AD.avd_ap_array_subdirectorios[i],i,I_AD);
            }
        }
    }
    //---
}

void REPORTAR_TREE_COMPLETO_DD(struct PARAMETRO_REP_TREE param,int I_DD,int apuntador,int I_AD_DDPadre,string etiquet_AD_DD){
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(param.inicio_particion,I_DD,param.valor_n,param.path_disco);

    ARCHIVO_REPORTE<<"DD"<<I_DD<<"[label=\"{<t0>"<<"DD"<<I_DD<<"|";

    for(int i=0; i<6; i++){
        if(i==5){
            ARCHIVO_REPORTE<<"<p"<<i<<">"<<DD.dd_ap_detalle_directorio;
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                ARCHIVO_REPORTE<<"{"<<DD.dd_array_files[i].dd_file_nombre<<"|<p"<<i<<">"<<DD.dd_array_files[i].dd_file_ap_inodo<<"}|";
            }else{
                ARCHIVO_REPORTE<<"{"<<"----"<<"|<p"<<i<<">"<<DD.dd_array_files[i].dd_file_ap_inodo<<"}|";
            }
        }
    }
    ARCHIVO_REPORTE<<"}\"];"<<endl;

    ARCHIVO_REPORTE<<etiquet_AD_DD<<I_AD_DDPadre<<":p"<<apuntador<<"->DD"<<I_DD<<":t0"<<endl;

    for(int i=0; i<6; i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                REPORTAR_TREE_COMPLETO_DD(param,DD.dd_ap_detalle_directorio,i,I_DD,"DD");
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                REPORTAR_TREE_COMPLETO_I(param,DD.dd_array_files[i].dd_file_ap_inodo,i,I_DD,"DD");
            }
        }
    }

}

void REPORTAR_TREE_COMPLETO_I(struct PARAMETRO_REP_TREE param,int I_I,int apuntador,int I_DD_IPadre,string etiqueta_DD_I){
    struct Inodos INODO;
    INODO=PV_RECUPERAR_INODO(param.inicio_particion,I_I,param.valor_n,param.path_disco);

    ARCHIVO_REPORTE<<"I"<<I_I<<"[label=\"{<t0>"<<I_I<<"|";

    for(int i=0; i<5; i++){
        if(i==4){
            ARCHIVO_REPORTE<<"{"<<"<p"<<i<<">"<<INODO.i_array_bloques[i]<<"}";
        }else{
            ARCHIVO_REPORTE<<"{"<<i<<"|<p"<<i<<">"<<INODO.i_array_bloques[i]<<"}|";
        }
    }

    ARCHIVO_REPORTE<<"}\"];"<<endl;

    ARCHIVO_REPORTE<<etiqueta_DD_I<<I_DD_IPadre<<":p"<<apuntador<<"->I"<<I_I<<":t0"<<endl;

    for(int i=0; i<5; i++){
        if(i==4){
            if(INODO.i_ap_indirecto!=-1){
                REPORTAR_TREE_COMPLETO_I(param,INODO.i_ap_indirecto,i,I_I,"I");
            }
        }else{
            if(INODO.i_array_bloques[i]!=-1){
                REPORTAR_TREE_COMPLETO_B(param,INODO.i_array_bloques[i],i,I_I,"I");
            }
        }
    }

}

void REPORTAR_TREE_COMPLETO_B(struct PARAMETRO_REP_TREE param,int I_B,int apuntador,int I_I_Padre, string etiqueta_I){
    struct Bloque_Datos BLOQUE;
    BLOQUE=PV_RECUPERAR_BLOQUES(param.inicio_particion,I_B,param.valor_n,param.path_disco);

    ARCHIVO_REPORTE<<"B"<<I_B<<"[label=\"{<t0>"<<I_B<<"|"<<BLOQUE.db_data<<"}\"];";

    ARCHIVO_REPORTE<<etiqueta_I<<I_I_Padre<<":p"<<apuntador<<"->B"<<I_B<<":t0"<<endl;

}

bool REPORTAR_LS(vector<string> RUTA,int I_AD, int POSICION_RUTA,struct PARAMETRO_REP_TREE param){
    struct Arbol_Directorio AD;
    bool ENCONTRADO_CA=false;
    AD=PV_RECUPERAR_AD(param.inicio_particion,I_AD,param.valor_n,param.path_disco);

    if(POSICION_RUTA<RUTA.size()){
        if(PV_EXISTE_ARCHIVO_CARPETA(RUTA,I_AD,POSICION_RUTA)){
            //---
            if(VALIDAR_EXTENSION_SINMENSAJE(RUTA[POSICION_RUTA])){//SI ENTRA AQUI ES ARCHIVO
                if(AD.avd_ap_detalle_directorio!=-1){
                    ENCONTRADO_CA=REPORTAR_LS_DD(RUTA,AD.avd_ap_detalle_directorio,POSICION_RUTA,param);
                }
            }else{//CASO CONTRARIO ES UNA CARPETA
                for(int i=0; (i<7) && (ENCONTRADO_CA==false); i++){
                    if(i==6){
                        if(AD.avd_ap_arbol_virtual_directorio!=-1){
                            ENCONTRADO_CA=REPORTAR_LS(RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_RUTA,param);
                        }
                    }else{
                        if(AD.avd_ap_array_subdirectorios[i]!=-1){
                            struct Arbol_Directorio BUSCANDO_CARPETA;
                            BUSCANDO_CARPETA=PV_RECUPERAR_AD(param.inicio_particion,AD.avd_ap_array_subdirectorios[i],param.valor_n,param.path_disco);
                            if(strcmp(BUSCANDO_CARPETA.avd_nombre_directorio,RUTA[POSICION_RUTA].c_str())==0){
                                    ENCONTRADO_CA=true;
                              //  cout<<PERMISOS_RECUPERADOS(BUSCANDO_CARPETA.avd_permisos)<<"   "<<PV_NGRUPO_PROPIETARIO(BUSCANDO_CARPETA.avd_proper)<<"   "<<PV_NUSUARIO_PROPIETARIO(BUSCANDO_CARPETA.avd_proper)<<"   "<<ctime(&BUSCANDO_CARPETA.avd_fecha_creacion)<<"   "<<BUSCANDO_CARPETA.avd_nombre_directorio<<endl;
                               /* ARCHIVO_REPORTE<<"<tr><td>"<<PERMISOS_RECUPERADOS(BUSCANDO_CARPETA.avd_permisos)<<"</td><td>"<<PV_NUSUARIO_PROPIETARIO(BUSCANDO_CARPETA.avd_proper)<<"</td><td>"<<PV_NGRUPO_PROPIETARIO(BUSCANDO_CARPETA.avd_proper)<<"</td><td>";
                                ARCHIVO_REPORTE<<ctime(&BUSCANDO_CARPETA.avd_fecha_creacion)<<"</td><td>"<<BUSCANDO_CARPETA.avd_nombre_directorio<<"</td></tr>"<<endl;*/
                                cout<<PERMISOS_RECUPERADOS(BUSCANDO_CARPETA.avd_permisos)<<" ******* "<<PV_NUSUARIO_PROPIETARIO(BUSCANDO_CARPETA.avd_proper)<<" ******* "<<PV_NGRUPO_PROPIETARIO(BUSCANDO_CARPETA.avd_proper)<<" ******* "<<BUSCANDO_CARPETA.avd_nombre_directorio<<" ******* "<<ctime(&BUSCANDO_CARPETA.avd_fecha_creacion)<<endl;
                                if(POSICION_RUTA==(RUTA.size()-1)){
                                    ENCONTRADO_CA=true;
                                }else{
                                    ENCONTRADO_CA=REPORTAR_LS(RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_RUTA+1),param);
                                }
                            }
                        }
                    }
                }
            }
            //---
        }
    }
    return ENCONTRADO_CA;
}

bool REPORTAR_LS_DD(vector<string> RUTA,int I_DD, int POSICION_RUTA,struct PARAMETRO_REP_TREE param){
    struct Detalle_Directorio DD;
    bool encontrador=false;
    DD=PV_RECUPERAR_DD(param.inicio_particion,I_DD,param.valor_n,param.path_disco);

    for(int i=0; (i<6) && (encontrador==false); i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                encontrador=REPORTAR_LS_DD(RUTA,DD.dd_ap_detalle_directorio,POSICION_RUTA,param);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                if(strcmp(DD.dd_array_files[i].dd_file_nombre,RUTA[POSICION_RUTA].c_str())==0){
                    struct Inodos INODO;
                    INODO=PV_RECUPERAR_INODO(param.inicio_particion,DD.dd_array_files[i].dd_file_ap_inodo,param.valor_n,param.path_disco);
                    encontrador=true;
                   /* ARCHIVO_REPORTE<<"<tr><td>"<<PERMISOS_RECUPERADOS(DD.dd_array_files[i].permisos)<<"</td><td>"<<PV_NUSUARIO_PROPIETARIO(INODO.i_id_proper)<<"</td><td>"<<PV_NGRUPO_PROPIETARIO(INODO.i_id_proper)<<"</td><td>";
                    ARCHIVO_REPORTE<<ctime(&DD.dd_array_files[i].dd_file_date_creacion)<<"</td><td>"<<DD.dd_array_files[i].dd_file_nombre<<"</td></tr>"<<endl;*/
                    cout<<PERMISOS_RECUPERADOS(DD.dd_array_files[i].permisos)<<" ******* "<<PV_NUSUARIO_PROPIETARIO(INODO.i_id_proper)<<" ******* "<<PV_NGRUPO_PROPIETARIO(INODO.i_id_proper)<<" ******* "<<DD.dd_array_files[i].dd_file_nombre<<" ******* "<<ctime(&DD.dd_array_files[i].dd_file_date_creacion)<<endl;
                }
            }
        }
    }

    return encontrador;
}

//-------------------------------------------------------OBTIENE LAS RUTAS PARA EL MENU TREE_DIRECTORIO -----------------------------------------

bool REPORTAR_LISTADO_TREE_DIRECTORIO(struct PARAMETRO_REP_TREE param,vector<string> RUTA,int I_AD, int POSICION_RUTA,bool PERMITIR_LISTADO,string concatenado){
    struct Arbol_Directorio AD;
    bool ENCONTRADO=false;
   // string CONCATENACION=concatenado+"/";

    AD=PV_RECUPERAR_AD(param.inicio_particion,I_AD,param.valor_n,param.path_disco);

   // if(PV_EXISTE_ARCHIVO_CARPETA(RUTA,I_AD,POSICION_RUTA)){
        for(int i=0; (i<7) && (ENCONTRADO==false); i++){
            if(i==6){
                if(AD.avd_ap_arbol_virtual_directorio!=-1){
                    ENCONTRADO=REPORTAR_LISTADO_TREE_DIRECTORIO(param,RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_RUTA,PERMITIR_LISTADO,concatenado);
                }
            }else{
                if(AD.avd_ap_array_subdirectorios[i]!=-1){
                    struct Arbol_Directorio BUSCAR;
                    BUSCAR=PV_RECUPERAR_AD(param.inicio_particion,AD.avd_ap_array_subdirectorios[i],param.valor_n,param.path_disco);
                  /*  if((strcmp(BUSCAR.avd_nombre_directorio,RUTA[RUTA.size()-1].c_str())==0) || (PERMITIR_LISTADO==true)){
                        string nuevo=concatenado+BUSCAR.avd_nombre_directorio+"/";
                        imprimirln(nuevo);
                        ENCONTRADO=REPORTAR_LISTADO_TREE_DIRECTORIO(param,RUTA,AD.avd_ap_array_subdirectorios[i],POSICION_RUTA,true,nuevo);
                    }else{
                        string nuevo=concatenado+BUSCAR.avd_nombre_directorio+"/";
                        ENCONTRADO=REPORTAR_LISTADO_TREE_DIRECTORIO(param,RUTA,AD.avd_ap_array_subdirectorios[i],POSICION_RUTA,false,nuevo);
                    }*/
                    //--
                    if(strcmp(BUSCAR.avd_nombre_directorio,RUTA[RUTA.size()-1].c_str())==0){
                        ENCONTRADO=true;
                        string nuevo=concatenado+BUSCAR.avd_nombre_directorio+"/";
                       // imprimirln(nuevo);
                        DIRECTORIOS_TREE_REPORTES.push_back(nuevo);
                        ENCONTRADO=REPORTAR_LISTADO_TREE_DIRECTORIO(param,RUTA,AD.avd_ap_array_subdirectorios[i],POSICION_RUTA,true,nuevo);
                    }else if(PERMITIR_LISTADO==true){
                        string nuevo=concatenado+BUSCAR.avd_nombre_directorio+"/";
                        //imprimirln(nuevo);
                        DIRECTORIOS_TREE_REPORTES.push_back(nuevo);
                        ENCONTRADO=REPORTAR_LISTADO_TREE_DIRECTORIO(param,RUTA,AD.avd_ap_array_subdirectorios[i],POSICION_RUTA,true,nuevo);
                    }else if(strcmp(BUSCAR.avd_nombre_directorio,RUTA[POSICION_RUTA].c_str())==0){
                        ENCONTRADO=true;
                        if(POSICION_RUTA<(RUTA.size())){
                            string nuevo=concatenado+BUSCAR.avd_nombre_directorio+"/";
                            ENCONTRADO=REPORTAR_LISTADO_TREE_DIRECTORIO(param,RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_RUTA+1),false,nuevo);
                        }else{
                            string nuevo=concatenado+BUSCAR.avd_nombre_directorio+"/";
                            ENCONTRADO=REPORTAR_LISTADO_TREE_DIRECTORIO(param,RUTA,AD.avd_ap_array_subdirectorios[i],POSICION_RUTA,false,nuevo);
                        }

                    }
                    //--
                }
            }
        }
  //  }else{
  //      imprimirln("=== ERROR: LA CARPETA NO EXISTE ===");
  //  }
    return ENCONTRADO;

}

//-------------------------------------------------------ESTOS REPORTAN UN TREEE_DIRECTORIO ----------------------------------------------------------
void REPORTAR_TREE_DIRECTORIO_AD(vector<string>RUTA,int POSICION_RUTA,struct PARAMETRO_REP_TREE param,int I_AD,int apuntador,int I_ADPadre){
//--
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(param.inicio_particion,I_AD,param.valor_n,param.path_disco);

    ARCHIVO_REPORTE<<"DA"<<I_AD<<"[label=\"{<t0>"<<AD.avd_nombre_directorio<<"|{";
    for(int i=0; i<8; i++){
        if(i==6){
            ARCHIVO_REPORTE<<"<p"<<i<<">"<<AD.avd_ap_detalle_directorio<<"|";
        }else if(i==7){
            ARCHIVO_REPORTE<<"<p"<<i<<">"<<AD.avd_ap_arbol_virtual_directorio;
        }else{
            ARCHIVO_REPORTE<<"<p"<<i<<">"<<AD.avd_ap_array_subdirectorios[i]<<"|";
        }
    }
    ARCHIVO_REPORTE<<"}}\"];"<<endl;

    if((I_ADPadre!=-1) && (apuntador!=-1)){
        ARCHIVO_REPORTE<<"DA"<<I_ADPadre<<":p"<<apuntador<<"->DA"<<I_AD<<":t0"<<endl;
    }

    if(strcmp(RUTA[RUTA.size()-1].c_str(),AD.avd_nombre_directorio)==0){
        if(AD.avd_ap_detalle_directorio!=-1){
            REPORTAR_TREE_DIRECTORIO_DD(param,AD.avd_ap_detalle_directorio,6,I_AD,"DA");
        }
    }else{
    //**
        bool CARPETA_ENCONTRADA=false;
        for(int i=0; (i<7) && (CARPETA_ENCONTRADA==false); i++){
            if(i==6){
                if(AD.avd_ap_arbol_virtual_directorio!=-1){
                    REPORTAR_TREE_DIRECTORIO_AD(RUTA,POSICION_RUTA,param,AD.avd_ap_arbol_virtual_directorio,(i+1),I_AD);
                }
            }else{
                if(AD.avd_ap_array_subdirectorios[i]!=-1){
                    struct Arbol_Directorio ADTEMPORAL;
                    ADTEMPORAL=PV_RECUPERAR_AD(param.inicio_particion,AD.avd_ap_array_subdirectorios[i],param.valor_n,param.path_disco);
                    if(strcmp(ADTEMPORAL.avd_nombre_directorio,RUTA[POSICION_RUTA].c_str())==0){
                        CARPETA_ENCONTRADA=true;
                        if(POSICION_RUTA<RUTA.size()){
                            REPORTAR_TREE_DIRECTORIO_AD(RUTA,(POSICION_RUTA+1),param,AD.avd_ap_array_subdirectorios[i],i,I_AD);
                        }
                    }
                }
            }
        }
    //**
    }

//--
}

void REPORTAR_TREE_DIRECTORIO_DD(struct PARAMETRO_REP_TREE param,int I_DD,int apuntador,int I_AD_DDPadre,string etiquet_AD_DD){
 //---
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(param.inicio_particion,I_DD,param.valor_n,param.path_disco);

    ARCHIVO_REPORTE<<"DD"<<I_DD<<"[label=\"{<t0>"<<"DD"<<I_DD<<"|";

    for(int i=0; i<6; i++){
        if(i==5){
            ARCHIVO_REPORTE<<"<p"<<i<<">"<<DD.dd_ap_detalle_directorio;
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                ARCHIVO_REPORTE<<"{"<<DD.dd_array_files[i].dd_file_nombre<<"|<p"<<i<<">"<<DD.dd_array_files[i].dd_file_ap_inodo<<"}|";
            }else{
                ARCHIVO_REPORTE<<"{"<<"----"<<"|<p"<<i<<">"<<DD.dd_array_files[i].dd_file_ap_inodo<<"}|";
            }
        }
    }
    ARCHIVO_REPORTE<<"}\"];"<<endl;

    ARCHIVO_REPORTE<<etiquet_AD_DD<<I_AD_DDPadre<<":p"<<apuntador<<"->DD"<<I_DD<<":t0"<<endl;

    if(DD.dd_ap_detalle_directorio!=-1){
       // REPORTAR_TREE_COMPLETO_DD(param,DD.dd_ap_detalle_directorio,5,I_DD,"DD");
       REPORTAR_TREE_DIRECTORIO_DD(param,DD.dd_ap_detalle_directorio,5,I_DD,"DD");
    }
 //---
}

//------------------------------------------------------OBTIENE LAS RUTAS PARA EL MENU DE TREE_FILE -------------------------------------------------
bool REPORTAR_LISTADO_TREE_FILE(struct PARAMETRO_REP_TREE param,vector<string> RUTA,int I_AD,int POSICION_RUTA,bool PERMITIR_LISTADO,string concatenado){
    //---
    struct Arbol_Directorio AD;
    bool ENCONTRADO=false;
   // string CONCATENACION=concatenado+"/";

    AD=PV_RECUPERAR_AD(param.inicio_particion,I_AD,param.valor_n,param.path_disco);

   // if(PV_EXISTE_ARCHIVO_CARPETA(RUTA,I_AD,POSICION_RUTA)){
        for(int i=0; (i<8); i++){
            if((i==7) && (POSICION_RUTA==true)){
                if(AD.avd_ap_detalle_directorio!=-1){
                  //  REPORTAR_LISTADO_TREE_FILE_DD(param,AD.avd_ap_detalle_directorio,concatenado);
                }
            }else
            if((i==6) && (ENCONTRADO==false)){
                if(AD.avd_ap_arbol_virtual_directorio!=-1){
                    ENCONTRADO=REPORTAR_LISTADO_TREE_FILE(param,RUTA,AD.avd_ap_arbol_virtual_directorio,POSICION_RUTA,PERMITIR_LISTADO,concatenado);
                }
            }else if(ENCONTRADO==false){
                if(AD.avd_ap_array_subdirectorios[i]!=-1){
                    struct Arbol_Directorio BUSCAR;
                    BUSCAR=PV_RECUPERAR_AD(param.inicio_particion,AD.avd_ap_array_subdirectorios[i],param.valor_n,param.path_disco);

                    if(strcmp(BUSCAR.avd_nombre_directorio,RUTA[RUTA.size()-1].c_str())==0){
                        ENCONTRADO=true;
                        string nuevo=concatenado+BUSCAR.avd_nombre_directorio+"/";
                       // imprimirln(nuevo);
                        //DIRECTORIOS_TREE_REPORTES.push_back(nuevo);
                        if(BUSCAR.avd_ap_detalle_directorio!=-1){
                            REPORTAR_LISTADO_TREE_FILE_DD(param,BUSCAR.avd_ap_detalle_directorio,nuevo);
                        }
                        ENCONTRADO=REPORTAR_LISTADO_TREE_FILE(param,RUTA,AD.avd_ap_array_subdirectorios[i],POSICION_RUTA,true,nuevo);
                    }else if(PERMITIR_LISTADO==true){
                        string nuevo=concatenado+BUSCAR.avd_nombre_directorio+"/";
                        //imprimirln(nuevo);
                       // DIRECTORIOS_TREE_REPORTES.push_back(nuevo);
                       if(BUSCAR.avd_ap_detalle_directorio!=-1){
                            REPORTAR_LISTADO_TREE_FILE_DD(param,BUSCAR.avd_ap_detalle_directorio,nuevo);
                       }
                        ENCONTRADO=REPORTAR_LISTADO_TREE_FILE(param,RUTA,AD.avd_ap_array_subdirectorios[i],POSICION_RUTA,true,nuevo);
                    }else if(strcmp(BUSCAR.avd_nombre_directorio,RUTA[POSICION_RUTA].c_str())==0){
                        ENCONTRADO=true;
                        if(POSICION_RUTA<(RUTA.size())){
                            string nuevo=concatenado+BUSCAR.avd_nombre_directorio+"/";
                            ENCONTRADO=REPORTAR_LISTADO_TREE_FILE(param,RUTA,AD.avd_ap_array_subdirectorios[i],(POSICION_RUTA+1),false,nuevo);
                        }else{
                            string nuevo=concatenado+BUSCAR.avd_nombre_directorio+"/";
                            ENCONTRADO=REPORTAR_LISTADO_TREE_FILE(param,RUTA,AD.avd_ap_array_subdirectorios[i],POSICION_RUTA,false,nuevo);
                        }

                    }
                    //--
                }
            }
        }
    //---
    return ENCONTRADO;
}

void REPORTAR_LISTADO_TREE_FILE_DD(struct PARAMETRO_REP_TREE param,int I_DD,string concatenado){
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(param.inicio_particion,I_DD,param.valor_n,param.path_disco);


    for(int i=0; i<6; i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                REPORTAR_LISTADO_TREE_FILE_DD(param,DD.dd_ap_detalle_directorio,concatenado);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                string nuevo=concatenado+DD.dd_array_files[i].dd_file_nombre;
                DIRECTORIOS_TREE_REPORTES.push_back(nuevo);
            }
        }
    }

}

void REPORTAR_TREE_FILE_AD(vector<string> RUTA, int POSICION_RUTA,int I_AD,struct PARAMETRO_REP_TREE param){
    struct Arbol_Directorio AD;
    AD=PV_RECUPERAR_AD(param.inicio_particion,I_AD,param.valor_n,param.path_disco);

    if(POSICION_RUTA<RUTA.size()){
        if(VALIDAR_EXTENSION_SINMENSAJE(RUTA[POSICION_RUTA])){//ES ARCHIVO
      //      imprimirln("ENTRA ARCHIVO");
      //      imprimirln(RUTA[POSICION_RUTA]);
      //      imprimirln(AD.avd_nombre_directorio);
      //      imprimirNumln(AD.avd_ap_detalle_directorio);
            if(AD.avd_ap_detalle_directorio!=-1){
             //   imprimirln("si es !=-1");
                REPORTAR_TREE_FILE_DD(param,AD.avd_ap_detalle_directorio,RUTA[POSICION_RUTA]);
            }
        }else{//ES CARPETA
            bool ENCONTRADO=false;
            for(int i=0; (i<7) && (ENCONTRADO==false); i++){
                if(i==6){
                    if(AD.avd_ap_arbol_virtual_directorio!=-1){
                        REPORTAR_TREE_FILE_AD(RUTA,POSICION_RUTA,AD.avd_ap_arbol_virtual_directorio,param);
                    }
                }else{
                    if(AD.avd_ap_array_subdirectorios[i]!=-1){
                        struct Arbol_Directorio BUSCAR;
                        BUSCAR=PV_RECUPERAR_AD(param.inicio_particion,AD.avd_ap_array_subdirectorios[i],param.valor_n,param.path_disco);
                        if(strcmp(BUSCAR.avd_nombre_directorio,RUTA[POSICION_RUTA].c_str())==0){
                         //   imprimirln("ENTRO CARPETA");
                         //   imprimirln(BUSCAR.avd_nombre_directorio);
                            ENCONTRADO=true;
                            if(POSICION_RUTA<RUTA.size()){
                                REPORTAR_TREE_FILE_AD(RUTA,(POSICION_RUTA+1),AD.avd_ap_array_subdirectorios[i],param);
                            }
                        }
                    }
                }
            }
        }
    }
}

void REPORTAR_TREE_FILE_DD(struct PARAMETRO_REP_TREE param,int I_DD,string NOMBRE_ARCHIVO){
    struct Detalle_Directorio DD;
    DD=PV_RECUPERAR_DD(param.inicio_particion,I_DD,param.valor_n,param.path_disco);
  //  imprimirln("ENTRA A METODO DD");

    bool GRAFICADO=false;
    for(int i=0; (i<6) && (GRAFICADO==false); i++){
        if(i==5){
            if(DD.dd_ap_detalle_directorio!=-1){
                REPORTAR_TREE_FILE_DD(param,DD.dd_ap_detalle_directorio,NOMBRE_ARCHIVO);
            }
        }else{
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
            //    imprimirln(DD.dd_array_files[i].dd_file_nombre);
            //    imprimirln(NOMBRE_ARCHIVO);
            //    imprimirln("*-*");
                if(strcmp(DD.dd_array_files[i].dd_file_nombre,NOMBRE_ARCHIVO.c_str())==0){
               //     imprimirln("lo valido como reconocido");
                    GRAFICADO=true;
                    ARCHIVO_REPORTE<<"DD"<<I_DD<<"[label=\"{<t0>"<<"DD"<<I_DD<<"|";
                    ARCHIVO_REPORTE<<"{"<<DD.dd_array_files[i].dd_file_nombre<<"|<p"<<i<<">"<<DD.dd_array_files[i].dd_file_ap_inodo<<"}|";
                    ARCHIVO_REPORTE<<"}\"];"<<endl;
                }
            }
        }
    }
    bool ENCONTRADO=false;
    for(int i=0; (i<5) && (ENCONTRADO==false); i++){
            if(DD.dd_array_files[i].dd_file_ap_inodo!=-1){
                if(strcmp(DD.dd_array_files[i].dd_file_nombre,NOMBRE_ARCHIVO.c_str())==0){
                    ENCONTRADO=true;
                    REPORTAR_TREE_FILE_INODOS(param,DD.dd_array_files[i].dd_file_ap_inodo,i,I_DD,"DD");
                }
            }
    }
}

void REPORTAR_TREE_FILE_INODOS(struct PARAMETRO_REP_TREE param, int I_I,int apuntador,int I_DD_IPadre,string etiqueta_DD_I){
//----
    struct Inodos INODO;
    INODO=PV_RECUPERAR_INODO(param.inicio_particion,I_I,param.valor_n,param.path_disco);

    ARCHIVO_REPORTE<<"I"<<I_I<<"[label=\"{<t0>"<<I_I<<"|";

    for(int i=0; i<5; i++){
        if(i==4){
            ARCHIVO_REPORTE<<"{"<<"<p"<<i<<">"<<INODO.i_array_bloques[i]<<"}";
        }else{
            ARCHIVO_REPORTE<<"{"<<i<<"|<p"<<i<<">"<<INODO.i_array_bloques[i]<<"}|";
        }
    }

    ARCHIVO_REPORTE<<"}\"];"<<endl;

    ARCHIVO_REPORTE<<etiqueta_DD_I<<I_DD_IPadre<<":p"<<apuntador<<"->I"<<I_I<<":t0"<<endl;

    for(int i=0; i<5; i++){
        if(i==4){
            if(INODO.i_ap_indirecto!=-1){
                REPORTAR_TREE_FILE_INODOS(param,INODO.i_ap_indirecto,i,I_I,"I");
            }
        }else{
            if(INODO.i_array_bloques[i]!=-1){
                REPORTAR_TREE_FILE_BLOQUES(param,INODO.i_array_bloques[i],i,I_I,"I");
            }
        }
    }
//----
}

void REPORTAR_TREE_FILE_BLOQUES(struct PARAMETRO_REP_TREE param,int I_B,int apuntador,int I_I_Padre, string etiqueta_I){
    struct Bloque_Datos BLOQUE;
    BLOQUE=PV_RECUPERAR_BLOQUES(param.inicio_particion,I_B,param.valor_n,param.path_disco);

    ARCHIVO_REPORTE<<"B"<<I_B<<"[label=\"{<t0>"<<I_B<<"|"<<BLOQUE.db_data<<"}\"];";

    ARCHIVO_REPORTE<<etiqueta_I<<I_I_Padre<<":p"<<apuntador<<"->B"<<I_B<<":t0"<<endl;
}
//-------------------------------------------------------------------------------------------------

#endif // BLOQUES_ARCHIVOS_VACAS_H_INCLUDED

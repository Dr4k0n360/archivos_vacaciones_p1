#ifndef BLOQUES_ARCHIVOS_H_INCLUDED
#define BLOQUES_ARCHIVOS_H_INCLUDED

#include "ejemplo.h"
#include "discos_particiones.h"
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <dirent.h>
#include <time.h>
#include "validacionescomandos.h"

//Librerias para conexion con Socket
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>


//============ STRUCTS PARA LOS NUEVOS COMANDOS============

struct cmdMKFS{
    string _id;
    string _type;
    string _fs;
    string _unit;
    int _add;
};

struct cmdLOGIN{
    string _usr;
    string _pwd;
    string _id;
};

struct cmdMKGRP{
    string _name;
    string _id;
};

struct cmdMKUSR{
    string _usr;
    string _pwd;
    string _grp;
    string _id;
};

struct cmdMKFILE{
    string _id;
    string _path;
    string _p;
    string _size;
    string _cont;
};

struct cmdMKDIR{
    string _id;
    string _path;
    string _p;
};

struct cmdCAT{
    string _id;
    string _file;
    vector<string> _filen;
};

struct cmdLOSS{
    string _id;
};

struct cmdEDIT{
    string _path;
    string _cont;
    string _size;
    string _id;
};

struct cmdRMGRP{
    string _name;
    string _id;
};

struct cmdRMUSR{
    string _usr;
    string _id;
};

struct cmdCHGRP{
    string _usr;
    string _grp;
};

struct cmdREN{
    string _id;
    string _path;
    string _nombre;
};

struct cmdCHMOD{
    string _id;
    string _path;
    string _ugo;
    string _r;
};

struct cmdCHOWN{
    string _id;
    string _path;
    string _usr;
    string _r;
};

struct cmdMV{
    string _id;
    string _paht;
    string _destiny;
};

struct cmdRM{
    string _id;
    string _path;
    string _rf;
};

struct cmdSYNCRONICE{
    string _id;
};

//============ STRUCTS REFERENTES A LOS FORMATOS DE EXT2/EXT3
struct Super_Bloque{
    int s_filesystem_type;
    int s_inodes_count;
    int s_blocks_count;
    int s_free_blocks_count;
    int s_free_inodes_count;
    time_t s_mtime;
    time_t s_umtime;
    int s_mnt_count;
    int s_magic;
    int s_inode_size;
    int s_block_size;
    int s_first_ino;
    int s_first_blo;
    int s_bm_inode_start;
    int s_bm_block_start;
    int s_inode_start;
    int s_block_start;
};

struct Tabla_Inodos{
    int i_uid;
    int i_gid;
    int i_size;
    time_t i_atime;
    time_t i_ctime;
    time_t i_mtime;
    int i_block[16];
    int i_type;
    int i_perm;
};

struct Content{//ESTE BLOQUE CONTENT SERVIRA PARA LOS APUNTADORES DIRECTOS HACIA ARCHIVOS O CARPETAS
    char b_name[12];
    int b_inodo;
};

struct JOURNALING{
    char RUTA[200];
    char CONTENIDO[200];
    char CP[5];
    time_t FECHA_CREACION;
};

struct CONTADOR_JOURNALING{
    int CONTADOR;
};

//==================== STRUCT DE LOS BLOQUECARPETAS, BLOQUEARCHIVOS Y BLOQUEINODOS======
struct B_Carpetas{
    //char _estado;
    Content b_content[4];
};

struct B_Apuntadores{
    //char _estado;
    int b_pointers[16];
};

struct B_Archivos{
    //char _estado;
    char b_contentA[64];
};

struct DATOS_PUBLICOS{
    string path_disco;
    string path_raid;
    string path_journaling;
    string path_archivoExtra;
    string id_pmount;
    int inicio_particion;
    int valor_N;
    int u_id;
    int g_id;
    string _p_archivo;
    string _p_carpeta;
};

struct PARAMETRO_REP_TREE{
    string path_disco;
    int inicio_particion;
    int valor_n;
};

struct PARAMETROS_SYNCRONICE_PRIMARIA{
    string path_disco;
    string nombre_disco;
    string nombre_particion;
    int inicio_particion;
    int valor_n;
};
//=======================FIN STRUCTS REFERENTES A LOS FORMATOS EXT2/EXT3
bool EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(vector<string> RUTA_BUSCAR,int I_INODO,int POSICION_RUTA);
bool INDIRECTO_EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(vector<string> RUTA_BUSCAR,int I_INDIRECTO,int POSICION_RUTA,int TIPO_APUNTADOR_I, int NIVEL_APUNTADOR_I);
string OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(vector<string> RUTA,int I_INODO,int POSICION_RUTA);
vector<string> OBTENER_DIRECCION(string ruta,char* separador);
bool ES_ARCHIVO_ARCHIVODISCO(string nombre);
string CREAR_GRUPO_ARCHIVODISCO(string DATOSANTIGUOS,string NOMBRE_NGRUPO);
bool EXISTE_GRUPO_ARCHIVODISCO(string DATOSARCHIVO,string NOMBRE_GRUPO);
vector<string> TEXTO_BLOQUES_ARCHIVODISCO(string DATOSARCHIVO);
vector<string> ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(string TEXTOBLOQUE);
bool ESCRIBIR_TEXT_EN_ARCHIVODISCO(vector<string> RUTA,int I_INODO,int P_RUTA);
string INDIRECTO_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(vector<string> RUTA,int P_RUTA,int I_INDIRECTO,int TIPO_APUNTADOR,int NIVEL_APUNTADOR);
int ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
bool INDIRECTO_CARPETAS_ESCRIBIR_TEXT_EN_ARCHIVODISCO(vector<string> RUTA,int I_BLOQUE,int P_RUTA,int TIPO_APUNTADOR,int NIVEL_APUNTADOR);
void INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(vector<string> RUTA,int I_BLOQUE, int P_RUTA,int TIPO_APUNTADOR,int NIVEL_APUNTADOR);
string NOMBRE_EXACTO_10CARACTERES(string NOMBRE);
bool EXISTE_USUARIO_ARCHIVODISCO(string DATOSARCHIVO,string NOMBRE_USUARIO);
string CREAR_USUARIO_ARCHIVODISCO(string DATOS_ANTIGUOS,string grupo, string password, string nombre);
void CREAR_ARCHIVO_ARCHIVODISCO(vector<string> RUTA,int I_INODO,int POSICION_RUTA);
int PERMISO_ESCRITURA_LECTURA(int PERMISOS,int I_UID,int I_GID);
int OBTENER_PERMISO_NUMERICO(char texto);
int CREAR_INODO_Y_BLOQUECARPETA(int INODOPADRE,int TIPO_INODO);
int CREAR_BLOQUECARPETA(int INODOACTUAL,int INODOPADRE,bool BLOQUEPRINCIPAL);
bool CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(vector<string> RUTA,int I_BLOQUE,int P_RUTA,int TIPO_APUNTADOR,int NIVEL_APUNTADOR,int INODO_PADRE);
bool EXISTENCIA_APUNTADOR_INDIRECTO_ARCHIVODISCO(vector<string> RUTA,int I_BLOQUE, int P_RUTA, int TIPO_APUNTADOR,int NIVEL_APUNTADOR,int INODO_PADRE);
FILE *ARCHIVO_GLOBAL;
ofstream ARCHIVO_REPORTE;
FILE *ARCHIVO_PIVOTE;
string INDIRECTO_BLOQUESCARPETAS_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(vector<string> RUTA, int P_RUTA, int I_BLOQUE,int TIPO_APUNTADOR, int NIVEL_APUNTADOR);
string OBTENER_RUTA_COMPLETA_JOURNALING(vector<string> direccion);
void ACTUALIZACION_JOURNALING_A_Y_C_ARCHIVODISCO(vector<string> RUTA,string ORIGEN_DATOS,string crearPadres,string path_journaling);
void INICIO_REPORTE_INODOS(struct PARAMETRO_REP_TREE parametros, int I_INODO,int apuntador,int I_carpeta);
void REPORTE_INDIRECTOS_CARPETA(struct PARAMETRO_REP_TREE parametros,int I_bloque,int apuntador,int I_INO_AI, int nivel, int tipo);
void INICIO_REPORTE_LS(vector<string> RUTA,int I_inodo,int P_RUTA);
string RECUPERAR_NOMBRE_GRUPO(int gID);
string RECUPERAR_NOMBRE_USUARIO(int uID);
string RECUPERAR_PERMISO_INDIVIDUAL(char numero);
string PERMISOS_RECUPERADOS(int permisos);
string RECUPERAR_TIPO(int tipo);
bool INDIRECTO_REPORTE_LS(vector<string> RUTA,int I_bloque, int P_Ruta,int TIPO_APUNTADOR, int NIVEL_APUNTADOR);
string REMOVER_GRUPO(string DATOS,string nombreGrupo);
string REMOVER_USUARIO(string DATOS,string nombreUsuario);

//DECLARAMOS LOS METODOS DE VALIDACIONES_COMANDOSBLOQUES.H EN ESTE ESPACIO
struct Tabla_Inodos RECUPERAR_INODO(int P_I_P,int I_I,int V_N, string path_disco);
struct B_Carpetas RECUPERAR_BLOQUE_CARPETA(int P_I_P, int I_B, int V_N, string path_disco);
struct B_Archivos RECUPERAR_BLOQUE_ARCHIVO(int P_I_P, int I_A, int V_N, string path_disco);
struct B_Apuntadores RECUPERAR_BLOQUE_INDIRECTO(int P_I_P, int I_B, int V_N, string path_disco);
void GUARDAR_BLOQUE_ARCHIVOS(string path_disco,int POSICION,struct B_Archivos BLOQUE_ARCHIVO);
void GUARDAR_INODO_EN_DISCO(struct Tabla_Inodos INODO,int posision_inicio,string path_disco);
void GUARDAR_BLOQUE_INDIRECTO(string path_disco,int posicion,struct B_Apuntadores BLOQUE_INDIRECTO);
bool EXISTE_CARPETA_ARCHIVO_EN_INODO(struct Tabla_Inodos INODO,string NOMBRE_C_A,int I_P, int I_I_P,int V_N, string path_disco);
void GUARDAR_BLOQUE_CARPETAS(string path_disco, int posicion,struct B_Carpetas BLOQUE_CARPETA);
void GUARDAR_STRUCT_JOURNALING(string path_jour,struct JOURNALING JOUR);
string CAMBIAR_GRUPO_USUARIO(string DATOS,string nombreUsuario,string nombreGrupo);
void CONECTAR_ENVIAR_MENSAJE_SERVIDOR(string texto_enviar);
void INICIO_RECORRIDO_SYN_INODO(struct PARAMETROS_SYNCRONICE_PRIMARIA parametros,int I_inodo,string padre,int Identificador);
void RECORRIDO_SYN_INDIRECTOS_CA(struct PARAMETROS_SYNCRONICE_PRIMARIA parametros,int I_Bloque,int I_CA,int nivel, int tipo,string padre,int Identificador);
string RECPERAR_TEXTO_ARCHIVO_SIN_SIMBOLOS(string texto);
string RECUPERAR_TEXT_CHAR_STRING(string texto);
vector<string> OBTENER_USUARIOS_DE_PARTICION(string texto_archivo_usuarios);
//METODOS PARA LOS COMANDOS LOGIN EN ADELANTE
vector<DATOS_PUBLICOS> DATOS_PIVOTE;
vector<string> DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR;
//ESTOS METODOS SON PENSANDO EN LAS CARPETAS Y ARCHIVOS CON VERIFICACION DE PERMISOS
bool EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(vector<string> RUTA_BUSCAR,int I_INODO,int POSICION_RUTA){
    //RECUPERAR INODO EN EL CUAL SE BUSCAR EN SUS BLOQUE Y PUNTADORES INDIRECTOS
 //   imprimirln("entro a metodo");
 //   imprimirNumln(POSICION_RUTA);
    bool existe=false;
    struct Tabla_Inodos INODO;
    INODO=RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,I_INODO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    //SI ESTE IF ES VALIDO EL INODO ES UN INODO DE CARPETAS
    if(INODO.i_type==0){
   //     imprimirln("inodo carpeta");
        //imprimirln("ES INODO CARPETA CON PERMISOS: ");
        for(int i=0; (i<16) && (existe==false); i++){//RECORREMOS CADA APUNTADOR DEL INODO
            if(INODO.i_block[i]!=-1){
                if(i==13){
                    existe=INDIRECTO_EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(RUTA_BUSCAR,INODO.i_block[i],POSICION_RUTA,13,1);
                }else if(i==14){
                    existe=INDIRECTO_EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(RUTA_BUSCAR,INODO.i_block[i],POSICION_RUTA,14,2);
                }else if(i==15){
                    existe=INDIRECTO_EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(RUTA_BUSCAR,INODO.i_block[i],POSICION_RUTA,15,3);
                }else{//VA A ENTRAR AQUI SI ES UNO DE LOS APUNTADORES DEL 0-12
                    struct B_Carpetas CARPETA;
                    CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,INODO.i_block[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                    //RRECORREMOS LOS APUNTADORES DEL BLOQUE CARPETA
                    for(int j=0; (j<4) && (existe==false); j++){
                        if(CARPETA.b_content[j].b_inodo!=-1){
                            if(strcmp(CARPETA.b_content[j].b_name,RUTA_BUSCAR[POSICION_RUTA].c_str())==0){
                                if((RUTA_BUSCAR.size()-1)==POSICION_RUTA){//SI ESTO ES CIERTO SIGNIFICA QUE LLEGO AL FINAL DE LAS RUTAS DEL VECTOR
                                    //imprimirln("ENCONTRO ARCHIVO/CARPETA");
                                    existe=true;
                                }else{//SIGNIFICA QUE EXISTE QUE ES UNA CARPETA PADRE Y NO LLEGO AL FINAL DEL VECTOR RUTAS
                                //    imprimirln("lo encontro pero no es el final:");
                                    existe=EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(RUTA_BUSCAR,CARPETA.b_content[j].b_inodo,(POSICION_RUTA+1));
                                }
                            }
                        }
                    }

                }
            }
        }
    }else if(INODO.i_type==1){//EN CASO CONTRARIO ES UN INODO DE ARCHIVOS
        imprimirln("==== ERROR: UN ARCHIVO NO ES PADRE DE OTRO ARCHIVO/CARPETA VERIFIQUE SU RUTA ====");
    }

    return existe;
}

bool INDIRECTO_EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(vector<string> RUTA_BUSCAR,int I_INDIRECTO,int POSICION_RUTA,int TIPO_APUNTADOR_I, int NIVEL_APUNTADOR_I){
    bool existe=false;
    struct B_Apuntadores APUNTADOR_INDIRECTO;
    APUNTADOR_INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].inicio_particion,I_INDIRECTO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    NIVEL_APUNTADOR_I--;
    if((TIPO_APUNTADOR_I==13) && (NIVEL_APUNTADOR_I==0)){
        for(int i=0; (i<13) && (existe==false); i++){
            if(APUNTADOR_INDIRECTO.b_pointers[i]!=-1){

                struct B_Carpetas CARPETA;
                CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,APUNTADOR_INDIRECTO.b_pointers[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                for(int j=0; (j<4) && (existe==false); j++){
                    if(CARPETA.b_content[j].b_inodo!=-1){
                        if(strcmp(CARPETA.b_content[j].b_name,RUTA_BUSCAR[POSICION_RUTA].c_str())==0){
                            if((RUTA_BUSCAR.size()-1)==POSICION_RUTA){
                                existe=true;
                            }else{
                                existe=EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(RUTA_BUSCAR,CARPETA.b_content[j].b_inodo,(POSICION_RUTA+1));
                            }
                        }
                    }
                }
            }
        }
    }else
    if((TIPO_APUNTADOR_I==14) && (NIVEL_APUNTADOR_I==1)){
        if(APUNTADOR_INDIRECTO.b_pointers[13]!=-1){
           existe=INDIRECTO_EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(RUTA_BUSCAR,APUNTADOR_INDIRECTO.b_pointers[13],POSICION_RUTA,13,1);
        }
    }else
    if((TIPO_APUNTADOR_I==15) && (NIVEL_APUNTADOR_I==2)){
        if(APUNTADOR_INDIRECTO.b_pointers[13]!=-1){
            existe=INDIRECTO_EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(RUTA_BUSCAR,APUNTADOR_INDIRECTO.b_pointers[13],POSICION_RUTA,13,2);
        }
    }else
    if((TIPO_APUNTADOR_I==13) && (NIVEL_APUNTADOR_I!=0)){
        if(APUNTADOR_INDIRECTO.b_pointers[13]!=-1){
            existe=INDIRECTO_EXISTE_ARCHIVO_O_CARPETA_EN_ARCHIVODISCO(RUTA_BUSCAR,APUNTADOR_INDIRECTO.b_pointers[13],POSICION_RUTA,13,1);
        }
    }
    return existe;
}

string OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(vector<string> RUTA,int I_INODO,int POSICION_RUTA){
    string respuesta="";//se modifico y se le puso ""
    bool continuar=false;
//    imprimirln("entro a metodo texto usr");

    struct Tabla_Inodos INODO;
    INODO=RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,I_INODO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    if(INODO.i_type==0){//SI ES UN INODO BLOQUE
        if(strcmp("users.txt",RUTA[POSICION_RUTA].c_str())==0){//PARA RECUPERAR LOS DATOS DEL ARCHIVO USERS.TXT ASI QUE NO SE VALIDA PERMISOS
            for(int i=0; (i<16) && (continuar==false); i++){
                if(INODO.i_block[i]!=-1){//EN BLANCO LOS INDIRECTOS YA QUE USERSR.TXT SIEMPRE ESTA EN EL PRIMER DIRECTO INODO 0
                    if(i==13){

                    }else if(i==14){

                    }else if(i==15){

                    }else{
                        struct B_Carpetas CARPETA;
                        CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,INODO.i_block[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                        for(int j=0; (j<4) && (continuar==false); j++){
                            if(CARPETA.b_content[j].b_inodo!=-1){
                                if(strcmp(CARPETA.b_content[j].b_name,RUTA[POSICION_RUTA].c_str())==0){//AQUI VA A ENTRAR SI ES EL ARCHIVO USERS.TXT
                                    continuar=true;
                                    respuesta=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,POSICION_RUTA);
                                }
                            }
                        }
                    }
                }
            }
        }else{//PARA RECUPERAR CUALQUIER OTR ARCHIVO QUE NO SEA USERS.TXT EN DONDE SE DEBEN VALIDAR PERMISOS
            //ESTE ELSE SIGNIFICA QUE ESTAMOS EN UN INODO CARPETA Y COMO QUERESMOS OBTENER TEXTO DEBEMOS VALIDAR
            //QUE TENGA PERMISOS DE LECTURA
            if(POSICION_RUTA<RUTA.size()){
     //           imprimirln("entro a buscar otro archivo/carpeta");
                //VALIDANDO PERMISOS DE LECTURA
                if((PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==4) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==5) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==6) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==7)){
                    //COMO ES LECTURA SOLO DEVEMOS VALIDAR QUE APUNTE A !=-1
                    //RECORREMOS LOS APUNTADORES DE LOS INODOS
                    for(int i=0; (i<16) && (continuar==false) && (respuesta==""); i++){
                        if(INODO.i_block[i]!=-1){
                            //RECUPERAMOS EL BLOQUE CARPETA AL QUE APUNTA Y LO RECORREMOS
                            if(i==13){
                                respuesta=INDIRECTO_BLOQUESCARPETAS_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,POSICION_RUTA,INODO.i_block[i],13,1);
                            }else
                            if(i==14){
                                respuesta=INDIRECTO_BLOQUESCARPETAS_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,POSICION_RUTA,INODO.i_block[i],14,2);
                            }else
                            if(i==15){
                                respuesta=INDIRECTO_BLOQUESCARPETAS_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,POSICION_RUTA,INODO.i_block[i],15,3);
                            }else{
                                struct B_Carpetas CARPETA;
                                CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,INODO.i_block[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                //RECORREMOS EL BLOQUE CARPETA
                                for(int j=0; (j<4) && (continuar==false);j++){
                                    if(CARPETA.b_content[j].b_inodo!=-1){

                                        if(ES_ARCHIVO_ARCHIVODISCO(RUTA[POSICION_RUTA])){
                                            //SI ENTRA A ESTE IF SIGNIFICA QUE ES UN ARCHIVO POR LO TANTO NECESITAMOS RECUPERARLO
                                            //TODO SU TEXTO LLAMANDO RECURSIVAMENTE A ESTE METODO
                                            if(strcmp(CARPETA.b_content[j].b_name,RUTA[POSICION_RUTA].c_str())==0){
                                                //NO ESTA DE MAS VERIFICAR QUE SEA EL ARCHIVO QUE SE BUSCA
                                                continuar=true;
                                                respuesta=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,POSICION_RUTA);
                                            }
                                        }else{
                                            //SI NO ES EL ARCHIVO SIGNIFICA QUE ES UNA CARPETA POR LO TANTO DEBEMOS MANDARLE EL SIGUENTE
                                            //POSICION RUTA
                                            //PERO PRIMERO DEBEMOS VERIFICAR QUE SEA LA CARPETA EXACTA Y NO OTRA CARPETA
                                            if(strcmp(CARPETA.b_content[j].b_name,RUTA[POSICION_RUTA].c_str())==0){
                                                //SI ENTRA AQUI SIGNIFICA QUE ENCONTRO LA CARPETA CORRESPONDIENTE A LA QUE VENIA
                                                //Y COMO QUEREMOS RECUPERAR DATOS DEL ARCHIVO Y ESTE NO ES ARCHIVO TENEMOS QUE SEGUIR
                                                //BUSCANDO
                                                continuar=true;
                                                respuesta=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(POSICION_RUTA+1));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    imprimirln("=== ERROR: NO CUENTA CON PERMISOS DE LECTURA ===");
                }
            }else{
                imprimirln("==== ERROR: ACABA DE EXCEDERSE DEL TAMANIO DE LAS RUTAS ====");
            }
        }
    }else
    if(INODO.i_type==1){//SI ES UN INODO ARCHIVO
        if((RUTA.size()-1)==POSICION_RUTA){
            if(strcmp("users.txt",RUTA[POSICION_RUTA].c_str())==0){//ES EL ARCHIVO USERS NO NECESITA PERMISOS
                for(int i=0; i<16; i++){
                    if(INODO.i_block[i]!=-1){
                        if(i==13){
                            respuesta+=INDIRECTO_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,POSICION_RUTA,INODO.i_block[i],13,1);
                        }else if(i==14){
                            respuesta+=INDIRECTO_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,POSICION_RUTA,INODO.i_block[i],14,2);
                        }else if(i==15){
                            respuesta+=INDIRECTO_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,POSICION_RUTA,INODO.i_block[i],15,3);
                        }else{
                            struct B_Archivos CONTENIDO_ARCHIVO;
                            CONTENIDO_ARCHIVO=RECUPERAR_BLOQUE_ARCHIVO(DATOS_PIVOTE[0].inicio_particion,INODO.i_block[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                            respuesta+=CONTENIDO_ARCHIVO.b_contentA;
                        }
                    }
                }
            }else{//NO ES EL ARCHIVO USERS, NECESITA VALIDARSE PERMISOS
                if(POSICION_RUTA<RUTA.size()){
             //       imprimirln("ENTRO A UN INODO DE ARCHIVOS PARA RECUPERAR TODOS LOS DATOS DEL ARCHIVO");
                    if((PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==4) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==5) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==6) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==7)){

                        for(int i=0; i<16; i++){
                            if(INODO.i_block[i]!=-1){
                                if(i==13){
                                    respuesta+=INDIRECTO_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,POSICION_RUTA,INODO.i_block[i],13,1);
                                }else if(i==14){
                                    respuesta+=INDIRECTO_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,POSICION_RUTA,INODO.i_block[i],14,2);
                                }else if(i==15){
                                    respuesta+=INDIRECTO_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,POSICION_RUTA,INODO.i_block[i],15,3);
                                }else{
                                //VA A RECUPERAR EL TEXTO DEL INODO ARCHIVO YA QUE SI TIENE PERMISOS DE LECTURA
                                    struct B_Archivos ARCHIVO;
                                    ARCHIVO=RECUPERAR_BLOQUE_ARCHIVO(DATOS_PIVOTE[0].inicio_particion,INODO.i_block[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                    respuesta+=ARCHIVO.b_contentA;
                                }
                            }
                        }

                    }else{
                        imprimirln("=== ERROR: NO TIENE PERMISOS DE LECTURA AL INODO ARCHIVO ===");
                    }
                }else{
                    imprimirln("EXCEDIO LIMITES DIRECCION INODO/ARCHIVO");
                }
            }
        }else{
            imprimirln("=== ERROR: UN ARCHIVO NO ES PADRE DE OTRO ARCHIVO/CARPETA ===");
        }
    }

    return respuesta;
}
//ESTE METODO ES PARA LOS APUNTADORES INDIRECTOS DE UN INODO ARCHIVOS
//YA QUE ESTOS NO APUNTAN A BLOQUECARPETAS SI NO A BLOQUEARCHIVOS
string INDIRECTO_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(vector<string> RUTA,int P_RUTA,int I_INDIRECTO,int TIPO_APUNTADOR,int NIVEL_APUNTADOR){
    string respuesta;

    struct B_Apuntadores INDIRECTO;
    INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].inicio_particion,I_INDIRECTO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    NIVEL_APUNTADOR--;
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR==0)){
        for(int i=0; i<13; i++){
            if(INDIRECTO.b_pointers[i]!=-1){
                //RECUPERO EL STRUCT DEL ARCHIVO
                struct B_Archivos ARCHIVO;
                ARCHIVO=RECUPERAR_BLOQUE_ARCHIVO(DATOS_PIVOTE[0].inicio_particion,INDIRECTO.b_pointers[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                respuesta+=ARCHIVO.b_contentA;
            }
        }
    }else
    if((TIPO_APUNTADOR==14) && (NIVEL_APUNTADOR==1)){
        if(INDIRECTO.b_pointers[13]!=-1){
            respuesta=INDIRECTO_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,P_RUTA,INDIRECTO.b_pointers[13],13,1);
        }
    }else
    if((TIPO_APUNTADOR==15) && (NIVEL_APUNTADOR==2)){
        if(INDIRECTO.b_pointers[13]!=-1){
            respuesta=INDIRECTO_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,P_RUTA,INDIRECTO.b_pointers[13],13,2);
        }
    }else
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR!=0)){
        if(INDIRECTO.b_pointers[13]!=-1){
            respuesta=INDIRECTO_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,P_RUTA,INDIRECTO.b_pointers[13],13,1);
        }
    }

    return respuesta;
}
//ESTE METODO ES PARA LOS APUNTADORES INDIRECTOS DE UN INODO CARPETA
//YA QUE ESTOS SOLO PUENDEN APUNTAR A BLOQUESCARPETAS Y BLOQUESARCHIVOS
string INDIRECTO_BLOQUESCARPETAS_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(vector<string> RUTA, int P_RUTA, int I_BLOQUE,int TIPO_APUNTADOR, int NIVEL_APUNTADOR){
    string respuesta;
    bool encontrado=false;
    struct B_Apuntadores INDIRECTO;
    INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].inicio_particion,I_BLOQUE,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    NIVEL_APUNTADOR--;
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR==0)){
        for(int i=0; (i<13) && (encontrado==false); i++){
            if(INDIRECTO.b_pointers[i]!=-1){
                struct B_Carpetas CARPETA;
                CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,INDIRECTO.b_pointers[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

                for(int j=0; (j<4) && (encontrado==false); j++){
                    if(CARPETA.b_content[j].b_inodo!=-1){
                        if(strcmp(CARPETA.b_content[j].b_name,RUTA[P_RUTA].c_str())==0){
                            encontrado=true;
                            if(ES_ARCHIVO_ARCHIVODISCO(RUTA[P_RUTA])){//VERIFICA SI ES UN ARCHIVO EL NAME DEL BLOQUE LEIDO
                                //NO VERIFICO SI ES EL ULTIMO DE LA BUSQUEDA YA QUE ANTES DE LLAMAR A ESTE SE PONDRA EN UNA
                                //VALIDACION DE RUTA TODO EL METODO QUE CONTIEN TODO ESTO
                                respuesta+=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,P_RUTA);
                            }else{//EN CASO CONTRARIO ES UNA CARPETA
                                respuesta+=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(P_RUTA+1));
                            }
                        }
                    }
                }

            }
        }
    }else
    if((TIPO_APUNTADOR==14) && (NIVEL_APUNTADOR==1)){
        if(INDIRECTO.b_pointers[13]!=-1){
            respuesta=INDIRECTO_BLOQUESCARPETAS_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,P_RUTA,INDIRECTO.b_pointers[13],13,1);
        }
    }else
    if((TIPO_APUNTADOR==15) && (NIVEL_APUNTADOR==2)){
        if(INDIRECTO.b_pointers[13]!=-1){
            respuesta=INDIRECTO_BLOQUESCARPETAS_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,P_RUTA,INDIRECTO.b_pointers[13],13,2);
        }
    }else
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR!=0)){
        if(INDIRECTO.b_pointers[13]!=-1){
            respuesta=INDIRECTO_BLOQUESCARPETAS_OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(RUTA,P_RUTA,INDIRECTO.b_pointers[13],13,1);
        }
    }

    return respuesta;
}

bool ESCRIBIR_TEXT_EN_ARCHIVODISCO(vector<string> RUTA,int I_INODO,int P_RUTA){
    bool encontrada=false;
    struct Tabla_Inodos INODO;
    INODO=RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,I_INODO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
//    imprimirln("ENTRO A METODO DE ESCRIBIR TEXT EN ARCHIV DISCO");
    if(P_RUTA<RUTA.size()){//VALLIDO QUE NO ME PASE DEL VECTOR PARA QUE NO DE ERROR
                if(INODO.i_type==0){ //ES UN INODO DE CARPETA
                    if(strcmp("users.txt",RUTA[P_RUTA].c_str())==0){//SIGNIFICA QUE QUIERE ESCRIBIR EL ARCHIVO USERS NO VALIDA PERMISOS
                        //RECORRO INODO
                        for(int i=0; (i<16) && (encontrada==false); i++){
                            if(INODO.i_block[i]!=-1){
                                if(i==13){
                                    encontrada=INDIRECTO_CARPETAS_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,13,1);
                                }else if(i==14){
                                    encontrada=INDIRECTO_CARPETAS_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,14,2);
                                }else if(i==15){
                                    encontrada=INDIRECTO_CARPETAS_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,15,3);
                                }else{
                                    struct B_Carpetas CARPETA;
                                    CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,INODO.i_block[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                    for(int j=0; (j<4) && (encontrada==false); j++){
                                        if(CARPETA.b_content[j].b_inodo!=-1){
                                            if(strcmp(CARPETA.b_content[j].b_name,RUTA[P_RUTA].c_str())==0){
                                                encontrada=true;
                                               if(ES_ARCHIVO_ARCHIVODISCO(CARPETA.b_content[j].b_name)){
                                    //                imprimirln("ingreso a es archivo archivodisco if");
                                                    ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(P_RUTA));
                                               }else{
                                   //                 imprimirln("ingreso creyendo que es carpeta");
                                                    ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(P_RUTA+1));
                                               }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }else{//ES OTRO ARCHIVO SE VALIDAN PERMISOS
         //               imprimirln("ENTRO A ES OTRO ARCHISO DONDE SE VALIDAN PERMISOS");
                        if((PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==2) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==3) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==6) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==7)){
                            for(int i=0; (i<16) && (encontrada==false); i++){
                            //RECORRO EL INODO DE CARPETAS YA QUE ESTOY EN UN INODO DE CARPETA NECESITO VERIFICAR SI EL ARCHIVO ESTA AQUI
                            //O SE ENCUENTRA EN ALGUN INDIRECTO DE ESTE INODO YA QUE TIENE QUE EXISTIR PARA AGREGARLE CONTENIDO DE LO CONTRARIO
                            //SI NO EXISTE NO SE AGREGA NADA
                                if(INODO.i_block[i]!=-1){
                                    if(i==13){
                                        encontrada=INDIRECTO_CARPETAS_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,13,1);
                                    }else if(i==14){
                                        encontrada=INDIRECTO_CARPETAS_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,14,2);
                                    }else if(i==15){
                                        encontrada=INDIRECTO_CARPETAS_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,15,3);
                                    }else{
                                        struct B_Carpetas CARPETA;
                                        CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,INODO.i_block[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                        for(int j=0; (j<4) && (encontrada==false); j++){
                                            if(CARPETA.b_content[j].b_inodo!=-1){
                                                if(strcmp(CARPETA.b_content[j].b_name,RUTA[P_RUTA].c_str())==0){
                                                    if(ES_ARCHIVO_ARCHIVODISCO(RUTA[P_RUTA])){
                                                    //SI ENTRA AQUI SIGNIFICA QUE SI ES EL ARCHIVO QUE ESTAMOS BUSCANDO
                                                    //Y EL CUAL VAMOS A ESCRIBIR
                                //                    imprimirln("ENCONTRO EL ARCHIVO SE LLAMARA RECURSIVAMENTE");
                                                    encontrada=true;
                                                    ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,P_RUTA);
                                                    }else{
                                                    //SI NO ES SIGNIFICA QUE ES UNA CARPETA PADRE DEL ARCHIVO QUE ESTAMOS BUSCANDO
                                                    encontrada=true;
                                //                    imprimirln("ENCONTRO UNA CARPETA SE LLAMARA RECURSIVAMENTE");
                                                    ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(P_RUTA+1));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }else{
                            imprimirln("=== ERROR: NO TIENE PERMISOS DE ESCRITURA PARA ESCRIBRI EN EL INODO CARPETA ===");
                        }
                    }
                }else if(INODO.i_type==1){//ES UN INODO DE ARCHIVOS
      //              imprimirln("ENTRO A UN INODO DE ARCHISO");
                    if(strcmp("users.txt",RUTA[P_RUTA].c_str())==0){
     //                   imprimirln("LLEGO HASTA LA VALIDACION PARA CREAR LOS NUEVOS BLOQUES Y GUARDAR");
                        //RECORRO EL INODO POR TODOS SUS APUNTADORES Y MIENTRAS HALLA AUN BLOQUES POR GUARDAR
                        for(int i=0; (i<16) && (DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()!=0); i++){
                            if(i==13){//PARA LOS APUNTADORES INDIRECTOS
                                if(INODO.i_block[i]!=-1){//SI ES DIFERENTE A MENOS 1 RECUPERAMOS EL INDIRECTO Y LO RECORREMOS SI NO CREAMOS EL INDIRECTO
                                //ACTUALIZAMOS EL INODO PARA QUE APUNTE AL NUEVO INDIRECTO Y ACTUALIZAMOS EL INDIRECTO A SUS RESPECTIVOS INODS ARCHIVOS
                                    INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,13,1);
                                }else{//ENTRA EN ELSE SI ES IGUAL A MENOS UNO
                                    INODO.i_block[i]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
                                    int POSICIONGUARDAR_INODO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(I_INODO*sizeof(Tabla_Inodos));
                                    GUARDAR_INODO_EN_DISCO(INODO,POSICIONGUARDAR_INODO,DATOS_PIVOTE[0].path_disco);
                                    INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,13,1);
                                }
                            }else if(i==14){
                                if(INODO.i_block[i]!=-1){
                                    INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,14,2);
                                }else{
                                    INODO.i_block[i]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
                                    int POSICIONGUARDAR_INODO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(I_INODO*sizeof(Tabla_Inodos));
                                    GUARDAR_INODO_EN_DISCO(INODO,POSICIONGUARDAR_INODO,DATOS_PIVOTE[0].path_disco);
                                    INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,14,2);
                                }
                            }else if(i==15){
                                if(INODO.i_block[i]!=-1){
                                    INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,15,3);
                                }else{
                                    INODO.i_block[i]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
                                    int POSICION_GUARDAR_INODO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(I_INODO*sizeof(Tabla_Inodos));
                                    GUARDAR_INODO_EN_DISCO(INODO,POSICION_GUARDAR_INODO,DATOS_PIVOTE[0].path_disco);
                                    INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,15,3);
                                }
                            }else{//PARA LOS APUNTADORES DIRECTOS
                                struct B_Archivos ARCHIVOBLOQUE;
                                strcpy(ARCHIVOBLOQUE.b_contentA,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());
                                //cout<<ARCHIVOBLOQUE.b_contentA<<endl;
                                int POSICIONINICIO=2*(DATOS_PIVOTE[0].valor_N);
                                int POSICIONFINAL=3*(DATOS_PIVOTE[0].valor_N);

                                if(INODO.i_block[i]!=-1){
                                    //imprimirln("entro al primer if va a guardar el mismo bloque con id");
                                    //imprimirNumln(INODO.i_block[i]);
                                    int POSICIONGUARDAR=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(2*DATOS_PIVOTE[0].valor_N*sizeof(B_Archivos))+(INODO.i_block[i]*sizeof(B_Archivos));
                                    //imprimirNumln(POSICIONGUARDAR);
                                    GUARDAR_BLOQUE_ARCHIVOS(DATOS_PIVOTE[0].path_disco,POSICIONGUARDAR,ARCHIVOBLOQUE);
                                    DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                }else{
                                  //  FILE *archivo=fopen(DATOS_PIVOTE[0].path_archivoExtra.c_str(),"rb+");
                                    if(ARCHIVO_GLOBAL==NULL){
                                        ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_archivoExtra.c_str(),"rb+");
                                    }else{
                                        imprimirln("888888888888888 ERROR ARCHIVO ABIERTO 88888888888");
                                    }
                                    bool BLOQUEGUARDADO=false;
                                    int IDENTIFICADO_ID=0;
                                    char verificar;
                                    for(int k=POSICIONINICIO; (k<POSICIONFINAL) && (BLOQUEGUARDADO==false);k++){
                                        /*char verificar;
                                        fseek(archivo,k,SEEK_SET);
                                        fread(&verificar,1,1,archivo);*/
                                        verificar=00;
                                        fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                        fread(&verificar,1,1,ARCHIVO_GLOBAL);

                                        if(verificar==48){
                                            char respuesta=49;
                                            /*fseek(archivo,k,SEEK_SET);
                                            fwrite(&respuesta,1,1,archivo);
                                            fflush(archivo);
                                            fclose(archivo);*/
                                            fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                                            fwrite(&respuesta,1,1,ARCHIVO_GLOBAL);
                                            fflush(ARCHIVO_GLOBAL);
                                            fclose(ARCHIVO_GLOBAL);
                                            ARCHIVO_GLOBAL=NULL;
                                            BLOQUEGUARDADO=true;
                                            int POSICIONGUARDAR=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(2*DATOS_PIVOTE[0].valor_N*sizeof(B_Archivos))+(IDENTIFICADO_ID*sizeof(B_Archivos));
                                            //DEVO GUARDAR EL INODO Y EL BLOQUE DE ARCHIVOS EN SU UBICACION
                                            GUARDAR_BLOQUE_ARCHIVOS(DATOS_PIVOTE[0].path_disco,POSICIONGUARDAR,ARCHIVOBLOQUE);
                                            //ACTUALIZO EL INODO
                                            INODO.i_block[i]=IDENTIFICADO_ID;
                                            int POSICIONGUARDARINODO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(I_INODO*sizeof(Tabla_Inodos));
                                            GUARDAR_INODO_EN_DISCO(INODO,POSICIONGUARDARINODO,DATOS_PIVOTE[0].path_disco);
                                            DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                        }
                                        IDENTIFICADO_ID++;

                                    }
                                    if(ARCHIVO_GLOBAL!=NULL){
                                        fflush(ARCHIVO_GLOBAL);
                                        fclose(ARCHIVO_GLOBAL);
                                        ARCHIVO_GLOBAL=NULL;
                                    }
                                }
                            }
                        }

                    }else{
            //            imprimirln("NO ES EL ARCHIVO USERS");
                        if((PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==2) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==3) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==6) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==7)){
                            //RECORREMOS EL INODO DE ARCHIVO PARA BUSCAR POSICIONES EN LAS CUALES SE VA A GUARDAR EL BLOQUE
                            //DEL NUEVO ARCHIVO QUE SE VA A ESCRIBIR
                            for(int i=0; (i<16) && (DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()!=0); i++){
                //                imprimirln("RECORRIENDO EL INODO ARCHIVO CON LOS DATOS...");
                //                imprimirNumln(INODO.i_block[i]);
                //                imprimirNumln(i);
                //                imprimirNumln(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size());
                                if(i==13){
                                    if(INODO.i_block[i]!=-1){
                    //                    imprimirln("VA A ENTRAR A INODO INDIRECTO 13");
                                        INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,13,1);
                                    }else{
                                        INODO.i_block[i]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
                                        int POSICION_GUARDAR_IACTUALIZADO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(I_INODO*sizeof(Tabla_Inodos));
                                        GUARDAR_INODO_EN_DISCO(INODO,POSICION_GUARDAR_IACTUALIZADO,DATOS_PIVOTE[0].path_disco);
                                        INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,13,1);
                                    }
                                }else if(i==14){
                                    if(INODO.i_block[i]!=-1){
                                        INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,14,2);
                                    }else{
                                        INODO.i_block[i]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
                                        int POSICION_GUARDAR=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(I_INODO*sizeof(Tabla_Inodos));
                                        GUARDAR_INODO_EN_DISCO(INODO,POSICION_GUARDAR,DATOS_PIVOTE[0].path_disco);
                                        INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,14,2);
                                    }
                                }else if(i==15){
                                    if(INODO.i_block[i]!=-1){
                                        INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,15,3);
                                    }else{
                                        INODO.i_block[i]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
                                        int POSICION_GUARDAR=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(I_INODO*sizeof(Tabla_Inodos));
                                        GUARDAR_INODO_EN_DISCO(INODO,POSICION_GUARDAR,DATOS_PIVOTE[0].path_disco);
                                        INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INODO.i_block[i],P_RUTA,15,3);
                                    }
                                }else{
                                    //LLENAMOS LOS DATOS DEL BLOQUE_ARCHIVO
                                    struct B_Archivos ARCHIVO;
                                    strcpy(ARCHIVO.b_contentA,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());

                                    int POSICION_INICIO_BITMPAARCHIVOS=2*DATOS_PIVOTE[0].valor_N;
                                    int POSICION_FINAL_BITMAPARCHIVOS=3*DATOS_PIVOTE[0].valor_N;

                                    if(INODO.i_block[i]!=-1){
                   //                     imprimirln("ENTRO A QUE SI APUNTA ALGO, POR LO TANTO SE ACTUALIZARA INODO");
                                        int POSICION_GUARDAR=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(2*DATOS_PIVOTE[0].valor_N*sizeof(B_Archivos))+(INODO.i_block[i]*sizeof(B_Archivos));
                                        GUARDAR_BLOQUE_ARCHIVOS(DATOS_PIVOTE[0].path_disco,POSICION_GUARDAR,ARCHIVO);
                                        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                    }else{
                                    //SI EL APUNTADOR APUNTA A -1 Y AUN TENEMOS DATOS SIGNIFICA QUE GUARDARA EN ESTE APUNTADOR UN NUEVO
                                    //BLOQUE DE ARCHIVO AL CUAL VA APUNTAR
                       //                 imprimirln("ENTRO A UNO QUE NO APUNTA A NADA, SE ACTUALIZA INODO Y BLOQUE_ARCHISO");
                                        if(ARCHIVO_GLOBAL==NULL){
                                            ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_archivoExtra.c_str(),"rb+");
                                        }else{
                                            imprimirln("888888888888888888888888 ERROR EL ARCHIVO SE ENCUENTRA ABIERTO 888888888888888888888");
                                        }
                                        bool BLOQUE_COLOCADO=false;
                                        char verificar;
                                        int IDENTIFICADO_BLOQUE=0;
                                        for(int u=POSICION_INICIO_BITMPAARCHIVOS; (u<POSICION_FINAL_BITMAPARCHIVOS) && (BLOQUE_COLOCADO==false);u++){
                                            verificar=00;
                                            fseek(ARCHIVO_GLOBAL,u,SEEK_SET);
                                            fread(&verificar,1,1,ARCHIVO_GLOBAL);

                                            if(verificar==48){
                                                char respuesta=49;
                                                fseek(ARCHIVO_GLOBAL,u,SEEK_SET);
                                                fwrite(&respuesta,1,1,ARCHIVO_GLOBAL);
                                                fflush(ARCHIVO_GLOBAL);
                                                fclose(ARCHIVO_GLOBAL);
                                                ARCHIVO_GLOBAL=NULL;
                                                BLOQUE_COLOCADO=true;

                                                int POSICION_GUARDAR_BLOQUEARCHIVO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(2*DATOS_PIVOTE[0].valor_N*sizeof(B_Archivos))+(IDENTIFICADO_BLOQUE*sizeof(B_Archivos));
                                                GUARDAR_BLOQUE_ARCHIVOS(DATOS_PIVOTE[0].path_disco,POSICION_GUARDAR_BLOQUEARCHIVO,ARCHIVO);

                                                INODO.i_block[i]=IDENTIFICADO_BLOQUE;
                                                int POSICION_GUARDAR_INODOACTUALIZADO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(I_INODO*sizeof(Tabla_Inodos));
                                                GUARDAR_INODO_EN_DISCO(INODO,POSICION_GUARDAR_INODOACTUALIZADO,DATOS_PIVOTE[0].path_disco);
                                                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                                            }
                                            IDENTIFICADO_BLOQUE++;
                                        }
                                        if(ARCHIVO_GLOBAL!=NULL){
                                            fflush(ARCHIVO_GLOBAL);
                                            fclose(ARCHIVO_GLOBAL);
                                            ARCHIVO_GLOBAL=NULL;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    imprimirln("=== ERROR: NO SE PUDO RECUPERAR EL INODO (M, ESCRIBR, TXT ARCDISCO) ===");
                }
    }else{
        imprimirln("=== ERROR: SE EXCEDIO DE LOS LIMITES DE LA RUTA ===");
    }
    return encontrada;
}

void INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(vector<string> RUTA,int I_BLOQUE, int P_RUTA,int TIPO_APUNTADOR,int NIVEL_APUNTADOR){

    struct B_Apuntadores INDIRECTO;
    INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].inicio_particion,I_BLOQUE,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    NIVEL_APUNTADOR--;
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR==0)){
        //AQUI EL CODIGO QUE AGREGA LOS BLOQUES DE ARCHIVOS AL APUNTADOR INDIRECTO
        for(int i=0; (i<13) && (DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size()!=0);i++){

            struct B_Archivos ARCHIVO;
            strcpy(ARCHIVO.b_contentA,DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0].c_str());

            int POSICION_INICIO=2*(DATOS_PIVOTE[0].valor_N);
            int POSICION_FINAL=3*(DATOS_PIVOTE[0].valor_N);

            if(INDIRECTO.b_pointers[i]!=-1){//SE VUELVE GUARDAR CON EL NUEVO BLOQUE
                int POSICION_GUARDAR_ARHCIVO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(2*DATOS_PIVOTE[0].valor_N*sizeof(B_Archivos))+(INDIRECTO.b_pointers[i]*sizeof(B_Archivos));
                GUARDAR_BLOQUE_ARCHIVOS(DATOS_PIVOTE[0].path_disco,POSICION_GUARDAR_ARHCIVO,ARCHIVO);
                DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
            }else{//SE GUARDA POR PRIMERA VEZ EL BLOQUE
                bool bloqueguardado=false;
                //FILE *arc=fopen(DATOS_PIVOTE[0].path_archivoExtra.c_str(),"rb+");
                if(ARCHIVO_GLOBAL==NULL){
                    ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_archivoExtra.c_str(),"rb+");
                }else{
                    imprimirln("66666666666666666666666666666666 ERROR ARCHIVO ABIERTO 6666666666666666666666666666");
                }
                int CONTADOR_ID=0;
                char verificar;
                for(int k=POSICION_INICIO; (k<POSICION_FINAL) && (bloqueguardado==false); k++){
                    verificar=00;
                    //fseek(arc,k,SEEK_SET);
                    //fread(&verificar,1,1,arc);
                    fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                    fread(&verificar,1,1,ARCHIVO_GLOBAL);
                    if(verificar==48){
                        char respuesta=49;
                        /*fseek(arc,k,SEEK_SET);
                        fwrite(&respuesta,1,1,arc);
                        fflush(arc);
                        fclose(arc);*/
                        fseek(ARCHIVO_GLOBAL,k,SEEK_SET);
                        fwrite(&respuesta,1,1,ARCHIVO_GLOBAL);
                        fflush(ARCHIVO_GLOBAL);
                        fclose(ARCHIVO_GLOBAL);
                        ARCHIVO_GLOBAL=NULL;
                        bloqueguardado=true;
                        //PRIMERO GUARDAMOS EL BLOQUE ARCHIVO
                        int POSICION_GUARDAR_ARCHIVO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(2*DATOS_PIVOTE[0].valor_N*sizeof(B_Archivos))+(CONTADOR_ID*sizeof(B_Archivos));
                        GUARDAR_BLOQUE_ARCHIVOS(DATOS_PIVOTE[0].path_disco,POSICION_GUARDAR_ARCHIVO,ARCHIVO);
                        //LUEGO GUARDAMOS EL BLOQUE INDIRECTO ACTUALIZADO
                        INDIRECTO.b_pointers[i]=CONTADOR_ID;
                        int POSICION_GUARDAR_INDIRECTO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(DATOS_PIVOTE[0].valor_N*sizeof(B_Apuntadores))+(I_BLOQUE*sizeof(B_Apuntadores));
                        GUARDAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].path_disco,POSICION_GUARDAR_INDIRECTO,INDIRECTO);
                        DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR=ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[0]);
                    }
                    CONTADOR_ID++;
                }
                /*fflush(arc);
                fclose(arc);*/
                if(ARCHIVO_GLOBAL!=NULL){
                    fflush(ARCHIVO_GLOBAL);
                    fclose(ARCHIVO_GLOBAL);
                    ARCHIVO_GLOBAL=NULL;
                }
            }
        }

    }else
    if((TIPO_APUNTADOR==14) && (NIVEL_APUNTADOR==1)){
        if(INDIRECTO.b_pointers[13]!=-1){
            INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,1);
        }else{
            INDIRECTO.b_pointers[13]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
            int POSICION_GUARDAR_INDIRECTO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(DATOS_PIVOTE[0].valor_N*sizeof(B_Apuntadores))+(I_BLOQUE*sizeof(B_Apuntadores));
            GUARDAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].path_disco,POSICION_GUARDAR_INDIRECTO,INDIRECTO);
            INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,1);
        }
    }else
    if((TIPO_APUNTADOR==15) && (NIVEL_APUNTADOR==2)){
        if(INDIRECTO.b_pointers[13]!=-1){
            INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,2);
        }else{
            INDIRECTO.b_pointers[13]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
            int POSICION_GUARDAR_INDIRECTO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(DATOS_PIVOTE[0].valor_N*sizeof(B_Apuntadores))+(I_BLOQUE*sizeof(B_Apuntadores));
            GUARDAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].path_disco,POSICION_GUARDAR_INDIRECTO,INDIRECTO);
            INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,2);
        }
    }else
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR!=0)){
        if(INDIRECTO.b_pointers[13]!=-1){
            INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,1);
        }else{
            INDIRECTO.b_pointers[13]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
            int POSICION_GUARDAR_INDIRECTO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(DATOS_PIVOTE[0].valor_N*sizeof(B_Apuntadores))+(I_BLOQUE*sizeof(B_Apuntadores));
            GUARDAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].path_disco,POSICION_GUARDAR_INDIRECTO,INDIRECTO);
            INDIRECTO_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,1);
        }
    }

}


bool INDIRECTO_CARPETAS_ESCRIBIR_TEXT_EN_ARCHIVODISCO(vector<string> RUTA,int I_BLOQUE,int P_RUTA,int TIPO_APUNTADOR,int NIVEL_APUNTADOR){
 //   imprimirln("entro a metodo void indirecto text archivo disco");
    struct B_Apuntadores INDIRECTO;
    bool encontrado=false;
    INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].inicio_particion,I_BLOQUE,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    NIVEL_APUNTADOR--;
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR==0)){
        for(int i=0; (i<13) && (encontrado==false); i++){
            if(INDIRECTO.b_pointers[i]!=-1){
                struct B_Carpetas CARPETA;
                CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,INDIRECTO.b_pointers[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                for(int k=0; (k<4) && (encontrado==false); k++){
                    if(CARPETA.b_content[k].b_inodo!=-1){
                        if(strcmp(CARPETA.b_content[k].b_name,RUTA[P_RUTA].c_str())==0){
                            encontrado=true;
                            if(ES_ARCHIVO_ARCHIVODISCO(CARPETA.b_content[k].b_name)){
                                ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,CARPETA.b_content[k].b_inodo,P_RUTA);
                            }else{
                                ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,CARPETA.b_content[k].b_inodo,(P_RUTA+1));
                            }
                        }
                    }
                }
            }
        }
    }else
    if((TIPO_APUNTADOR==14) && (NIVEL_APUNTADOR==1)){
        if(INDIRECTO.b_pointers[13]!=-1){
            encontrado=INDIRECTO_CARPETAS_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,1);
        }
    }else
    if((TIPO_APUNTADOR==15) && (NIVEL_APUNTADOR==2)){
        if(INDIRECTO.b_pointers[13]!=-1){
            encontrado=INDIRECTO_CARPETAS_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,2);
        }
    }else
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR!=0)){
        if(INDIRECTO.b_pointers[13]!=-1){
            encontrado=INDIRECTO_CARPETAS_ESCRIBIR_TEXT_EN_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,1);
        }
    }
    return encontrado;
}


void CREAR_ARCHIVO_ARCHIVODISCO(vector<string> RUTA,int I_INODO,int POSICION_RUTA){//TAMBIEN SE PUEDE PARA INCIAR LA CREACION DE CARPETAS
    struct Tabla_Inodos INODO;
 //   imprimirln("si entro....");

    INODO=RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,I_INODO,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    if(POSICION_RUTA<RUTA.size()){
        if(INODO.i_type==0){//SI ENTRA AQUI SIGNIFICA QUE ES UN INODO CARPETAS/ARCHIVOS
            if((PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==6) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==7) || (PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==2) ||(PERMISO_ESCRITURA_LECTURA(INODO.i_perm,INODO.i_uid,INODO.i_gid)==3)){//VALIDANDO LOS PERMISOS DE ESCRITURA
                //SI ENTRA AQUI SIGNIFICA QUE TIENE PERMISOS DE ESCRITURA
                if(EXISTE_CARPETA_ARCHIVO_EN_INODO(INODO,RUTA[POSICION_RUTA],DATOS_PIVOTE[0].inicio_particion,0,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco)){
                    //ESTE IF VALIDA SI EN ESE INODO CARPETA EXISTE LA CARPETA/ARCHIVO ESPECIFICADA
     //               imprimirln("pudo validar permisos y si existe el dato path");
                    bool CARPETAPADREA_ARCHIVO_ENCONTRADO=false;

                    for(int i=0; (i<16) && (CARPETAPADREA_ARCHIVO_ENCONTRADO==false); i++){
                        //COMO SE SUPONE QUE SI ENTRA AQUI SIGNIFICA QUE LA CARPETA/ARCHIVO SI EXISTE POR LO TANTO
                        //SOLO VERIFICAMOS QUE SEA =!-1 YA QUE JAMAS VA A USAR A ==-1 PORQUE SI NO, NO EXISTIRIA
                        if(INODO.i_block[i]!=-1){
                            if(i==13){
                                CARPETAPADREA_ARCHIVO_ENCONTRADO=EXISTENCIA_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INODO.i_block[i],POSICION_RUTA,13,1,I_INODO);
                            }else
                            if(i==14){
                                CARPETAPADREA_ARCHIVO_ENCONTRADO=EXISTENCIA_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INODO.i_block[i],POSICION_RUTA,14,2,I_INODO);
                            }else
                            if(i==15){
                                CARPETAPADREA_ARCHIVO_ENCONTRADO=EXISTENCIA_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INODO.i_block[i],POSICION_RUTA,15,3,I_INODO);
                            }else{
                                struct B_Carpetas CARPETA;
                                CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,INODO.i_block[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

                                for(int j=0; (j<4) && (CARPETAPADREA_ARCHIVO_ENCONTRADO==false); j++){
                                    if(CARPETA.b_content[j].b_inodo!=-1){
                                        if(strcmp(CARPETA.b_content[j].b_name,RUTA[POSICION_RUTA].c_str())==0){
                                            if(ES_ARCHIVO_ARCHIVODISCO(RUTA[POSICION_RUTA])){
                                                CARPETAPADREA_ARCHIVO_ENCONTRADO=true;
                                   //             imprimirln("ENCONTRO ARCHIVOOO........");
                                            }else{
                                             /*   CARPETAPADREA_ARCHIVO_ENCONTRADO=true;
                                                CREAR_ARCHIVO_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(POSICION_RUTA+1));
                                                imprimirln("encontro carpeta");*/

                                                ////COLOCAR AQUI CODIGO MODIFICADO PARA LAS CARPETAS
                                                if(POSICION_RUTA!=(RUTA.size()-1)){//SI ES DIFERENTES QUIERE DECIR QUE ES UNA CARPETA PADRE DE OTRA CARPETA/ARCHIVOA
                                                    CARPETAPADREA_ARCHIVO_ENCONTRADO=true;
                                 //                   imprimirln("ENCONTRO CARPETA PADRE, CONTINUARA BUSCQUEDA DE LA CARPETA O ARCHIVO BUSCADO");
                                                    CREAR_ARCHIVO_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(POSICION_RUTA+1));
                                                }else{//SI NO SIGNIFICA QUE ENCONTRAMOS LA CARPETA BUSCADA
                                                    CARPETAPADREA_ARCHIVO_ENCONTRADO=true;
                                //                    imprimirln("ENCONTRO LA CARPETA BUSCADA......");
                                                }
                                                /////------------------------------------------------
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }else if((DATOS_PIVOTE[0]._p_archivo!="") || (POSICION_RUTA==(RUTA.size()-1))){//SI ENTRA AQUI NO EXISTE CARPETA ESPECIFICADA
                    //PERO SI SE ENVIA EL PARAMETRO "P" PARA SU CREACION
          //          imprimirln("pudo validar permisos y no existe el dato path");
                    bool CARPETAPADRE_ARCHIVO_CREADO=false;
                    //RECORREMOS CADA APUNTADOR DEL INODO
                    for(int i=0; (i<16) && (CARPETAPADRE_ARCHIVO_CREADO==false);i++){
          //              imprimir("RECORRIENDO INODO DE NO EXISTE EL PATH: VALOR DEL INODO.I_BLOCK[I]: ");
          //              imprimirNumln(INODO.i_block[i]);
          //              imprimir("EL VALOR DE i EN EL RECORRIDO ES: ");
         //               imprimirNumln(i);
                        if(INODO.i_block[i]!=-1){//VERIFICAMOS SI EL APUNTADOR APUNTA A UN BLOQUE CARPETA
                            //VERIFICAMOS SI ES INDIRECTO ESTE INODO YA DEBE APUNTAR A UN BLOQUE DE INDIRECTOS
                            if(i==13){
                                CARPETAPADRE_ARCHIVO_CREADO=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INODO.i_block[i],POSICION_RUTA,13,1,I_INODO);
                            }else if(i==14){
                                CARPETAPADRE_ARCHIVO_CREADO=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INODO.i_block[i],POSICION_RUTA,14,2,I_INODO);
                            }else if(i==15){
                                CARPETAPADRE_ARCHIVO_CREADO=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INODO.i_block[i],POSICION_RUTA,15,3,I_INODO);
                            }else{//SI NO ES INDIRECTO INGRESAMOS AQUI
                                //SE RECUPERA EL BLOQUE CARPETA PARA RECORRERLO Y VERIFICAR SI TIENE UN ESPACIO DISPONIBLE
                                struct B_Carpetas CARPETA;
                                CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,INODO.i_block[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

                                for(int j=0; (j<4) && (CARPETAPADRE_ARCHIVO_CREADO==false);j++){
                                    if(CARPETA.b_content[j].b_inodo==-1){//SI ENCUENTRA EXACTAMENTE UN APUNTADOR QUE APUNTA A -1
                                    //YA SE ENCONTRO EL INODO Y EL APUNTADOR DEL BLOQUE CARPETA DONDE COLOCAR LA NUEVA CARPETA/ARCHIVO
                                    //AHORA SE DEBE VERIFICAR SI ES UNA CARPETA O ARCHIVO LO QUE SE QUIERE CREAR
                                        if(ES_ARCHIVO_ARCHIVODISCO(RUTA[POSICION_RUTA])){//SIGNIFICA QUE SI ES UN ARCHIVO
                              //              imprimirln("se encontro espacio en un bloque carpeta y entro a que es archivo el nombre");
                                            strcpy(CARPETA.b_content[j].b_name,RUTA[POSICION_RUTA].c_str());
                                            //LE PASA COMO PARAMETRO 1 YA QUE ES UN ARCHIVO Y POR LO TANTO
                                            //UNICAMENTE DEBE CREAR SU INODO Y NO ACTUALIZAR EL INODO A UN ACTUAL Y A
                                            //UN PADRE
                                            CARPETA.b_content[j].b_inodo=CREAR_INODO_Y_BLOQUECARPETA(I_INODO,1);
                                            CARPETAPADRE_ARCHIVO_CREADO=true;
                                            //CON ESTO TERMINA YA QUE AQUI SI CREO EL ARCHIVO PERO UNICAMENTE DE CUANTO SE TIENE UN APUNTADOR
                                            //DE INODO APUNTANDO A UN BLOQUE FALTA HAY QUE VER LA PARTE EN LA QUE SE CREA PERO CUANDO
                                            //EL APUNTADOR DE INODO ES -1 Y TAMBIEN CUANDO ESE APUNTADOR ES UN APUNTADOR INDIRECTO
                               //             imprimirln("AHORA SE VA A GUARDAR EL BLOQUECARPETA CON EL NOMBREARCHIVO ACTUALIZADO ...");
                               //             imprimirNumln(CARPETA.b_content[j].b_inodo);
                                            int POSICION_GUARDAR_BLOQUECARPETAACTUALIZADO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(INODO.i_block[i]*sizeof(B_Carpetas));
                                            //imprimirNumln(POSICION_GUARDAR_BLOQUECARPETAACTUALIZADO);
                                            GUARDAR_BLOQUE_CARPETAS(DATOS_PIVOTE[0].path_disco,POSICION_GUARDAR_BLOQUECARPETAACTUALIZADO,CARPETA);
                                            ACTUALIZACION_JOURNALING_A_Y_C_ARCHIVODISCO(RUTA,DATOS_PIVOTE[0]._p_carpeta,DATOS_PIVOTE[0]._p_archivo,DATOS_PIVOTE[0].path_journaling);
                                //            imprimirln("GUARDO EL NOMBREARCHIVO ACTUALIZADO...");
                                        }else{//SIGNIFICA QUE ES UNA CARPETA
                               //             imprimirln("se encontro espacio en un bloque carpeta");
                                            strcpy(CARPETA.b_content[j].b_name,RUTA[POSICION_RUTA].c_str());
                                            //COMO ES UNA CARPETA SE PASA COMO PARAMETRO EL VALOR 0, Y ESTE METODO SE ENCARGA
                                            //DE CREAR EL INODO CARPETA Y APUNTARAR EL APUNTADOR 0 A UN BLOQUE CARPETA DONDE TENDRA SU
                                            //ACTUAL Y SU PADRE, SE CREA Y SE GUARDAN (ACTUALIZACION AUTOMATICA)
                                            CARPETA.b_content[j].b_inodo=CREAR_INODO_Y_BLOQUECARPETA(I_INODO,0);
                                            //SE GUARDA EL BLOQUE CARPETA ACTUALIZADO CON LOS DATOS
                                            CARPETAPADRE_ARCHIVO_CREADO=true;
                            //                imprimirln("NOMBRE Y #INODO DE LA CARPETA, INODO QUE FUE CREADO");
                            //                cout<<CARPETA.b_content[1].b_name<<endl;
                            //                imprimirNumln(CARPETA.b_content[j].b_inodo);
                                            int POSICION_GUARDAR_BLOQUECARPETAACTUALIZADO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(INODO.i_block[i]*sizeof(B_Carpetas));
                                            GUARDAR_BLOQUE_CARPETAS(DATOS_PIVOTE[0].path_disco,POSICION_GUARDAR_BLOQUECARPETAACTUALIZADO,CARPETA);
                                            //UNA VEZ GUARDADO Y ACTUALIZADO EL BLOQUE CARPETA QUE TENIA ESPACIO SE VUELVE A LLAMAR RECURSIVAMENTE ESTE METODO
                                            //YA QUE LO QUE SE CREO FUE UNA CARPETA Y NO EL INODO DE ARCHIVO YA QUE NO FUE ARCHIVO LO CREADO
                          //                  CREAR_ARCHIVO_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(POSICION_RUTA+1));

                                            //-----------------------CODIGO AGREGADO PARA LAS CARPETAS---------------------
                                                if(POSICION_RUTA!=(RUTA.size()-1)){//SI ES DIFERENTE ES CARPEA PADRE DE ARCHIVO O CARPETA
                                                    CREAR_ARCHIVO_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(POSICION_RUTA+1));
                                                }else{//SI NO LO ES QUIERE DECIR QUE ES LA CARPETA QUE QUEREMOS CREAR
                                                    ACTUALIZACION_JOURNALING_A_Y_C_ARCHIVODISCO(RUTA,DATOS_PIVOTE[0]._p_carpeta,DATOS_PIVOTE[0]._p_archivo,DATOS_PIVOTE[0].path_journaling);
                              //                      imprimirln("CARPETA CREADA EXITOSAMENTE++++");
                                                }
                                            //-----------------------------------------------------------------------
                                        }
                                    }
                                }
                            }
                        }else{//SI NO APUNTA A UN BLOQUE CARPETA SIGNIFICA QUE EN ESTE APUNTADOR SE CREA LA CARPETA/ARCHIVO YA QUE
                            //APUNTA A -1
                            //HAY QUE VER CUANDO SON APUNTADORES INDIRECTOS LOS QUE APUNTAN A A -1
       //                     imprimirln("SE SUPONE QUE ENTRA AQUI APUTNA A -1");
                            if(i==13){
                                INODO.i_block[i]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
                                int POSICION_GUARDAR_INODO_ACTUALIZADO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(I_INODO*sizeof(Tabla_Inodos));
                                GUARDAR_INODO_EN_DISCO(INODO,POSICION_GUARDAR_INODO_ACTUALIZADO,DATOS_PIVOTE[0].path_disco);
                                CARPETAPADRE_ARCHIVO_CREADO=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INODO.i_block[i],POSICION_RUTA,13,1,I_INODO);
                            }else if(i==14){
                                INODO.i_block[i]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
                                int POSICION_GUARDAR_INODO_ACTUALIZADO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(I_INODO*sizeof(Tabla_Inodos));
                                GUARDAR_INODO_EN_DISCO(INODO,POSICION_GUARDAR_INODO_ACTUALIZADO,DATOS_PIVOTE[0].path_disco);
                                CARPETAPADRE_ARCHIVO_CREADO=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INODO.i_block[i],POSICION_RUTA,14,2,I_INODO);
                            }else if(i==15){
                                INODO.i_block[i]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
                                int POSICION_GUARDAR_INODO_ACTUALIZADO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(I_INODO*sizeof(Tabla_Inodos));
                                GUARDAR_INODO_EN_DISCO(INODO,POSICION_GUARDAR_INODO_ACTUALIZADO,DATOS_PIVOTE[0].path_disco);
                                CARPETAPADRE_ARCHIVO_CREADO=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INODO.i_block[i],POSICION_RUTA,15,3,I_INODO);
                            }else{//SIGNIFICA QUE ES UN APUNTADOR DIRECTO QUE APUNTA A -1
                            //SI ESTE APUNTADOR APUNTA A -1 LO UNICO QUE HAY QUE HACER ES CREAR UN BLOQUE CARPETA NUEVO
                            //QUE ESTE APUNTADOR OBTENGA EL VALOR DE ESE APUNTADOR DE CARPETA NUEVO Y GUARDAR EL INODO EN SU
                            //POSICION ACTUAL Y PASARLO COMO PARAMETRO DE  NUEVO A ESTE MISMO METODO PARA QUE LO ENCUENTRE EN LA
                            //SIGUIENTE VALIDACION Y SE ENCARGUE EL CODIGO DE ARRIBA DE CREAR SU INODO
                            CARPETAPADRE_ARCHIVO_CREADO=true;
                            //COMO LE PASO FALSE SIGNIFICA QUE NO ES UN BLOQUE PRINCIPAL, POR LO TANTO
                            //NO LE VA A ASIGNAR PADRE NI ACTUAL UNICAMENTE CREA Y GUARDA EL BLOQUE CARPETA
                            //CON TODOS SUS APUNTADODRES APUNTANDO A -1
                         //   try{
                            ////SE VA A COLOCAR CODIGO AQUI
                            INODO.i_block[i]=CREAR_BLOQUECARPETA(0,0,false);
                            //GUARDAMOS EL INODO ACTUALIZADO EN SU POSICION Y SE LO PASAMOS
                            //COMO PARAMETRO A ESTE MISMO METOD PARA QUE CREE EL INODO CARPETA/ARCHIVO
            //                imprimir("ESTE SERA EL VALOR DE NUEVO BLOQUE CARPETA QUE SE CREO...");
                     //       imprimirNumln(INODO.i_block[i]);
                            int POSICION_GUARDAR_INODO_ACTUALIZADO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(I_INODO*sizeof(Tabla_Inodos));
                            GUARDAR_INODO_EN_DISCO(INODO,POSICION_GUARDAR_INODO_ACTUALIZADO,DATOS_PIVOTE[0].path_disco);
                            //NO INTERESA SI ES ARCHIVO O CARPETA POR ESO NO SE ACTUALIZA POSICION RUTA
                            //LO QUE INTERES ES EN ESTE INODO RECUPERADO QUE SU APUNTADOR APUNTE A UN NUEVO BLOQUE CARPETAS
                            //EL CUAL FUE CREADO Y GUARDADO ANTERIORMENTE Y ASI PASARLE A ESTE MISMO METODO SUS MISMOS PARAMETRO
                            //PARA QUE RECUPERE DE NUEVO ESTE INODO Y AHI SI YA VERIFIQUE QUE TIENE UN BLOQUE CARPETA
                            //Y EN ESE YA CREE LA CARPETA/ARCHIVO CORRESPONDIENTE
            //                imprimirln("SE GUARDO BIEN AHORA SE VA A LLAMAR RECURSIVAMETNELJ");
                            CREAR_ARCHIVO_ARCHIVODISCO(RUTA,I_INODO,POSICION_RUTA);
            //                imprimirln("AL PARECER SI ENTRO");
                            ////SE VA A COLOCAR CODIGO AQUI
                     //       }catch(exception& e){
                       //         imprimirln("SE ENCONTRO CON EERROR PARA EL SIGUIENTE BLOQUE");
                     //       }

                            }
                        }
                    }
                }else{
                    imprimirln("=== ERROR: CARPETA PADRE NO EXISTENTE PARA CREAR EL ARCHIVO ===");
                }

            }else{
                imprimirln("=== ERROR: NO CUENTA CON PERMISOS DE ESCRITURA ===");
            }
        }else
        if(INODO.i_type==1){//SI ENTRA AQUI SIGNIFICA QUE ES UN INODO ARCHIVOS
            //SE SUPONE QUE NO DEBE ENTRAR AQUI YA QUE JAMAS VA A VALIDAR SI INGRESAMOS A UN INODO ARCHIVO
            //UNICAMENTE CREAMOS EL INODO ARCHIVO SI ES ARCHIVO
    //        imprimirln("entro a archivo");
        }else{//SI ENTRA AQUI SIGNIFICA QUE OCURRIO UN ERROR AL RECUPERAR EL INODO
            imprimirln("===ERROR AL RECUPERAR INODO DE CREACION ARCHIVODISCO ===");
        }
    }else{
        imprimirln("=== ERROR: EXCEDIO LIMITES DE DIRECCION/RUTA ===");
    }

}

//ESTOS METODOS CREAN EL INODO CARPETA Y SU PRIMERO BLOQUE CARPETA QUE TIENE SU PADRE Y ACTUAL
int CREAR_INODO_Y_BLOQUECARPETA(int INODOPADRE,int TIPO_INODO){
    srand(time(NULL));//la semila para la hora y fecha
    //CREAMOS LA ESTRUCTRUA INODO
    struct Tabla_Inodos NUEVO_INODO;
    //LE INGRESAMOS LOS DATOS A LA ESTRUCTURA INODO
    NUEVO_INODO.i_uid=DATOS_PIVOTE[0].u_id;
    NUEVO_INODO.i_gid=DATOS_PIVOTE[0].g_id;
    NUEVO_INODO.i_size=0;
    NUEVO_INODO.i_atime=time(NULL);
    NUEVO_INODO.i_ctime=time(NULL);
    NUEVO_INODO.i_mtime=time(NULL);
    NUEVO_INODO.i_type=TIPO_INODO;
    NUEVO_INODO.i_perm=664;
    for(int i=0; i<16; i++){
        NUEVO_INODO.i_block[i]=-1;
    }
 //   imprimirln("ESTA EN METODO CREAR INODO Y BLOQUE CARPETA CON DATOS COMO...");
 //   imprimirNumln(NUEVO_INODO.i_uid);
 //   cout<<ctime(&NUEVO_INODO.i_ctime)<<endl;
 //   cout<<NUEVO_INODO.i_block[9]<<endl;

    int INICIO_INODOS=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque);
    int FIN_INODOS=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+DATOS_PIVOTE[0].valor_N;
    int IDENTIFICADOR=0;
    bool INSERTADO=false;
    //FILE *archivo=fopen(DATOS_PIVOTE[0].path_disco.c_str(),"rb+");
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_disco.c_str(),"rb+");
    }else{
        imprimirln("666666666666666666666666666666 ERROR ARCHIVO AIBERTO 6666666666666666666666666");
    }
//    imprimirln("posicion a colocarse");
    imprimirNumln(INICIO_INODOS);
    char verificacion;
    for(int i=INICIO_INODOS; (i<FIN_INODOS) && (INSERTADO==false);i++){
        verificacion=00;
       /* fseek(archivo,i,SEEK_SET);
        fread(&verificacion,1,1,archivo);*/
        fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
        fread(&verificacion,1,1,ARCHIVO_GLOBAL);

        if(verificacion==48){
            char respuesta=49;
            //ACTUALIZAMOS EL BITMAP DE INODOS
            /*fseek(archivo,i,SEEK_SET);
            fwrite(&respuesta,1,1,archivo);
            fflush(archivo);
            fclose(archivo);*/
            fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
            fwrite(&respuesta,1,1,ARCHIVO_GLOBAL);
            fflush(ARCHIVO_GLOBAL);
            fclose(ARCHIVO_GLOBAL);
            ARCHIVO_GLOBAL=NULL;
            INSERTADO=true;
            //ACTUALIZAMMOS EL INODO PARA QUE SU APUNTADOR CERO APUNTA A !=-1 SI ES UN TIPO INODO 0
            //LO QUE SIGNIFICA QUE SERA UN INODO CARPETA
//            imprimirln("VA A VERIFICAR SI ES INODO DE ARCHIVO O CARPETA");
            if(TIPO_INODO==0){
//                imprimirln("VA A ENTRARA QUI UNICAMENTE SI ES INODO CARPETA");
                NUEVO_INODO.i_block[0]=CREAR_BLOQUECARPETA(IDENTIFICADOR,INODOPADRE,true);
            }

            //GUARDANDO EL INODO EN LA POSICION CORRESPONDIENTE
            int POSICION_GUARDAR=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(IDENTIFICADOR*sizeof(Tabla_Inodos));
  //          imprimirln(" GUARDANDO INODO DEL METODO CREAR INODOYBLOQUE CARPETA");
            GUARDAR_INODO_EN_DISCO(NUEVO_INODO,POSICION_GUARDAR,DATOS_PIVOTE[0].path_disco);

        }else{
            IDENTIFICADOR++;
        }
    }
    /*fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL!=NULL){
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }
    return IDENTIFICADOR;
}

int CREAR_BLOQUECARPETA(int INODOACTUAL,int INODOPADRE,bool BLOQUEPRINCIPAL){//ESTE METODO LO VA A SOLICITAR EL METODO CREAR_INODO_Y_BLOQUECARPETA
    struct B_Carpetas NUEVO_BLOQUECARPETA;
//    imprimirln("INODO PADRE Y EL ACTUAL");
//    imprimirNumln(INODOPADRE);
//    imprimirNumln(INODOACTUAL);
    for(int i=0; i<4; i++){
        NUEVO_BLOQUECARPETA.b_content[i].b_inodo=-1;
    }
    //SI ENTRA AQUI SIGNIFICA QUE ES BLOQUE PRINCIPAL Y VA A SER USO DEL PADRE Y DEL ACTUAL EN CASO CONTRARIO
    //SOLAMENTE CREA EL BLOQUE Y LO GUARDA CON LA NUEVA ACTUALIZACION DE QUE ESTA VACIO
    if(BLOQUEPRINCIPAL==true){
        //LLENANDO LOS DATOS DEL ACTUAL
        strcpy(NUEVO_BLOQUECARPETA.b_content[0].b_name,".");
        NUEVO_BLOQUECARPETA.b_content[0].b_inodo=INODOACTUAL;
        //LLENANDO LOS DATOS DEL PADRE
        strcpy(NUEVO_BLOQUECARPETA.b_content[1].b_name,"..");
        NUEVO_BLOQUECARPETA.b_content[1].b_inodo=INODOPADRE;
    }
    //BUSCANDO POSICION A GUARDARLO
    int INICIO_BITPMAPCARPETAS=0;
    int FIN_INICIO_BIPMAPCARPETAS=DATOS_PIVOTE[0].valor_N;
    int IDENTIFICADO_BLOQUE=0;
    bool INSERTADO=false;
   //FILE *archivo=fopen(DATOS_PIVOTE[0].path_archivoExtra.c_str(),"rb+");
   if(ARCHIVO_GLOBAL==NULL){
    ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_archivoExtra.c_str(),"rb+");
   }else{
    imprimirln("66666666666666666666666666 ARCHIVO ABIERTO ERROR 666666666666666666666666666666");
   }
//    imprimirln("YA CREO EL ARCHIVO AHORITA ENTRARA AL FOR DE BITMAP");
    char verificacion;
    for(int i=INICIO_BITPMAPCARPETAS;(i<FIN_INICIO_BIPMAPCARPETAS) && (INSERTADO==false); i++){
        verificacion=00;
        /*fseek(archivo,i,SEEK_SET);
        fread(&verificacion,1,1,archivo);*/
        fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
        fread(&verificacion,1,1,ARCHIVO_GLOBAL);

        if(verificacion==48){
  //          imprimirln("VERIFICO UNA POSICION ESCRIBIRA EN EL ARCHIVO Y LO CERRARA");
            char respuesta=49;
            //ACTUALIZANDO EL BITMAP DE BLOQUECARPETAS
            /*fseek(archivo,i,SEEK_SET);
            fwrite(&respuesta,1,1,archivo);
            fflush(archivo);
            fclose(archivo);*/
            fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
            fwrite(&respuesta,1,1,ARCHIVO_GLOBAL);
            fflush(ARCHIVO_GLOBAL);
            fclose(ARCHIVO_GLOBAL);
            ARCHIVO_GLOBAL=NULL;
   //         imprimirln("ACABA DE CERRAR EL ARCHIVO");
            INSERTADO=true;
  //          imprimirln("posicion id a colocar y demas datos");
  //          imprimirNumln(IDENTIFICADO_BLOQUE);
            cout<<NUEVO_BLOQUECARPETA.b_content[1].b_name<<endl;
            cout<<NUEVO_BLOQUECARPETA.b_content[1].b_inodo<<endl;
            //GUARDANDO EL BLOQUE CARPETA EN SU POSICION CORRESPONDIENTE
            int POSICION_GUARDAR_CARPETA=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(IDENTIFICADO_BLOQUE*sizeof(B_Carpetas));
     //       imprimirln("GUARDANDO EL BLOQUE CARPETA DEL METODO CREAR_BLOQUECARPETA");
            GUARDAR_BLOQUE_CARPETAS(DATOS_PIVOTE[0].path_disco,POSICION_GUARDAR_CARPETA,NUEVO_BLOQUECARPETA);
     //       imprimirln("GUARDO EL NUEVO BLOQUE CARPETA DEL METODO CREAR CARPETA");
        }else{
            IDENTIFICADO_BLOQUE++;
        }

    }
   /* fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL!=NULL){
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }

    return IDENTIFICADO_BLOQUE;
}

bool CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(vector<string> RUTA,int I_BLOQUE,int P_RUTA,int TIPO_APUNTADOR,int NIVEL_APUNTADOR,int INODO_PADRE){
 //   imprimirln("ENTRO A METODO CREAR APUNTADOR INDIRECTO ARCHIVODISCO");
    struct B_Apuntadores INDIRECTO;
    INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].inicio_particion,I_BLOQUE,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
    bool CARPETA_ARCHIVO_CREADA=false;

    NIVEL_APUNTADOR--;
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR==0)){
  //      imprimirln("ECONTRO EL APUNTADOR 13,0");
        //ESTE APUNTADOR INDIRECTO ES EL QUE TENDRA LOS BLOQUES DE CARPETAS DE ARCHIVOS/CARPETAS
        //Y POR SUPUESTO TAMBIEN COMO EL METODO DE CREAR_BLOQUECARPETA SE TENDRA PARA CADA BLOQUECARPETA SU APUNTADOR
        //A SU INODO CORRESPONDIENTE YA SEA DE ARCHIVOS O INODO DE CARPETAS
        for(int i=0; (i<13) && (CARPETA_ARCHIVO_CREADA==false); i++){
      //      imprimir("RECORRIENDO INDIRECTO DE METODO ARCHIVODISCO CON #APUNTADOR E I: ");
      //      imprimirNumln(INDIRECTO.b_pointers[i]);
      //      imprimirNumln(i);
            if(INDIRECTO.b_pointers[i]!=-1){//SI ENTRA AQUI SIGNIFICA QUE EL APUNTADOR DE INDIRECTO YA APUNTA
            //A UN BLOQUE CARPETA (YA QUE SON LOS INDIRECTOS DE CARPETA) POR LO TANTO RECUPERABMOS EL BLOQUE Y VALIDAMOS SI TIENE
            //ESPACIO, EN ESTE CASO YA NO VERIFICAMOS APUNTADORES INDIRECTOS YA QUE ESTE INDIRECTO SOLO HACE USO DE LOS APUNTADORES
            //DIRECTOS
             //   imprimirln("VA A RECUPERAR BLOQUECARPETA DE APUNTADOR INDIRECTO");
                struct B_Carpetas CARPETA;
                CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,INDIRECTO.b_pointers[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

                for(int j=0; (j<4) && (CARPETA_ARCHIVO_CREADA==false); j++){
                    if(CARPETA.b_content[j].b_inodo==-1){//SIGNIFICA QUE EN BLOQUECARPETA TIENE UN APUNTADOR QUE NO APUNTA A NADA ESO QUIERE
                    //DECIR QUE PODEMOS COLOCAR AQUI NUESTOR NUEVO ARCHIVO/CARPETA
                        if(ES_ARCHIVO_ARCHIVODISCO(RUTA[P_RUTA])){//VA A ENTRAR AQUI SI ES UN ARHCIVO EL QUE SE VA A CREAR
                        //    imprimirln("ENCONTRO UNA POSICION PARA ARCHIVO INDIRECOT");
                            strcpy(CARPETA.b_content[j].b_name,RUTA[P_RUTA].c_str());
                            CARPETA.b_content[j].b_inodo=CREAR_INODO_Y_BLOQUECARPETA(INODO_PADRE,1);
                            int POSICION_GUARDARBLOQUECARPETA_ACTUALIZADO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(INDIRECTO.b_pointers[i]*sizeof(B_Carpetas));
                         //   imprimirln("GUARDARA EL BLOQUE CARPETA DE INDIRECTO");
                            GUARDAR_BLOQUE_CARPETAS(DATOS_PIVOTE[0].path_disco,POSICION_GUARDARBLOQUECARPETA_ACTUALIZADO,CARPETA);
                            ACTUALIZACION_JOURNALING_A_Y_C_ARCHIVODISCO(RUTA,DATOS_PIVOTE[0]._p_carpeta,DATOS_PIVOTE[0]._p_archivo,DATOS_PIVOTE[0].path_journaling);
                            CARPETA_ARCHIVO_CREADA=true;
                        }else{//EN CASO CONTRARIO ES UNA CARPETA Y COMO SE VA A COLOCAR EN BLOQUECARPETA EL
                        //NOMBRE DE LA NUEVA CARPETA A LA QUE VA APUNTAR EL INODO AL QUE APUNTA ESA CARPETA TIENE PADRE Y ACTUAL
                       //    imprimirln("ENCONTRO UNA POSICON PARA CARPETA INDIRECTO");
                            strcpy(CARPETA.b_content[j].b_name,RUTA[P_RUTA].c_str());
                            CARPETA.b_content[j].b_inodo=CREAR_INODO_Y_BLOQUECARPETA(INODO_PADRE,0);
                            int POSICION_GUARDARBLOQUECARPETA=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(INDIRECTO.b_pointers[i]*sizeof(B_Carpetas));
                            //imprimirln("ESTA IN APUNTADORES INDIRECTOS DONDE SE GUARDO COMO CARPETA Y LLAMARA AL METOD ARCHIVO_ARCHIVODISCO");
                            GUARDAR_BLOQUE_CARPETAS(DATOS_PIVOTE[0].path_disco,POSICION_GUARDARBLOQUECARPETA,CARPETA);
                            CARPETA_ARCHIVO_CREADA=true;
                 //           CREAR_ARCHIVO_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(P_RUTA+1));

                            //---------------------------------------CODIGO AGREGADO PARA LAS CARPETAS
                            if(P_RUTA!=(RUTA.size()-1)){
                                CREAR_ARCHIVO_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(P_RUTA+1));
                            }else{
                                ACTUALIZACION_JOURNALING_A_Y_C_ARCHIVODISCO(RUTA,DATOS_PIVOTE[0]._p_carpeta,DATOS_PIVOTE[0]._p_archivo,DATOS_PIVOTE[0].path_journaling);
                            //    imprimirln("CARPETA CREADA EN APUNTADOR INDIRECTO -------");
                            }
                            //-----------------------------------------------------------------------------
                        }
                    }
                }
            }else{//SI LLEGA AQUI SIGNIFICA QUE EL APUNTADOR INDIRECTO NO TIENE UN BLOQUE CARPETA AL CUAL APUNTAR
            //ESO QUIERE DECIR QUE TENEMOS QUE CREAR EL BLOUQE CARPETA Y EN ESE BLOQUE CARPETA COLOCAR LA CARPETA/ARCHIVO
            //QUE QUEREMOS CREAR, COMO ES APUNTADOR INDIRECTO ESTE NO TENDRA PADRE NI ACTUAL YA QUE NO ES INODO
             //   imprimirln("ESTE APUNTADOR INDIRECTO APUNTA A -1");
                INDIRECTO.b_pointers[i]=CREAR_BLOQUECARPETA(0,0,false);
                int POSICION_GUARDAR_INDIRECTO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(DATOS_PIVOTE[0].valor_N*sizeof(B_Apuntadores))+(I_BLOQUE*sizeof(B_Apuntadores));
                //SE GUARDA CON SUS DATOS ACTUALIZADOS
                GUARDAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].path_disco,POSICION_GUARDAR_INDIRECTO,INDIRECTO);
                //SE LLAMA A ESTE MISMO METODO CON SUS MISMOS PARAMETRO YA QUE AHORA SI TIENE UN BLOQUE CARPETA
                //CON APUNTADORES HACIA -1
                //CARPETA_ARCHIVO_CREADA=true;
             //   imprimirln("LE GUARDO TODO LOS DATOS PARA APUNTADRO INDIRECTO AHORA SE LLLAMRA RECURSIVAMENTE");
                CARPETA_ARCHIVO_CREADA=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,I_BLOQUE,P_RUTA,13,1,INODO_PADRE);
             //   imprimirln("REGRESO DE LLAMARASE RECURSIVAMENTE DE INDIDRECTO BLQOUE");
            }
        }

    }else
    if((TIPO_APUNTADOR==14) && (NIVEL_APUNTADOR==1)){
        if(INDIRECTO.b_pointers[13]!=-1){
            CARPETA_ARCHIVO_CREADA=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,1,INODO_PADRE);
        }else{
            //ESTO SE REALIZARA CON TODOS, SE ACTUALIZA EL INDIRECTO EN SU POSICION 13
            INDIRECTO.b_pointers[13]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
            //SE GUARDA EL INDIRECTO ACTUALIZADO Y SE MANDA RECURSIVAMENTE PARA RECUPERE EL INODO CREADO
            int POSICION_GUARDARBLOQUE_ACTUALIZADO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(DATOS_PIVOTE[0].valor_N*sizeof(B_Apuntadores))+(I_BLOQUE*sizeof(B_Apuntadores));
            GUARDAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].path_disco,POSICION_GUARDARBLOQUE_ACTUALIZADO,INDIRECTO);
            //SE LLAMA RECURSIVAMENTE A ESTE METODO PARA RECUPERAR EL NUEVO APUNTADOR INDIRECTO QUE YA FUE CREADO
            //Y NO SE MANDA A RECUPERAR ESTE MISMO BLOQUE INDIRECTO GUARDADO YA QUE NOS LLEVARIA A LO MISMO
            //VERIFICARIA QUE EN 13 ES !=-1 Y SE LLAMARIA NUEVAMENTE, ES GASTO DE RECURSOS EN MEMORIA
            CARPETA_ARCHIVO_CREADA=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,1,INODO_PADRE);
        }
    }else
    if((TIPO_APUNTADOR==15) && (NIVEL_APUNTADOR==2)){
        if(INDIRECTO.b_pointers[13]!=-1){
            CARPETA_ARCHIVO_CREADA=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,2,INODO_PADRE);
        }else{
            INDIRECTO.b_pointers[13]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
            int POSICION_GUARDARBLOQUE_ACTUALIZADO=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(DATOS_PIVOTE[0].valor_N*sizeof(B_Apuntadores))+(I_BLOQUE*sizeof(B_Apuntadores));
            GUARDAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].path_disco,POSICION_GUARDARBLOQUE_ACTUALIZADO,INDIRECTO);

            CARPETA_ARCHIVO_CREADA=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,2,INODO_PADRE);
        }
    }else
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR!=0)){
        if(INDIRECTO.b_pointers[13]!=-1){
            CARPETA_ARCHIVO_CREADA=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,1,INODO_PADRE);
        }else{
            INDIRECTO.b_pointers[13]=ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO();
            int POSICION_GUARDARBLOQUE_ACTUALIZADOS=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(DATOS_PIVOTE[0].valor_N*sizeof(B_Apuntadores))+(I_BLOQUE*sizeof(B_Apuntadores));
            GUARDAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].path_disco,POSICION_GUARDARBLOQUE_ACTUALIZADOS,INDIRECTO);

            CARPETA_ARCHIVO_CREADA=CREAR_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,INDIRECTO.b_pointers[13],P_RUTA,13,1,INODO_PADRE);
        }
    }

    return CARPETA_ARCHIVO_CREADA;
}

bool EXISTENCIA_APUNTADOR_INDIRECTO_ARCHIVODISCO(vector<string> RUTA,int I_BLOQUE, int P_RUTA, int TIPO_APUNTADOR,int NIVEL_APUNTADOR,int INODO_PADRE){
    bool EXISTE=false;

    struct B_Apuntadores APUNTADOR;
    APUNTADOR=RECUPERAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].inicio_particion,I_BLOQUE,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    NIVEL_APUNTADOR--;
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR==0)){
        for(int i=0; (i<13) && (EXISTE==false);i++){
            if(APUNTADOR.b_pointers[i]!=-1){
                struct B_Carpetas CARPETA;
                CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,APUNTADOR.b_pointers[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

                for(int j=0; (j<4) && (EXISTE==false);j++){
                    if(CARPETA.b_content[j].b_inodo!=-1){
                        if(strcmp(CARPETA.b_content[j].b_name,RUTA[P_RUTA].c_str())==0){
                            if(ES_ARCHIVO_ARCHIVODISCO(RUTA[P_RUTA])){
                             //   imprimirln("ENCONTRO ARCHIVOO.... EN BLOQUE CARPETA DE UN APUNTADOR INDIRECTO");
                                EXISTE=true;
                            }else{
                                EXISTE=true;
                          //      CREAR_ARCHIVO_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(P_RUTA+1));

                                //----------------------CODIGO PARA LAS CARPETAS (CODIGO AGREGADO)------
                                    if(P_RUTA!=(RUTA.size()-1)){
                                        CREAR_ARCHIVO_ARCHIVODISCO(RUTA,CARPETA.b_content[j].b_inodo,(P_RUTA+1));
                                    }else{
                              //          imprimirln("ENCONTRO CARPETA.... EN BLOQUE CARPETA APUNTADOR INDIRECTO");
                                    }
                                //------------------------------------------------------------------------
                            }
                        }
                    }
                }
            }
        }
    }else
    if((TIPO_APUNTADOR==14) && (NIVEL_APUNTADOR==1)){
        if(APUNTADOR.b_pointers[13]!=-1){
            EXISTE=EXISTENCIA_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,APUNTADOR.b_pointers[13],P_RUTA,13,1,INODO_PADRE);
        }
    }else
    if((TIPO_APUNTADOR==15) && (NIVEL_APUNTADOR==2)){
        if(APUNTADOR.b_pointers[13]!=-1){
            EXISTE=EXISTENCIA_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,APUNTADOR.b_pointers[13],P_RUTA,13,2,INODO_PADRE);
        }
    }else
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR!=0)){
        if(APUNTADOR.b_pointers[13]!=-1){
            EXISTE=EXISTENCIA_APUNTADOR_INDIRECTO_ARCHIVODISCO(RUTA,APUNTADOR.b_pointers[13],P_RUTA,13,1,INODO_PADRE);
        }
    }

    return EXISTE;
}

int PERMISO_ESCRITURA_LECTURA(int PERMISOS,int I_UID,int I_GID){
    string permisosTexto=to_string(PERMISOS);
    int permisosUsuario=OBTENER_PERMISO_NUMERICO(permisosTexto[0]);
    int permisosGrupo=OBTENER_PERMISO_NUMERICO(permisosTexto[1]);
    int permisosOtro=OBTENER_PERMISO_NUMERICO(permisosTexto[2]);
    int PERMISOACCESO=0;

    if(DATOS_PIVOTE[0].u_id==1){//PRIMERO VALIDAMOS QUE SI ES EL USUARIO ROOT
        PERMISOACCESO=7;
    //    imprimirln("usuario root");
    }else
    if(DATOS_PIVOTE[0].u_id==I_UID){//VALIDAMOS SI ES EL USUARIO PROPIETARIO EL QUE SOLICITA PERMISO
        PERMISOACCESO=permisosUsuario;
    //    imprimirln("es el usuario");
    }else
    if(DATOS_PIVOTE[0].g_id==I_GID){
        PERMISOACCESO=permisosGrupo;
    //    imprimirln("pertenece al grupo");
    }else{
        PERMISOACCESO=permisosOtro;
    //    imprimirln("es otro usuario");
    }
    return PERMISOACCESO;
}

int OBTENER_PERMISO_NUMERICO(char texto){
    int respuesta=0;

    switch(texto){
        case 48://0
            respuesta=0;
        break;
        case 49://1
            respuesta=1;
        break;
        case 50://2
            respuesta=2;
        break;
        case 51://3
            respuesta=3;
        break;
        case 52://4
            respuesta=4;
        break;
        case 53://5
            respuesta=5;
        break;
        case 54://6
            respuesta=6;
        break;
        case 55://7
            respuesta=7;
        break;
    }
    return respuesta;
};

//-----------------------------------------------METODOS NUEVOS PARA EL USO DEL PROYECTO 2 ----------------------------------------
void CONECTAR_ENVIAR_MENSAJE_SERVIDOR(string texto_enviar){
////
    int sock=socket(AF_INET,SOCK_STREAM,0);

    if(sock==-1){
        imprimirln("===ERROR AL OBTENER EL SOCKET ===");
    }else{
        int PORT=9999;
        string ipDir="192.168.0.15";
        sockaddr_in hint;
        hint.sin_family=AF_INET;
        hint.sin_port=htons(PORT);
        inet_pton(AF_INET,ipDir.c_str(),&hint.sin_addr);

        int connectando=connect(sock,(sockaddr*)&hint,sizeof(hint));

        if(connectando==-1){
            imprimirln("=== ERROR AL CONECTAR CON EL SERVIDOR ===");
        }else{
            //se supone que es ciclo pero nel
            //string saludo="SELECT * from PRODUCETOS WHERE (1,'varios',17/09/1.088)";

            int sendRes=send(sock,texto_enviar.c_str(),texto_enviar.size()+1,0);

            if(sendRes==-1){
                imprimirln("=== ERROR: NO SE ENVIO EL MENSAJE AL SERVIDOR ===");
            }else{
                imprimirln("++++ QUERY ENVIADO A JAVA ++++");
            }

        }
    }

    close(sock);
}

void INICIO_RECORRIDO_SYN_INODO(struct PARAMETROS_SYNCRONICE_PRIMARIA parametros,int I_inodo,string padre, int Identificador){
    struct Tabla_Inodos INODO;
    INODO=RECUPERAR_INODO(parametros.inicio_particion,I_inodo,parametros.valor_n,parametros.path_disco);

    if(INODO.i_type==0){//ES INODO DE CARPETAS
        //imprimirln("inodo de carpetas");
        //RECORREMOS CADA APUNTADOR DE BLOQUE CARPETAS DEL INODO
        int identificador_hijos=Identificador;
        for(int i=0; i<16;i++){
            if(INODO.i_block[i]!=-1){
                if(i==13){
                    RECORRIDO_SYN_INDIRECTOS_CA(parametros,INODO.i_block[i],0,1,13,padre,Identificador);
                }else
                if(i==14){
                    RECORRIDO_SYN_INDIRECTOS_CA(parametros,INODO.i_block[i],0,2,14,padre,Identificador);
                }else
                if(i==15){
                    RECORRIDO_SYN_INDIRECTOS_CA(parametros,INODO.i_block[i],0,3,15,padre,Identificador);
                }else{
                    struct B_Carpetas CARPETA;
                    CARPETA=RECUPERAR_BLOQUE_CARPETA(parametros.inicio_particion,INODO.i_block[i],parametros.valor_n,parametros.path_disco);
                    for(int j=0; j<4; j++){
                        if(CARPETA.b_content[j].b_inodo!=-1){
                            if(i==0){
                                if(j>1){
                                    identificador_hijos++;
                                   imprimirln(padre+".."+CARPETA.b_content[j].b_name);
                                    imprimirln(to_string(Identificador)+"..."+to_string(identificador_hijos));
                                    CONECTAR_ENVIAR_MENSAJE_SERVIDOR("{CALL INSERCION_AC('"+padre+"','"+RECUPERAR_TEXT_CHAR_STRING(CARPETA.b_content[j].b_name)+"','NULL ',"+to_string(Identificador)+","+to_string(identificador_hijos)+",'"+parametros.nombre_disco+"','"+parametros.nombre_particion+"',"+to_string(INODO.i_type)+","+to_string(INODO.i_perm)+")}");
                                  //  CONECTAR_ENVIAR_MENSAJE_SERVIDOR(padre+".."+CARPETA.b_content[j].b_name);
                                   INICIO_RECORRIDO_SYN_INODO(parametros,CARPETA.b_content[j].b_inodo,CARPETA.b_content[j].b_name,identificador_hijos);
                                }
                            }else{
                                identificador_hijos++;
                                imprimirln(padre+".."+CARPETA.b_content[j].b_name);
                                imprimirln(to_string(Identificador)+"..."+to_string(identificador_hijos));
                               // CONECTAR_ENVIAR_MENSAJE_SERVIDOR(padre+".."+CARPETA.b_content[j].b_name);
                                CONECTAR_ENVIAR_MENSAJE_SERVIDOR("{CALL INSERCION_AC('"+padre+"','"+RECUPERAR_TEXT_CHAR_STRING(CARPETA.b_content[j].b_name)+"','NULL ',"+to_string(Identificador)+","+to_string(identificador_hijos)+",'"+parametros.nombre_disco+"','"+parametros.nombre_particion+"',"+to_string(INODO.i_type)+","+to_string(INODO.i_perm)+")}");
                                INICIO_RECORRIDO_SYN_INODO(parametros,CARPETA.b_content[j].b_inodo,CARPETA.b_content[j].b_name,identificador_hijos);
                            }
                        }
                    }
                }
            }
        }
    }else
    if(INODO.i_type==1){//ES INODO DE ARCHIVOS
       for(int i=0; i<16; i++){
            if(INODO.i_block[i]!=-1){
                if(i==13){
                    RECORRIDO_SYN_INDIRECTOS_CA(parametros,INODO.i_block[i],1,1,13,padre,Identificador);
                }else
                if(i==14){
                    RECORRIDO_SYN_INDIRECTOS_CA(parametros,INODO.i_block[i],1,2,14,padre,Identificador);
                }else
                if(i==15){
                    RECORRIDO_SYN_INDIRECTOS_CA(parametros,INODO.i_block[i],1,3,15,padre,Identificador);
                }else{
                    struct B_Archivos ARCH;
                    ARCH=RECUPERAR_BLOQUE_ARCHIVO(parametros.inicio_particion,INODO.i_block[i],parametros.valor_n,parametros.path_disco);
                    imprimirln(padre+".."+ARCH.b_contentA);
                    CONECTAR_ENVIAR_MENSAJE_SERVIDOR("{CALL INSERCION_AC('"+padre+"','NULL','"+RECPERAR_TEXTO_ARCHIVO_SIN_SIMBOLOS(RECUPERAR_TEXT_CHAR_STRING(ARCH.b_contentA))+"',"+to_string(Identificador)+","+"0"+",'"+parametros.nombre_disco+"','"+parametros.nombre_particion+"',"+"1"+","+"664"+")}");
                 //   CONECTAR_ENVIAR_MENSAJE_SERVIDOR(padre+".."+RECPERAR_TEXTO_ARCHIVO_SIN_SIMBOLOS(ARCH.b_contentA));
                }
            }
       }
    }
}

void RECORRIDO_SYN_INDIRECTOS_CA(struct PARAMETROS_SYNCRONICE_PRIMARIA parametros,int I_Bloque,int I_CA,int nivel, int tipo,string padre,int Identificador){
    struct B_Apuntadores APUNT;
    APUNT=RECUPERAR_BLOQUE_INDIRECTO(parametros.inicio_particion,I_Bloque,parametros.valor_n,parametros.path_disco);

    nivel--;
    if((tipo==13) && (nivel==0)){
        if(I_CA==0){//APUNTADOR INDIRECTO DE CARPETAS
            int Identificador_hijos=Identificador;
            for(int i=0; i<13; i++){
                if(APUNT.b_pointers[i]!=-1){
                    struct B_Carpetas CARPETA;
                    CARPETA=RECUPERAR_BLOQUE_CARPETA(parametros.inicio_particion,APUNT.b_pointers[i],parametros.valor_n,parametros.path_disco);
                    for(int j=0; j<4; j++){
                        if(CARPETA.b_content[j].b_inodo!=-1){
                            Identificador_hijos++;
                            imprimirln(padre+".."+CARPETA.b_content[j].b_name);
                            imprimirln(to_string(Identificador)+"..."+to_string(Identificador_hijos));
                           // CONECTAR_ENVIAR_MENSAJE_SERVIDOR(padre+".."+CARPETA.b_content[j].b_name);
                            CONECTAR_ENVIAR_MENSAJE_SERVIDOR("{CALL INSERCION_AC('"+padre+"','"+RECUPERAR_TEXT_CHAR_STRING(CARPETA.b_content[j].b_name)+"','NULL ',"+to_string(Identificador)+","+to_string(Identificador_hijos)+",'"+parametros.nombre_disco+"','"+parametros.nombre_particion+"',"+"0"+","+"664"+")}");
                            INICIO_RECORRIDO_SYN_INODO(parametros,CARPETA.b_content[j].b_inodo,CARPETA.b_content[j].b_name,Identificador_hijos);
                        }
                    }
                }
            }
        }else
        if(I_CA==1){//APUNTADOR INDIRECTO DE ARCHIVOS
            for(int i=0; i<13; i++){
                if(APUNT.b_pointers[i]!=-1){
                    struct B_Archivos ARCH;
                    ARCH=RECUPERAR_BLOQUE_ARCHIVO(parametros.inicio_particion,APUNT.b_pointers[i],parametros.valor_n,parametros.path_disco);
                    imprimirln(padre+".."+ARCH.b_contentA);
                    CONECTAR_ENVIAR_MENSAJE_SERVIDOR("{CALL INSERCION_AC('"+padre+"','NULL','"+RECPERAR_TEXTO_ARCHIVO_SIN_SIMBOLOS(RECUPERAR_TEXT_CHAR_STRING(ARCH.b_contentA)+"',"+to_string(Identificador))+","+"0"+",'"+parametros.nombre_disco+"','"+parametros.nombre_particion+"',1,664)}");
                   // CONECTAR_ENVIAR_MENSAJE_SERVIDOR(padre+".."+RECPERAR_TEXTO_ARCHIVO_SIN_SIMBOLOS(ARCH.b_contentA));
                }
            }
        }
    }else
    if((tipo==14) && (nivel==1)){
        if(APUNT.b_pointers[13]!=-1){
            RECORRIDO_SYN_INDIRECTOS_CA(parametros,APUNT.b_pointers[13],I_CA,1,13,padre,Identificador);
        }
    }else
    if((tipo==15) && (nivel==2)){
        if(APUNT.b_pointers[13]!=-1){
            RECORRIDO_SYN_INDIRECTOS_CA(parametros,APUNT.b_pointers[13],I_CA,2,13,padre,Identificador);
        }
    }else
    if((tipo==13) && (nivel!=0)){
        if(APUNT.b_pointers[13]!=-1){
            RECORRIDO_SYN_INDIRECTOS_CA(parametros,APUNT.b_pointers[13],I_CA,1,13,padre,Identificador);
        }
    }

}

//ESTE METODO DEVUELVE EL ARCHIVO SIN SIMBOLOS COMO SALTOS DE LINEA TABULACIONES ETC.. (SE USA MAS PARA EL ARCHIVOS USERS.TXT QUE TIENE SALTOS LINEA)
string RECPERAR_TEXTO_ARCHIVO_SIN_SIMBOLOS(string texto){
    string NUEVO_TEXTO="";

    for(int i=0; i<texto.size(); i++){
        if(texto[i]!=10){
            NUEVO_TEXTO+=texto[i];
        }
    }

    return NUEVO_TEXTO;
}

string RECUPERAR_TEXT_CHAR_STRING(string texto){
    string nuevo_texto="";

    for(int i=0; i<texto.size(); i++){
        nuevo_texto+=texto[i];
    }

    return nuevo_texto;
}

vector<string> OBTENER_USUARIOS_DE_PARTICION(string texto_archivo_usuarios){
    vector<string> NOMBRES;

    vector<string> SEPARADOR_LINEAS=OBTENER_DIRECCION(texto_archivo_usuarios,"\n");

    for(int i=0; i<SEPARADOR_LINEAS.size(); i++){
        vector<string> SEPARADOR_COMAS=OBTENER_DIRECCION(SEPARADOR_LINEAS[i],",");
        if(SEPARADOR_COMAS.size()==5){
            NOMBRES.push_back(SEPARADOR_COMAS[3]);
        }
    }

    return NOMBRES;
}

//------------------------------------METODOS VARIOS DE VERIFICACION, ELIMINACION Y ACTUALIZACION DE DATOS --------------------------------

bool ES_ARCHIVO_ARCHIVODISCO(string nombre){
    bool respuesta=false;
    vector<string> NOMBRE_CON_EXTENSION=OBTENER_DIRECCION(nombre,".");

    if(NOMBRE_CON_EXTENSION.size()>1){
        respuesta=true;
    }
    return respuesta;
}

    vector<string> OBTENER_DIRECCION(string ruta,char* separador){
    char temporal[ruta.size()+1];
    strcpy(temporal,ruta.c_str());
    vector<string> respuesta=split(temporal,separador);
    return respuesta;
}

string CREAR_GRUPO_ARCHIVODISCO(string DATOSANTIGUOS,string NOMBRE_NGRUPO){
    vector<string> SEPARADORLINEAS=OBTENER_DIRECCION(DATOSANTIGUOS,"\n");
    int IDGRUPO=0;
    //RECORREMOS CADA DATOS, DE SALTO DE LINEA
    for(int i=0; i<SEPARADORLINEAS.size(); i++){
        //OBTENEMOS LOS DATOS SEPARADOS POR COMAS DE CADA SALTO DE LINEA
        vector<string> SEPARADORCOMAS=OBTENER_DIRECCION(SEPARADORLINEAS[i],",");
        //VA A ENTRAR AQUI CADA VEZ QUE ENCUENTRE UN GRUPO SIN IMPORTAR SI ESTA ACTIVO O NO YA QUE EL ID VA A SER CONTINUO
        if(SEPARADORCOMAS.size()==3){
            IDGRUPO++;
        }
    }
    //LE SUMAMOS UNO YA QUE EL VALOR QUE TIENE ACTUALMENTE ES DE LA CANTIDAD DE GRUPO QUE HAY Y QUEREMOS AGREGAR UN GRUPO NUEVO
    IDGRUPO++;
    //UNA VEZ OBTENIDO EL ID DEL GRUPO CREAMOS EL STRING CON TODOS LOS NUEVOS DATOS
    //PERO PRIMERO EL NOMBRE DEL GRUPO NO PUEDE PASAR DE MAS DE 10 CARACTERES ASI QUE OBTENEMOS EL NOMBRE CON MAX=10 CARACTERES
    string NOMBRE_EXACTO="";
    for(int i=0; (i<NOMBRE_NGRUPO.size()) && (i<10);i++){
        NOMBRE_EXACTO+=NOMBRE_NGRUPO[i];
    }
    string DATOS_ACTUALIZADOS=DATOSANTIGUOS+to_string(IDGRUPO)+",G,"+NOMBRE_EXACTO+"\n";
    return DATOS_ACTUALIZADOS;
}

string CREAR_USUARIO_ARCHIVODISCO(string DATOS_ANTIGUOS,string grupo, string password, string nombre){
    vector<string> SEPARADOR_LINEAS=OBTENER_DIRECCION(DATOS_ANTIGUOS,"\n");
    int ID_USUARIO=0;

    for(int i=0; i<SEPARADOR_LINEAS.size();i++){
        vector<string> SEPARADOR_COMAS=OBTENER_DIRECCION(SEPARADOR_LINEAS[i],",");
        if(SEPARADOR_COMAS.size()==5){
            ID_USUARIO++;
        }
    }

    ID_USUARIO++;//OBTENEMOS EL NUEVO ID DEL USUARIO

    string NUEVOS_DATOS=DATOS_ANTIGUOS+to_string(ID_USUARIO)+",U,"+grupo+","+nombre+","+password+"\n";
    return NUEVOS_DATOS;
}

string NOMBRE_EXACTO_10CARACTERES(string NOMBRE){
    string NOMBRE_EXACTO="";
        for(int i=0; (i<NOMBRE.size()) && (i<10); i++){
            NOMBRE_EXACTO+=NOMBRE[i];
        }
    return NOMBRE_EXACTO;
}

bool EXISTE_GRUPO_ARCHIVODISCO(string DATOSARCHIVO,string NOMBRE_GRUPO){
    bool respuesta=false;
    vector<string> SEPARADOR_LINEAS=OBTENER_DIRECCION(DATOSARCHIVO,"\n");

    for(int i=0; (i<SEPARADOR_LINEAS.size()) && (respuesta==false); i++){
        vector<string> SEPARADOR_COMAS=OBTENER_DIRECCION(SEPARADOR_LINEAS[i],",");
        //VERIFICAMOS QUE SEA UN GRUPO, Y QUE ESTE ACTIVO
        if((SEPARADOR_COMAS.size()==3) && (strcmp(SEPARADOR_COMAS[0].c_str(),"0")!=0)){
            if(strcmp(SEPARADOR_COMAS[2].c_str(),NOMBRE_GRUPO.c_str())==0){
                respuesta=true;
            }
        }
    }

    return respuesta;
}

bool EXISTE_USUARIO_ARCHIVODISCO(string DATOSARCHIVO,string NOMBRE_USUARIO){
    bool respuesta=false;
    vector<string> SEPARADOR_LINEAS=OBTENER_DIRECCION(DATOSARCHIVO,"\n");

    for(int i=0; (i<SEPARADOR_LINEAS.size()) && (respuesta==false); i++){
        vector<string> SEPARADOR_COMAS=OBTENER_DIRECCION(SEPARADOR_LINEAS[i],",");
        if((SEPARADOR_COMAS.size()==5) && (strcmp(SEPARADOR_COMAS[0].c_str(),"0")!=0)){//SE VALIDA QUE SEA UNA SECCION DE USUARIOS
            if(strcmp(SEPARADOR_COMAS[3].c_str(),NOMBRE_USUARIO.c_str())==0){
                respuesta=true;
            }
        }
    }
    return respuesta;
}

vector<string> TEXTO_BLOQUES_ARCHIVODISCO(string DATOSARCHIVO){
    string respuesta="";
    int contador_temporal=23;
    for(int i=0; i<DATOSARCHIVO.size(); i++){
        if(i<contador_temporal){
            respuesta+=DATOSARCHIVO[i];
        }else{
           // cout<<DATOSARCHIVO[i]<<endl;
            respuesta+=";";
            respuesta+=DATOSARCHIVO[i];
            contador_temporal+=23;
        }
    }
    vector<string> RESPONSE=OBTENER_DIRECCION(respuesta,";");
    return RESPONSE;
}

vector<string> ELIMINAR_BLOQUETEMPORAL_ARCHIVODISCO(string TEXTOBLOQUE){
    bool encontrado=false;
    vector<string> TEMPORAL_BLOQUEDOS;
    for(int i=0; i<DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR.size(); i++){
        /*if(strcmp(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[i].c_str(),TEXTOBLOQUE.c_str())!=0){
            TEMPORAL_BLOQUEDOS.push_back(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[i]);
        }*/
        if((strcmp(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[i].c_str(),TEXTOBLOQUE.c_str())==0) && (encontrado==false)){
            encontrado=true;
        }else{
            TEMPORAL_BLOQUEDOS.push_back(DATOS_TEMPORAL_BLOQUES_ARCHIVOS_ESCRIBIR[i]);
        }
    }
    return TEMPORAL_BLOQUEDOS;
}

int ID_NUEVO_APUNTADORINDIRECTO_ESCRIBIR_TEXT_ARCHIVODISCO(){
   // imprimirln("entro a metodo crear nuevo indirecto txt archivo");
    int IDENTIFICADO=0;
    bool creadobloque=false;
    //FILE *archivo=fopen(DATOS_PIVOTE[0].path_archivoExtra.c_str(),"rb+");
    if(ARCHIVO_GLOBAL==NULL){
        ARCHIVO_GLOBAL=fopen(DATOS_PIVOTE[0].path_archivoExtra.c_str(),"rb+");
    }else{
        imprimirln("6666666666666666666666666 ERROR ARCHIVO SIGUE ABIERTO 666666666666666666666666666");
    }

    int POSICIONINICIO=DATOS_PIVOTE[0].valor_N;
    int POSICIONFINAL=(2*DATOS_PIVOTE[0].valor_N);
    char verificar;
    for(int i=POSICIONINICIO; (i<POSICIONFINAL) && (creadobloque==false); i++){
        verificar=00;
        /*fseek(archivo,i,SEEK_SET);
        fread(&verificar,1,1,archivo);*/
        fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
        fread(&verificar,1,1,ARCHIVO_GLOBAL);
        if(verificar==48){
            char respuesta=49;
            /*fseek(archivo,i,SEEK_SET);
            fwrite(&respuesta,1,1,archivo);
            fflush(archivo);
            fclose(archivo);*/
            fseek(ARCHIVO_GLOBAL,i,SEEK_SET);
            fwrite(&respuesta,1,1,ARCHIVO_GLOBAL);
            fflush(ARCHIVO_GLOBAL);
            fclose(ARCHIVO_GLOBAL);
            ARCHIVO_GLOBAL=NULL;
            creadobloque=true;
            struct B_Apuntadores APUNTADOR;
            for(int j=0; j<16; j++){
                APUNTADOR.b_pointers[j]=-1;
            }
            int POSICIONGUARDAR=DATOS_PIVOTE[0].inicio_particion+sizeof(Super_Bloque)+(4*DATOS_PIVOTE[0].valor_N)+(DATOS_PIVOTE[0].valor_N*sizeof(Tabla_Inodos))+(DATOS_PIVOTE[0].valor_N*sizeof(B_Apuntadores))+(IDENTIFICADO*sizeof(B_Apuntadores));
            GUARDAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].path_disco,POSICIONGUARDAR,APUNTADOR);
        }else{
            IDENTIFICADO++;
        }
    }
    /*fflush(archivo);
    fclose(archivo);*/
    if(ARCHIVO_GLOBAL!=NULL){
        fflush(ARCHIVO_GLOBAL);
        fclose(ARCHIVO_GLOBAL);
        ARCHIVO_GLOBAL=NULL;
    }

    return IDENTIFICADO;
}

string OBTENER_RUTA_COMPLETA_JOURNALING(vector<string> direccion){
    string RUTA="";

    for(int i=0; i<direccion.size(); i++){
    RUTA+="/"+direccion[i];
    }
    return RUTA;
}

void ACTUALIZACION_JOURNALING_A_Y_C_ARCHIVODISCO(vector<string> RUTA,string ORIGEN_DATOS,string crearPadres,string path_journaling){
    string RUTACOMPLETA=OBTENER_RUTA_COMPLETA_JOURNALING(RUTA);
    struct JOURNALING JOUR;
    srand(time(NULL));
    strcpy(JOUR.RUTA,RUTACOMPLETA.c_str());
    strcpy(JOUR.CONTENIDO,ORIGEN_DATOS.c_str());
    strcpy(JOUR.CP,crearPadres.c_str());
    JOUR.FECHA_CREACION=time(NULL);
    GUARDAR_STRUCT_JOURNALING(path_journaling,JOUR);
}


void INICIO_REPORTE_INODOS(struct PARAMETRO_REP_TREE parametros, int I_INODO,int apuntador,int I_carpeta){

    struct Tabla_Inodos INODO;
    INODO=RECUPERAR_INODO(parametros.inicio_particion,I_INODO,parametros.valor_n,parametros.path_disco);

    if(INODO.i_type==0){
        //EN ESTA AREA PRIMERO SE ESCRIBE EN EL REPORTE LOS DATOS DEL INODO
        ARCHIVO_REPORTE<<"inodo"<<I_INODO<<"[label=\""<<"\n";
        ARCHIVO_REPORTE<<"<t"<<I_INODO<<">inodo"<<I_INODO<<"|";
        ARCHIVO_REPORTE<<"{"<<"i_uid"<<"|"<<INODO.i_uid<<"}|";
        ARCHIVO_REPORTE<<"{"<<"i_gid"<<"|"<<INODO.i_gid<<"}|";
        ARCHIVO_REPORTE<<"{"<<"i_size"<<"|"<<INODO.i_size<<"}|";
        ARCHIVO_REPORTE<<"{"<<"i_atime"<<"|"<<ctime(&INODO.i_atime)<<"}|";
        ARCHIVO_REPORTE<<"{"<<"i_ctime"<<"|"<<ctime(&INODO.i_ctime)<<"}|";
        ARCHIVO_REPORTE<<"{"<<"i_mtime"<<"|"<<ctime(&INODO.i_mtime)<<"}|";
        for(int i=0; i<16; i++){
          ARCHIVO_REPORTE<<"{"<<i<<"|<p"<<i<<">"<<INODO.i_block[i]<<"}|";
        }
        ARCHIVO_REPORTE<<"{"<<"i_type"<<"|"<<INODO.i_type<<"}|";
        ARCHIVO_REPORTE<<"{"<<"i_perm"<<"|"<<INODO.i_perm<<"}\"];"<<endl;

        if((apuntador!=-1) && (I_carpeta!=-1)){
            ARCHIVO_REPORTE<<"carpeta"<<I_carpeta<<":p"<<apuntador<<"->inodo"<<I_INODO<<":t"<<I_INODO<<endl;
        }

        for(int i=0; i<16; i++){//ESTE SOLO APUNTA A CARPETAS ASI QUE LLAMA AL METODO DE CARPETA
            if(INODO.i_block[i]!=-1){
                if(i==13){
                ////
                    struct B_Apuntadores INDIRECTO;
                    INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(parametros.inicio_particion,INODO.i_block[i],parametros.valor_n,parametros.path_disco);
                    ARCHIVO_REPORTE<<"apuntador"<<INODO.i_block[i]<<"[label=\"\n";
                    ARCHIVO_REPORTE<<"<t"<<INODO.i_block[i]<<">apuntador"<<INODO.i_block[i];
                    for(int k=0; k<13; k++){
                        ARCHIVO_REPORTE<<"|{"<<k<<"|<p"<<k<<">"<<INDIRECTO.b_pointers[k]<<"}";
                    }
                    ARCHIVO_REPORTE<<"\"];"<<endl;
                    ARCHIVO_REPORTE<<"inodo"<<I_INODO<<":p"<<i<<"->apuntador"<<INODO.i_block[i]<<":t"<<INODO.i_block[i]<<endl;
                    //REPORTE_INDIRECTOS_CARPETA(parametros,INODO.i_block[i],i,I_INODO,1,13);
                    REPORTE_INDIRECTOS_CARPETA(parametros,INODO.i_block[i],i,0,1,13);
                ////
                }else if(i==14){
                ////
                   struct B_Apuntadores INDIRECTO;
                    INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(parametros.inicio_particion,INODO.i_block[i],parametros.valor_n,parametros.path_disco);
                    ARCHIVO_REPORTE<<"apuntador"<<INODO.i_block[i]<<"[label=\"\n";
                    ARCHIVO_REPORTE<<"<t"<<INODO.i_block[i]<<">apuntador"<<INODO.i_block[i]<<"|";
                    ARCHIVO_REPORTE<<"{"<<13<<"|<p"<<13<<">"<<INDIRECTO.b_pointers[13]<<"}\"];"<<endl;
                    ARCHIVO_REPORTE<<"inodo"<<I_INODO<<":p"<<i<<"->apuntador"<<INODO.i_block[i]<<":t"<<INODO.i_block[i]<<endl;
                    REPORTE_INDIRECTOS_CARPETA(parametros,INODO.i_block[i],13,0,2,14);
                ////
                }else if(i==15){
                ////
                    struct B_Apuntadores INDIRECTO;
                    INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(parametros.inicio_particion,INODO.i_block[i],parametros.valor_n,parametros.path_disco);
                    ARCHIVO_REPORTE<<"apuntador"<<INODO.i_block[i]<<"[label=\"\n";
                    ARCHIVO_REPORTE<<"<t"<<INODO.i_block[i]<<">apuntador"<<INODO.i_block[i]<<"|";
                    ARCHIVO_REPORTE<<"{"<<13<<"|<p"<<13<<">"<<INDIRECTO.b_pointers[13]<<"}\"];"<<endl;
                    ARCHIVO_REPORTE<<"inodo"<<I_INODO<<":p"<<i<<"->apuntador"<<INODO.i_block[i]<<":t"<<INODO.i_block[i]<<endl;
                    REPORTE_INDIRECTOS_CARPETA(parametros,INODO.i_block[i],13,0,3,15);
                ////
                }else{
                    struct B_Carpetas CARPETA;
                    CARPETA=RECUPERAR_BLOQUE_CARPETA(parametros.inicio_particion,INODO.i_block[i],parametros.valor_n,parametros.path_disco);
                    //ESCRIBIENDO EN EL REPORTE
                    ARCHIVO_REPORTE<<"carpeta"<<INODO.i_block[i]<<"[label=\"\n";
                    ARCHIVO_REPORTE<<"<t"<<INODO.i_block[i]<<">carpeta"<<INODO.i_block[i];
                    for(int j=0; j<4; j++){
                        if(CARPETA.b_content[j].b_inodo!=-1){
                            ARCHIVO_REPORTE<<"|{"<<CARPETA.b_content[j].b_name<<"|<p"<<j<<">"<<CARPETA.b_content[j].b_inodo<<"}";
                        }else{
                            ARCHIVO_REPORTE<<"|{"<<"NULL"<<"|<p"<<j<<">"<<CARPETA.b_content[j].b_inodo<<"}";
                        }
                    }
                    ARCHIVO_REPORTE<<"\"];"<<endl;
                    ARCHIVO_REPORTE<<"inodo"<<I_INODO<<":p"<<i<<"->carpeta"<<INODO.i_block[i]<<":t"<<INODO.i_block[i]<<endl;
                    //VERIFICAR SI EL APUNTADOR ES EL NUMERO 0 DEL INODO NO DEBE REVISAR LOS PRIMEROS DOS BLOQUES DE CARPETAS
                    for(int j=0; j<4; j++){
                        if(CARPETA.b_content[j].b_inodo!=-1){
                            if(i==0){
                                if(j>1){
                                    INICIO_REPORTE_INODOS(parametros,CARPETA.b_content[j].b_inodo,j,INODO.i_block[i]);
                                }
                            }else{
                                INICIO_REPORTE_INODOS(parametros,CARPETA.b_content[j].b_inodo,j,INODO.i_block[i]);
                            }
                        }
                    }
                }
            }
        }

    }else
    if(INODO.i_type==1){
        ARCHIVO_REPORTE<<"inodo"<<I_INODO<<"[label=\""<<"\n";
        ARCHIVO_REPORTE<<"<t"<<I_INODO<<">inodo"<<I_INODO<<"|";
        ARCHIVO_REPORTE<<"{"<<"i_uid"<<"|"<<INODO.i_uid<<"}|";
        ARCHIVO_REPORTE<<"{"<<"i_gid"<<"|"<<INODO.i_gid<<"}|";
        ARCHIVO_REPORTE<<"{"<<"i_size"<<"|"<<INODO.i_size<<"}|";
        ARCHIVO_REPORTE<<"{"<<"i_atime"<<"|"<<ctime(&INODO.i_atime)<<"}|";
        ARCHIVO_REPORTE<<"{"<<"i_ctime"<<"|"<<ctime(&INODO.i_ctime)<<"}|";
        ARCHIVO_REPORTE<<"{"<<"i_mtime"<<"|"<<ctime(&INODO.i_mtime)<<"}|";
        for(int i=0; i<16; i++){
          ARCHIVO_REPORTE<<"{"<<i<<"|<p"<<i<<">"<<INODO.i_block[i]<<"}|";
        }
        ARCHIVO_REPORTE<<"{"<<"i_type"<<"|"<<INODO.i_type<<"}|";
        ARCHIVO_REPORTE<<"{"<<"i_perm"<<"|"<<INODO.i_perm<<"}\"];"<<endl;

        if((apuntador!=-1) && (I_carpeta!=-1)){
            ARCHIVO_REPORTE<<"carpeta"<<I_carpeta<<":p"<<apuntador<<"->inodo"<<I_INODO<<":t"<<I_INODO<<endl;
        }

        for(int i=0; i<16; i++){
            if(INODO.i_block[i]!=-1){
                if(i==13){
                    struct B_Apuntadores INDIRECTO;
                    INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(parametros.inicio_particion,INODO.i_block[i],parametros.valor_n,parametros.path_disco);
                    ARCHIVO_REPORTE<<"apuntador"<<INODO.i_block[i]<<"[label=\"\n";
                    ARCHIVO_REPORTE<<"<t"<<INODO.i_block[i]<<">apuntador"<<INODO.i_block[i];
                    for(int k=0; k<13; k++){
                        ARCHIVO_REPORTE<<"|{"<<k<<"|<p"<<k<<">"<<INDIRECTO.b_pointers[k]<<"}";
                    }
                    ARCHIVO_REPORTE<<"\"];"<<endl;
                    ARCHIVO_REPORTE<<"inodo"<<I_INODO<<":p"<<i<<"->apuntador"<<INODO.i_block[i]<<":t"<<INODO.i_block[i]<<endl;
                    //REPORTE_INDIRECTOS_CARPETA(parametros,INODO.i_block[i],i,I_INODO,1,13);
                    REPORTE_INDIRECTOS_CARPETA(parametros,INODO.i_block[i],i,1,1,13);
                }else if(i==14){
                   struct B_Apuntadores INDIRECTO;
                    INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(parametros.inicio_particion,INODO.i_block[i],parametros.valor_n,parametros.path_disco);
                    ARCHIVO_REPORTE<<"apuntador"<<INODO.i_block[i]<<"[label=\"\n";
                    ARCHIVO_REPORTE<<"<t"<<INODO.i_block[i]<<">apuntador"<<INODO.i_block[i]<<"|";
                    ARCHIVO_REPORTE<<"{"<<13<<"|<p"<<13<<">"<<INDIRECTO.b_pointers[13]<<"}\"];"<<endl;
                    ARCHIVO_REPORTE<<"inodo"<<I_INODO<<":p"<<i<<"->apuntador"<<INODO.i_block[i]<<":t"<<INODO.i_block[i]<<endl;
                    REPORTE_INDIRECTOS_CARPETA(parametros,INODO.i_block[i],13,1,2,14);
                }else if(i==15){
                    struct B_Apuntadores INDIRECTO;
                    INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(parametros.inicio_particion,INODO.i_block[i],parametros.valor_n,parametros.path_disco);
                    ARCHIVO_REPORTE<<"apuntador"<<INODO.i_block[i]<<"[label=\"\n";
                    ARCHIVO_REPORTE<<"<t"<<INODO.i_block[i]<<">apuntador"<<INODO.i_block[i]<<"|";
                    ARCHIVO_REPORTE<<"{"<<13<<"|<p"<<13<<">"<<INDIRECTO.b_pointers[13]<<"}\"];"<<endl;
                    ARCHIVO_REPORTE<<"inodo"<<I_INODO<<":p"<<i<<"->apuntador"<<INODO.i_block[i]<<":t"<<INODO.i_block[i]<<endl;
                    REPORTE_INDIRECTOS_CARPETA(parametros,INODO.i_block[i],13,1,3,15);
                }else{
                    struct B_Archivos ARCHIVO;
                    ARCHIVO=RECUPERAR_BLOQUE_ARCHIVO(parametros.inicio_particion,INODO.i_block[i],parametros.valor_n,parametros.path_disco);

                    ARCHIVO_REPORTE<<"archivo"<<INODO.i_block[i]<<"[label=\"\n";
                    ARCHIVO_REPORTE<<"<t"<<INODO.i_block[i]<<">archivo"<<INODO.i_block[i]<<"|";
                    ARCHIVO_REPORTE<<ARCHIVO.b_contentA<<"\"];";

                    ARCHIVO_REPORTE<<"inodo"<<I_INODO<<":p"<<i<<"->"<<"archivo"<<INODO.i_block[i]<<":t"<<INODO.i_block[i]<<endl;

                }
            }
        }


    }else{
        imprimirln("=== ERROR: NO SE PUDO RECUPERAR INODO PARA REPORTE ===");
    }

}
//ESTE EN VEZ DE USARSE PARA CARPETAS SE VA A USAR PARA ARCHIVOS
void REPORTE_INDIRECTOS_CARPETA(struct PARAMETRO_REP_TREE parametros,int I_bloque,int apuntador,int I_INO_AI, int nivel, int tipo){

    struct B_Apuntadores APUN;
    APUN=RECUPERAR_BLOQUE_INDIRECTO(parametros.inicio_particion,I_bloque,parametros.valor_n,parametros.path_disco);
    nivel--;
    if((tipo==13) && (nivel==0)){

        if(I_INO_AI==0){
            for(int i=0; i<13; i++){
                if(APUN.b_pointers[i]!=-1){
                ////
                     struct B_Carpetas CARPETA;
                    CARPETA=RECUPERAR_BLOQUE_CARPETA(parametros.inicio_particion,APUN.b_pointers[i],parametros.valor_n,parametros.path_disco);
                    //ESCRIBIENDO EN EL REPORTE
                    ARCHIVO_REPORTE<<"carpeta"<<APUN.b_pointers[i]<<"[label=\"\n";
                    ARCHIVO_REPORTE<<"<t"<<APUN.b_pointers[i]<<">carpeta"<<APUN.b_pointers[i];
                    for(int j=0; j<4; j++){
                        if(CARPETA.b_content[j].b_inodo!=-1){
                            ARCHIVO_REPORTE<<"|{"<<CARPETA.b_content[j].b_name<<"|<p"<<j<<">"<<CARPETA.b_content[j].b_inodo<<"}";
                        }else{
                            ARCHIVO_REPORTE<<"|{"<<"NULL"<<"|<p"<<j<<">"<<CARPETA.b_content[j].b_inodo<<"}";
                        }
                    }
                    ARCHIVO_REPORTE<<"\"];"<<endl;
                    ARCHIVO_REPORTE<<"apuntador"<<I_bloque<<":p"<<i<<"->carpeta"<<APUN.b_pointers[i]<<":t"<<APUN.b_pointers[i]<<endl;
                    //VERIFICAR SI EL APUNTADOR ES EL NUMERO 0 DEL INODO NO DEBE REVISAR LOS PRIMEROS DOS BLOQUES DE CARPETAS
                    for(int j=0; j<4; j++){
                        if(CARPETA.b_content[j].b_inodo!=-1){
                                INICIO_REPORTE_INODOS(parametros,CARPETA.b_content[j].b_inodo,j,APUN.b_pointers[i]);
                        }
                    }
                ////
                }
            }
        }else if(I_INO_AI==1){
            for(int i=0; i<13; i++){
                if(APUN.b_pointers[i]!=-1){
                        struct B_Archivos ARCHIVO;
                        ARCHIVO=RECUPERAR_BLOQUE_ARCHIVO(parametros.inicio_particion,APUN.b_pointers[i],parametros.valor_n,parametros.path_disco);

                        ARCHIVO_REPORTE<<"archivo"<<APUN.b_pointers[i]<<"[label=\"\n";
                        ARCHIVO_REPORTE<<"<t"<<APUN.b_pointers[i]<<">archivo"<<APUN.b_pointers[i]<<"|";
                        ARCHIVO_REPORTE<<ARCHIVO.b_contentA<<"\"];";

                        ARCHIVO_REPORTE<<"apuntador"<<I_bloque<<":p"<<i<<"->"<<"archivo"<<APUN.b_pointers[i]<<":t"<<APUN.b_pointers[i]<<endl;
                }
            }
        }

    /*    for(int i=0; i<13; i++){
            if(APUN.b_pointers[i]!=-1){
                    struct B_Archivos ARCHIVO;
                    ARCHIVO=RECUPERAR_BLOQUE_ARCHIVO(parametros.inicio_particion,APUN.b_pointers[i],parametros.valor_n,parametros.path_disco);

                    ARCHIVO_REPORTE<<"archivo"<<APUN.b_pointers[i]<<"[label=\"\n";
                    ARCHIVO_REPORTE<<"<t"<<APUN.b_pointers[i]<<">archivo"<<APUN.b_pointers[i]<<"|";
                    ARCHIVO_REPORTE<<ARCHIVO.b_contentA<<"\"];";

                    ARCHIVO_REPORTE<<"apuntador"<<I_bloque<<":p"<<i<<"->"<<"archivo"<<APUN.b_pointers[i]<<":t"<<APUN.b_pointers[i]<<endl;
            }
        }*/

    }else
    if((tipo==14) && (nivel==1)){
        if(APUN.b_pointers[13]!=-1){
            struct B_Apuntadores NUEVO;
            NUEVO=RECUPERAR_BLOQUE_INDIRECTO(parametros.inicio_particion,APUN.b_pointers[13],parametros.valor_n,parametros.path_disco);
            ARCHIVO_REPORTE<<"apuntador"<<APUN.b_pointers[13]<<"[label=\"\n";
            ARCHIVO_REPORTE<<"<t"<<APUN.b_pointers[13]<<">apuntador"<<APUN.b_pointers[13];
            for(int k=0; k<13; k++){
                 ARCHIVO_REPORTE<<"|{"<<k<<"|<p"<<k<<">"<<NUEVO.b_pointers[k]<<"}";
            }
            ARCHIVO_REPORTE<<"\"];"<<endl;
            ARCHIVO_REPORTE<<"apuntador"<<I_bloque<<":p"<<apuntador<<"->apuntador"<<APUN.b_pointers[13]<<":t"<<APUN.b_pointers[13]<<endl;
            REPORTE_INDIRECTOS_CARPETA(parametros,APUN.b_pointers[13],13,I_INO_AI,1,13);//tenia 13
        }
    }else
    if((tipo==15) && (nivel==2)){
        if(APUN.b_pointers[13]!=-1){
            struct B_Apuntadores NUEVO;
            NUEVO=RECUPERAR_BLOQUE_INDIRECTO(parametros.inicio_particion,APUN.b_pointers[13],parametros.valor_n,parametros.path_disco);
            ARCHIVO_REPORTE<<"apuntador"<<APUN.b_pointers[13]<<"[label=\"\n";
            ARCHIVO_REPORTE<<"<t"<<APUN.b_pointers[13]<<">apuntador"<<APUN.b_pointers[13]<<"|";
            ARCHIVO_REPORTE<<"{"<<13<<"|<p"<<13<<">"<<NUEVO.b_pointers[13]<<"}\"];"<<endl;
            ARCHIVO_REPORTE<<"apuntador"<<I_bloque<<":p"<<apuntador<<"->apuntador"<<APUN.b_pointers[13]<<":t"<<APUN.b_pointers[13]<<endl;
            REPORTE_INDIRECTOS_CARPETA(parametros,APUN.b_pointers[13],13,I_INO_AI,2,13);
        }
    }else
    if((tipo==13) && (nivel!=-1)){
        if(APUN.b_pointers[13]!=-1){
            struct B_Apuntadores NUEVO;
            NUEVO=RECUPERAR_BLOQUE_INDIRECTO(parametros.inicio_particion,APUN.b_pointers[13],parametros.valor_n,parametros.path_disco);
            ARCHIVO_REPORTE<<"apuntador"<<APUN.b_pointers[13]<<"[label=\"\n";
            ARCHIVO_REPORTE<<"<t"<<APUN.b_pointers[13]<<">apuntador"<<APUN.b_pointers[13];
          //  ARCHIVO_REPORTE<<"{"<<13<<"|<p"<<13<<">"<<NUEVO.b_pointers[13]<<"}\"];"<<endl;
            for(int k=0; k<13; k++){
                 ARCHIVO_REPORTE<<"|{"<<k<<"|<p"<<k<<">"<<NUEVO.b_pointers[k]<<"}";
            }
            ARCHIVO_REPORTE<<"\"];"<<endl;
            ARCHIVO_REPORTE<<"apuntador"<<I_bloque<<":p"<<apuntador<<"->apuntador"<<APUN.b_pointers[13]<<":t"<<APUN.b_pointers[13]<<endl;
            REPORTE_INDIRECTOS_CARPETA(parametros,APUN.b_pointers[13],13,I_INO_AI,1,13);
        }
    }

}

void INICIO_REPORTE_LS(vector<string> RUTA,int I_inodo,int P_RUTA){
    struct Tabla_Inodos INODO;
    bool encontrado=false;
    INODO=RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,I_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    if(P_RUTA<RUTA.size()){
        if(INODO.i_type==0){
            if(EXISTE_CARPETA_ARCHIVO_EN_INODO(INODO,RUTA[P_RUTA],DATOS_PIVOTE[0].inicio_particion,0,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco)){
                for(int i=0; (i<16) && (encontrado==false); i++){
                    if(INODO.i_block[i]!=-1){
                        if(i==13){
                           encontrado=INDIRECTO_REPORTE_LS(RUTA,INODO.i_block[i],P_RUTA,13,1);
                        }else
                        if(i==14){
                            encontrado=INDIRECTO_REPORTE_LS(RUTA,INODO.i_block[i],P_RUTA,14,2);
                        }else
                        if(i==15){
                            encontrado=INDIRECTO_REPORTE_LS(RUTA,INODO.i_block[i],P_RUTA,15,3);
                        }else{
                            struct B_Carpetas CARPETA;
                            CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,INODO.i_block[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                            for(int j=0; (j<4) && (encontrado==false); j++){
                                if(CARPETA.b_content[j].b_inodo!=-1){
                                    if(strcmp(CARPETA.b_content[j].b_name,RUTA[P_RUTA].c_str())==0){
                                        encontrado=true;
                                        struct Tabla_Inodos INODO2;
                                        INODO2=RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,CARPETA.b_content[j].b_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                        ARCHIVO_REPORTE<<"<tr><td>"<<PERMISOS_RECUPERADOS(INODO2.i_perm)<<"</td><td>"<<RECUPERAR_NOMBRE_USUARIO(INODO2.i_uid)<<"</td><td>"<<RECUPERAR_NOMBRE_GRUPO(INODO2.i_gid)<<"</td><td>";
                                        ARCHIVO_REPORTE<<INODO2.i_size<<"</td><td>"<<ctime(&INODO2.i_ctime)<<"</td><td>"<<RECUPERAR_TIPO(INODO2.i_type)<<"</td><td>"<<CARPETA.b_content[j].b_name<<"</td></tr>"<<endl;

                                        if(P_RUTA!=(RUTA.size()-1)){
                                            INICIO_REPORTE_LS(RUTA,CARPETA.b_content[j].b_inodo,(P_RUTA+1));
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                imprimirln("=== ERROR: UN ARCHIVO/CARPETA NO EXISTE ===");
            }

        }else
        if(INODO.i_type==1){

        }else{
            imprimirln("=== ERROR: NO SE PUDO RECUPERAR INODO PARA REPORTE LS ===");
        }
    }else{
        imprimirln("=== ERROR: EXCEDIO LA RUTA REPORTE LS ===");
    }

   // return encontrado;
}

bool INDIRECTO_REPORTE_LS(vector<string> RUTA,int I_bloque, int P_Ruta,int TIPO_APUNTADOR, int NIVEL_APUNTADOR){

    struct B_Apuntadores INDIRECTO;
    bool encontrado=false;
    INDIRECTO=RECUPERAR_BLOQUE_INDIRECTO(DATOS_PIVOTE[0].inicio_particion,I_bloque,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);

    NIVEL_APUNTADOR--;
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR==0)){
            for(int i=0; (i<13) && (encontrado==false); i++){
                if(INDIRECTO.b_pointers[i]!=-1){
                    ////
                            struct B_Carpetas CARPETA;
                            CARPETA=RECUPERAR_BLOQUE_CARPETA(DATOS_PIVOTE[0].inicio_particion,INDIRECTO.b_pointers[i],DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                            for(int j=0; (j<4) && (encontrado==false); j++){
                                if(CARPETA.b_content[j].b_inodo!=-1){
                                    if(strcmp(CARPETA.b_content[j].b_name,RUTA[P_Ruta].c_str())==0){
                                        encontrado=true;
                                        struct Tabla_Inodos INODO2;
                                        INODO2=RECUPERAR_INODO(DATOS_PIVOTE[0].inicio_particion,CARPETA.b_content[j].b_inodo,DATOS_PIVOTE[0].valor_N,DATOS_PIVOTE[0].path_disco);
                                        ARCHIVO_REPORTE<<"<tr><td>"<<PERMISOS_RECUPERADOS(INODO2.i_perm)<<"</td><td>"<<RECUPERAR_NOMBRE_USUARIO(INODO2.i_uid)<<"</td><td>"<<RECUPERAR_NOMBRE_GRUPO(INODO2.i_gid)<<"</td><td>";
                                        ARCHIVO_REPORTE<<INODO2.i_size<<"</td><td>"<<ctime(&INODO2.i_ctime)<<"</td><td>"<<RECUPERAR_TIPO(INODO2.i_type)<<"</td><td>"<<CARPETA.b_content[j].b_name<<"</td></tr>"<<endl;

                                        if(P_Ruta!=(RUTA.size()-1)){
                                            INICIO_REPORTE_LS(RUTA,CARPETA.b_content[j].b_inodo,(P_Ruta+1));
                                        }

                                    }
                                }
                            }
                    ////
                }
            }
    }else
    if((TIPO_APUNTADOR==14) && (NIVEL_APUNTADOR==1)){
        if(INDIRECTO.b_pointers[13]!=-1){
            encontrado=INDIRECTO_REPORTE_LS(RUTA,INDIRECTO.b_pointers[13],P_Ruta,13,1);
        }
    }else
    if((TIPO_APUNTADOR==15) && (NIVEL_APUNTADOR==2)){
        if(INDIRECTO.b_pointers[13]!=-1){
            encontrado=INDIRECTO_REPORTE_LS(RUTA,INDIRECTO.b_pointers[13],P_Ruta,13,2);
        }
    }else
    if((TIPO_APUNTADOR==13) && (NIVEL_APUNTADOR!=0)){
        if(INDIRECTO.b_pointers[13]!=-1){
            encontrado=INDIRECTO_REPORTE_LS(RUTA,INDIRECTO.b_pointers[13],P_Ruta,13,1);
        }
    }
    return encontrado;
}


string RECUPERAR_NOMBRE_GRUPO(int gID){
    string rutaArchivo="/users.txt";
    bool encontrado=false;
    string NUM_GRUPO=to_string(gID);
    vector<string> BUSCAR=OBTENER_DIRECCION(rutaArchivo,"/");
    string contenido=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(BUSCAR,0,0);
    string respuesta="";

    vector<string> DATOS=OBTENER_DIRECCION(contenido,"\n");

    for(int i=0; (i<DATOS.size()) && (encontrado==false); i++){
        vector<string> DATOS2=OBTENER_DIRECCION(DATOS[i],",");

        if(DATOS2.size()==3){
            if(strcmp(DATOS2[0].c_str(),NUM_GRUPO.c_str())==0){
                encontrado=true;
                respuesta=DATOS2[2];
            }
        }
    }

    return respuesta;
}

string RECUPERAR_NOMBRE_USUARIO(int uID){
    string rutaArchivo="/users.txt";
    bool encontrado=false;
    string NUM_USUARIO=to_string(uID);
    vector<string> BUSCAR=OBTENER_DIRECCION(rutaArchivo,"/");
    string contenido=OBTENER_TEXTO_USUARIOS_ARCHIVODISCO(BUSCAR,0,0);
    string respuesta="";

    vector<string> DATOS=OBTENER_DIRECCION(contenido,"\n");

    for(int i=0; (i<DATOS.size()) && (encontrado==false); i++){
        vector<string> DATOS2=OBTENER_DIRECCION(DATOS[i],",");

        if(DATOS2.size()==5){
            if(strcmp(DATOS2[0].c_str(),NUM_USUARIO.c_str())==0){
                encontrado=true;
                respuesta=DATOS2[3];
            }
        }
    }

    return respuesta;
}

string PERMISOS_RECUPERADOS(int permisos){
    string perm=to_string(permisos);
    string permUsuario=RECUPERAR_PERMISO_INDIVIDUAL(perm[0]);
    string permGrupo=RECUPERAR_PERMISO_INDIVIDUAL(perm[1]);
    string permOtro=RECUPERAR_PERMISO_INDIVIDUAL(perm[2]);

    return (permUsuario+permGrupo+permOtro);
}

string RECUPERAR_PERMISO_INDIVIDUAL(char numero){
    string respuesta="";

    switch(numero){
        case 48:
            respuesta="---";
        break;
        case 49:
            respuesta="--x";
        break;
        case 50:
            respuesta="-w-";
        break;
        case 51:
            respuesta="-wx";
        break;
        case 52:
            respuesta="r--";
        break;
        case 53:
            respuesta="r-x";
        break;
        case 54:
            respuesta="rw-";
        break;
        case 55:
            respuesta="rwx";
        break;
    }
    return respuesta;
}

string RECUPERAR_TIPO(int tipo){
    string respuesta;

    if(tipo==0){
        respuesta="carpeta";
    }else if(tipo==1){
        respuesta="archivo";
    }
    return respuesta;
}

string REMOVER_GRUPO(string DATOS,string nombreGrupo){
    vector<string> SALTO_LINEA=OBTENER_DIRECCION(DATOS,"\n");
    string DATOS_ACTUALIZADOS="";

    for(int i=0; i<SALTO_LINEA.size(); i++){
        vector<string> SEPARADOR_COMA=OBTENER_DIRECCION(SALTO_LINEA[i],",");

        if(SEPARADOR_COMA.size()==3){//SIGNIFICA QUE ES GRUPO
            if((strcmp(SEPARADOR_COMA[2].c_str(),nombreGrupo.c_str())==0) && (strcmp(SEPARADOR_COMA[0].c_str(),"0")!=0)){
                SEPARADOR_COMA[0]="0";
            }
        }

        for(int j=0; j<SEPARADOR_COMA.size(); j++){
            if(j==(SEPARADOR_COMA.size()-1)){
                DATOS_ACTUALIZADOS+=SEPARADOR_COMA[j]+"\n";
            }else{
                DATOS_ACTUALIZADOS+=SEPARADOR_COMA[j]+",";
            }
        }

    }
    return DATOS_ACTUALIZADOS;
}

string REMOVER_USUARIO(string DATOS,string nombreUsuario){
////
    vector<string> SALTO_LINEA=OBTENER_DIRECCION(DATOS,"\n");
    string DATOS_ACTUALIZADOS="";

    for(int i=0; i<SALTO_LINEA.size(); i++){
        vector<string> SEPARADOR_COMA=OBTENER_DIRECCION(SALTO_LINEA[i],",");

        if(SEPARADOR_COMA.size()==5){//SIGNIFICA QUE ES GRUPO
            if((strcmp(SEPARADOR_COMA[3].c_str(),nombreUsuario.c_str())==0) && (strcmp(SEPARADOR_COMA[0].c_str(),"0")!=0)){
                SEPARADOR_COMA[0]="0";
            }
        }

        for(int j=0; j<SEPARADOR_COMA.size(); j++){
            if(j==(SEPARADOR_COMA.size()-1)){
                DATOS_ACTUALIZADOS+=SEPARADOR_COMA[j]+"\n";
            }else{
                DATOS_ACTUALIZADOS+=SEPARADOR_COMA[j]+",";
            }
        }

    }
    return DATOS_ACTUALIZADOS;
////
}

string CAMBIAR_GRUPO_USUARIO(string DATOS,string nombreUsuario,string nombreGrupo){
////
    vector<string> SALTO_LINEA=OBTENER_DIRECCION(DATOS,"\n");
    string DATOS_ACTUALIZADOS="";

    for(int i=0; i<SALTO_LINEA.size(); i++){
        vector<string> SEPARADOR_COMA=OBTENER_DIRECCION(SALTO_LINEA[i],",");

        if(SEPARADOR_COMA.size()==5){//SIGNIFICA QUE ES USUARIO
            if((strcmp(SEPARADOR_COMA[3].c_str(),nombreUsuario.c_str())==0) && (strcmp(SEPARADOR_COMA[0].c_str(),"0")!=0)){
                SEPARADOR_COMA[2]=nombreGrupo;
            }
        }

        for(int j=0; j<SEPARADOR_COMA.size(); j++){
            if(j==(SEPARADOR_COMA.size()-1)){
                DATOS_ACTUALIZADOS+=SEPARADOR_COMA[j]+"\n";
            }else{
                DATOS_ACTUALIZADOS+=SEPARADOR_COMA[j]+",";
            }
        }

    }
    return DATOS_ACTUALIZADOS;
////
}

#endif // BLOQUES_ARCHIVOS_H_INCLUDED
